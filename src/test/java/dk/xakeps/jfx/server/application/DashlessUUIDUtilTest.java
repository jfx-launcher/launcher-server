package dk.xakeps.jfx.server.application;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DashlessUUIDUtilTest {

    @Test
    public void testToString() {
        UUID uuid = UUID.randomUUID();
        String s = DashlessUUIDUtil.toString(uuid);
        assertEquals(uuid.toString().replace("-", ""), s);
    }

    @Test
    public void fromString() {
        UUID expected = UUID.randomUUID();
        String replace = expected.toString().replace("-", "");
        UUID actual = DashlessUUIDUtil.fromString(replace);
        assertEquals(expected, actual);
    };
}