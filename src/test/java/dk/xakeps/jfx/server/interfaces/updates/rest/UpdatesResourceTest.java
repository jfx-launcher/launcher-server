package dk.xakeps.jfx.server.interfaces.updates.rest;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.*;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.util.HexFormat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
public class UpdatesResourceTest {
    @Order(4)
    @Test
    @TestSecurity(user = "admin", roles = {"MANAGE_UPDATES", "MANAGE_UPDATES_JAVA"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testZip() throws Exception {
        URL testJavaUrl = getClass().getResource("/java.zip");
        Assertions.assertNotNull(testJavaUrl, "/java.zip not found");
        URI testModPackUri = testJavaUrl.toURI();

        UpdatePackageType packageType = UpdatePackageType.JAVA;
        String version = "8";
        String arch = "x86_64";
        String system = "linux";

        RestAssured.given()
                .contentType("multipart/form-data")
                .multiPart(new File(testModPackUri))
                .multiPart("version", version)
                .post("updates-service/{updatePackageType}/{system}/{arch}", packageType, system, arch)
                .then()
                .statusCode(202);

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new EnableVersionRequest(version))
                .post("updates-service/{updatePackageType}/{system}/{arch}/version/enabled", packageType, system, arch)
                .then()
                .statusCode(200);

        UpdatePackageList list = RestAssured.given()
                .get("updates-service/{updatePackageType}/manifest.json", packageType)
                .as(UpdatePackageList.class);
        Assertions.assertEquals(1, list.manifests().size());
        UpdatePackageManifest manifest = list.manifests().get(0);
        Assertions.assertEquals(1, manifest.updates().size());
        Assertions.assertEquals(version, manifest.enabledVersion());

        UpdatePackageInfo versionInfo = manifest.updates().get(0);
        Assertions.assertEquals(version, versionInfo.version());
        Assertions.assertEquals(arch, versionInfo.arch());
        Assertions.assertEquals(system, versionInfo.system());

        UpdatePackageData versionData = RestAssured.given()
                .get(versionInfo.uri())
                .as(UpdatePackageData.class);
        Assertions.assertEquals(version, versionData.version());
        Assertions.assertEquals(arch, versionData.arch());
        Assertions.assertEquals(system, versionData.system());

        Assertions.assertEquals(2, versionData.files().size());
        Assertions.assertEquals(0, versionData.symlinks().size());

        checkFileArchive(versionData.files().get(0), "java/testFile.txt");
        checkFileArchive(versionData.files().get(1), "java/dir/testFileLink.txt");
    }

    // FIXME: Disabled due to https://github.com/quarkusio/quarkus/issues/20779
    @Disabled
    @Order(4)
    @Test
    @TestSecurity(user = "admin", roles = {"MANAGE_UPDATES", "MANAGE_UPDATES_JAVA"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testTarGz() throws Exception {
        URL testJavaUrl = getClass().getResource("/java.tar.gz");
        Assertions.assertNotNull(testJavaUrl, "/java.tar.gz not found");
        URI testModPackUri = testJavaUrl.toURI();

        UpdatePackageType packageType = UpdatePackageType.JAVA;
        String version = "8";
        String arch = "x86_64";
        String system = "linux";

        RestAssured.given()
                .contentType("multipart/form-data")
                .multiPart(new File(testModPackUri))
                .multiPart("version", version)
                .post("updates-service/{updatePackageType}/{system}/{arch}", packageType, system, arch)
                .then()
                .statusCode(202);

        UpdatePackageList list = RestAssured.given()
                .get("updates-service/{updatePackageType}/manifest.json", packageType)
                .as(UpdatePackageList.class);
        Assertions.assertEquals(1, list.manifests().size());
        UpdatePackageManifest manifest = list.manifests().get(0);
        Assertions.assertEquals(1, manifest.updates().size());

        UpdatePackageInfo versionInfo = manifest.updates().get(0);
        Assertions.assertEquals(version, versionInfo.version());
        Assertions.assertEquals(arch, versionInfo.arch());
        Assertions.assertEquals(system, versionInfo.system());

        UpdatePackageData versionData = RestAssured.given()
                .get(versionInfo.uri())
                .as(UpdatePackageData.class);
        Assertions.assertEquals(version, versionData.version());
        Assertions.assertEquals(arch, versionData.arch());
        Assertions.assertEquals(system, versionData.system());

        Assertions.assertEquals(1, versionData.files().size());
        Assertions.assertEquals(1, versionData.symlinks().size());

        checkFileArchive(versionData.files().get(0), "java/testFile.txt");

        SymlinkArtifactDto symlinkArtifact = versionData.symlinks().get(0);
        Assertions.assertEquals("java/dir/testFileLink.txt", symlinkArtifact.source());
        Assertions.assertEquals("java/testFile.txt", symlinkArtifact.target());
    }

    private static void checkFileArchive(FileArtifactDto fileArtifact, String file)
            throws Exception {
        Assertions.assertEquals(file, fileArtifact.file());
        URL testFileUrl = UpdatesResourceTest.class.getResource("/" + file);
        Assertions.assertNotNull(testFileUrl);
        Path testFilePath = Path.of(testFileUrl.toURI());
        long size = Files.size(testFilePath);
        Assertions.assertEquals(size, fileArtifact.size());
        byte[] dlBytes = RestAssured.given().get(fileArtifact.uri()).asByteArray();
        byte[] srcBytes = Files.readAllBytes(testFilePath);
        Assertions.assertArrayEquals(srcBytes, dlBytes);
        String srcHash = HexFormat.of().formatHex(MessageDigest.getInstance("SHA-1").digest(srcBytes));
        Assertions.assertEquals(srcHash, fileArtifact.hash());
    }

    @Order(3)
    @Test
    @TestSecurity(user = "admin", roles = {"MANAGE_UPDATES", "MANAGE_UPDATES_JAVA"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testNoAccess() throws URISyntaxException {
        URL testJavaUrl = getClass().getResource("/java.zip");
        Assertions.assertNotNull(testJavaUrl, "/java.zip not found");
        URI testModPackUri = testJavaUrl.toURI();

        UpdatePackageType packageType = UpdatePackageType.UPDATER;
        String version = "8";
        String arch = "x86_64";
        String system = "linux";

        RestAssured.given()
                .contentType("multipart/form-data")
                .multiPart(new File(testModPackUri))
                .multiPart("version", version)
                .post("updates-service/{updatePackageType}/{system}/{arch}", packageType, system, arch)
                .then()
                .statusCode(403);
    }

    @Order(2)
    @Test
    @TestSecurity(user = "admin", roles = {"MANAGE_UPDATES", "MANAGE_UPDATES_JAVA"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testDeletePackage() throws URISyntaxException {
        URL testJavaUrl = getClass().getResource("/java.zip");
        Assertions.assertNotNull(testJavaUrl, "/java.zip not found");
        URI testModPackUri = testJavaUrl.toURI();

        UpdatePackageType packageType = UpdatePackageType.JAVA;
        String version = "8";
        String arch = "x86_64";
        String system = "linux";

        RestAssured.given()
                .contentType("multipart/form-data")
                .multiPart(new File(testModPackUri))
                .multiPart("version", version)
                .post("updates-service/{updatePackageType}/{system}/{arch}", packageType, system, arch)
                .then()
                .statusCode(202);

        UpdatePackageList list = RestAssured.given()
                .get("updates-service/{updatePackageType}/manifest.json", packageType)
                .as(UpdatePackageList.class);
        Assertions.assertEquals(1, list.manifests().size());
        UpdatePackageManifest manifest = list.manifests().get(0);
        Assertions.assertEquals(1, manifest.updates().size());

        RestAssured.given()
                .delete("updates-service/{updatePackageType}/{system}/{arch}", packageType, system, arch)
                .then()
                .statusCode(202);

        UpdatePackageList listAfterDelete = RestAssured.given()
                .get("updates-service/{updatePackageType}/manifest.json", packageType)
                .as(UpdatePackageList.class);
        Assertions.assertEquals(0, listAfterDelete.manifests().size());
    }

    @Order(1)
    @Test
    @TestSecurity(user = "admin", roles = {"MANAGE_UPDATES", "MANAGE_UPDATES_JAVA"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testDeleteVersion() throws URISyntaxException {
        URL testJavaUrl = getClass().getResource("/java.zip");
        Assertions.assertNotNull(testJavaUrl, "/java.zip not found");
        URI testModPackUri = testJavaUrl.toURI();

        UpdatePackageType packageType = UpdatePackageType.JAVA;
        String version = "8";
        String arch = "x86_64";
        String system = "linux";

        RestAssured.given()
                .contentType("multipart/form-data")
                .multiPart(new File(testModPackUri))
                .multiPart("version", version)
                .post("updates-service/{updatePackageType}/{system}/{arch}", packageType, system, arch)
                .then()
                .statusCode(202);

        UpdatePackageList list = RestAssured.given()
                .get("updates-service/{updatePackageType}/manifest.json", packageType)
                .as(UpdatePackageList.class);
        Assertions.assertEquals(1, list.manifests().size());
        UpdatePackageManifest manifest = list.manifests().get(0);
        Assertions.assertEquals(1, manifest.updates().size());

        RestAssured.given()
                .delete("updates-service/{updatePackageType}/{system}/{arch}/{version}", packageType, system, arch, version)
                .then()
                .statusCode(200);

        UpdatePackageList listAfterDelete = RestAssured.given()
                .get("updates-service/{updatePackageType}/manifest.json", packageType)
                .as(UpdatePackageList.class);
        Assertions.assertEquals(1, listAfterDelete.manifests().size());
        UpdatePackageManifest manifestAfterDelete = listAfterDelete.manifests().get(0);
        Assertions.assertEquals(0, manifestAfterDelete.updates().size());
    }

    @AfterEach
    void cleanDir() throws IOException {
        Path path = Path.of("updates.d");
        if (Files.notExists(path)) {
            return;
        }
        Files.walkFileTree(path, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.deleteIfExists(file);
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.deleteIfExists(dir);
                return super.postVisitDirectory(dir, exc);
            }
        });
    }
}