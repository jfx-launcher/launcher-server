package dk.xakeps.jfx.server.interfaces.profile.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GameProfileDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GetOrCreateRequest;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.ProfileListDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.PropertyDto;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.smallrye.jwt.build.Jwt;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Set;
import java.util.UUID;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
public class SkinResourceTest {

    enum Params {
        SKIN_CLASSIC("skin", "classic", false),
        SKIN_SLIM("skin", "slim", false),
        CLOAK("cape", "", true);

        private final String skinType;
        private final String variant;
        private final boolean cape;

        Params(String skinType, String variant, boolean cape) {
            this.skinType = skinType;
            this.variant = variant;
            this.cape = cape;
        }

        private boolean isCape() {
            return cape;
        }
    }

    @ParameterizedTest
    @EnumSource(Params.class)
    public void updateSkin(Params params) throws Exception {
        String accessToken = getAccessToken("admin", Set.of("USER", "CHANGE_SKIN", "CHANGE_CAPE"));
        ProfileListDto oldProfiles = RestAssured.given().auth().oauth2(accessToken)
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        URI testSkinUri = getClass().getResource("/test_skin.png").toURI();
        RestAssured.given().auth().oauth2(accessToken)
                .contentType("multipart/form-data")
                .multiPart(new File(testSkinUri))
                .formParam("variant", params.variant)
                .formParam("profileId", oldProfiles.selectedProfile().toString())
                .post("profile-service/skin/" + params.skinType)
                .then()
                .statusCode(202);

        ProfileListDto updatedProfiles = RestAssured.given().auth().oauth2(accessToken)
                .body(new GetOrCreateRequest(true, true))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);
        UUID selectedProfileId = updatedProfiles.selectedProfile();
        GameProfileDto selectedProfile = null;
        for (GameProfileDto profile : updatedProfiles.profiles()) {
            if (selectedProfileId.equals(profile.id())) {
                selectedProfile = profile;
                break;
            }
        }
        Assertions.assertNotNull(selectedProfile, "selectedProfile null");

        PropertyDto texturesPropertyDto = null;
        for (PropertyDto property : selectedProfile.properties()) {
            if ("textures".equals(property.name())) {
                texturesPropertyDto = property;
                break;
            }
        }
        Assertions.assertNotNull(texturesPropertyDto, "no textures property");

        String jsonString = new String(Base64.getDecoder().decode(texturesPropertyDto.value()), StandardCharsets.UTF_8);
        JsonNode jsonNode = new ObjectMapper().readTree(jsonString);

        String profileId = jsonNode.path("profileId").asText();
        Assertions.assertEquals(selectedProfileId.toString(), profileId);

        String profileName = jsonNode.path("profileName").asText();
        Assertions.assertEquals(selectedProfile.name(), profileName);

        JsonNode textures = jsonNode.path("textures");
        JsonNode skin;
        if (params.isCape()) {
            skin = textures.path("CAPE");
        } else {
            skin = textures.path("SKIN");
        }
        String url = skin.path("url").asText();
        Assertions.assertNotNull(url, "url");

        InputStream pubKeyIs = getClass().getResourceAsStream("/public.der");
        Assertions.assertNotNull(pubKeyIs, "can't find public key");
        X509EncodedKeySpec spec = new X509EncodedKeySpec(IOUtils.toByteArray(pubKeyIs));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(spec);

        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initVerify(publicKey);
        signature.update(texturesPropertyDto.value().getBytes(StandardCharsets.UTF_8));
        signature.verify(Base64.getDecoder().decode(texturesPropertyDto.signature()));

        byte[] gotSkinBytes = RestAssured.given().get(url).asByteArray();
        Path testSkinPath = Path.of(testSkinUri);
        byte[] testSkinBytes = Files.readAllBytes(testSkinPath);
        Assertions.assertArrayEquals(testSkinBytes, gotSkinBytes, "Skins do not equal!");
    }

    @Test
    public void updateSkinNoAccess() throws URISyntaxException {
        String accessToken = getAccessToken("admin", Set.of("USER"));
        ProfileListDto as = RestAssured.given().auth().oauth2(accessToken)
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        RestAssured.given().auth().oauth2(getAccessToken("admin", Set.of()))
                .contentType("multipart/form-data")
                .multiPart(new File(getClass().getResource("/test_skin.png").toURI()))
                .formParam("variant", "classic")
                .formParam("profileId", as.selectedProfile().toString())
                .post("profile-service/skin/skin")
                .then()
                .statusCode(403);
    }

    private String getAccessToken(String userName, Set<String> groups) {
        return Jwt.preferredUserName(userName)
                .subject("randomSubject")
                .groups(groups)
                .issuer("https://server.example.com")
                .audience("https://service.example.com")
                .sign();
    }
}