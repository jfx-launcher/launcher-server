package dk.xakeps.jfx.server.interfaces.account.rest;

import dk.xakeps.jfx.server.domain.model.account.RemoteAccountInfo;
import dk.xakeps.jfx.server.domain.service.RemoteAccountFinderService;
import io.quarkus.test.Mock;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mock
public class MockRemoteAccountFinderService implements RemoteAccountFinderService {
    private static final String ID = "20a8ecda-7f89-4429-a150-14e250db9cc6";
    private static final String EMAIL = "admin@xakeps.dk";
    private static final RemoteAccountInfo ACCOUNT_INFO = new RemoteAccountInfo(ID, EMAIL);

    @Override
    public Optional<RemoteAccountInfo> findAccountById(String id) {
        if (ID.equals(id)) {
            return Optional.of(ACCOUNT_INFO);
        }
        return Optional.empty();
    }

    @Override
    public Optional<RemoteAccountInfo> findAccountByEmail(String email) {
        if (EMAIL.equals(email)) {
            return Optional.of(ACCOUNT_INFO);
        }
        return Optional.empty();
    }

    @Override
    public List<RemoteAccountInfo> findAccountsByEmails(List<String> emails) {
        return emails.stream()
                .map(this::findAccountByEmail)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    @Override
    public List<RemoteAccountInfo> findAccountsByIds(List<String> ids) {
        return ids.stream()
                .map(this::findAccountById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
