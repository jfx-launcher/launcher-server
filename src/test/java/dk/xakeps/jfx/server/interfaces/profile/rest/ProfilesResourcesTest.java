package dk.xakeps.jfx.server.interfaces.profile.rest;

import dk.xakeps.jfx.server.interfaces.profile.facade.dto.CreateProfileRequest;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GameProfileDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GetOrCreateRequest;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.ProfileListDto;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
public class ProfilesResourcesTest {
    @Test
    @TestSecurity(user = "admin", roles = "USER")
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void getOrCreate() {
        ProfileListDto content = RestAssured.given()
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);
        UUID selectedProfile = content.selectedProfile();
        assertNotNull(selectedProfile, "selectedProfile is null");
        List<GameProfileDto> profiles = content.profiles();
        assertNotNull(profiles, "profiles is null");
        assertTrue(profiles.size() > 0, "profiles is empty");
        GameProfileDto profileDto = profiles.stream()
                .filter(gameProfileDto -> gameProfileDto.id().equals(selectedProfile))
                .findFirst().orElse(null);
        assertNotNull(profileDto, "selectedProfile not found in profiles list");
        assertEquals("admin", profileDto.name());
    }

    @Test
    @TestSecurity(user = "admin")
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void getOrCreateNoAccess() {
        RestAssured.given()
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .then().statusCode(403);
    }

    @Test
    @TestSecurity(user = "admin", roles = "USER")
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void getOrCreateFullAndSigned() {
        ProfileListDto content = RestAssured.given()
                .body(new GetOrCreateRequest(true, true))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);
        UUID selectedProfile = content.selectedProfile();
        assertNotNull(selectedProfile, "selectedProfile is null");
        List<GameProfileDto> profiles = content.profiles();
        assertNotNull(profiles, "profiles is null");
        assertTrue(profiles.size() > 0, "profiles is empty");
        GameProfileDto profileDto = profiles.stream()
                .filter(gameProfileDto -> gameProfileDto.id().equals(selectedProfile))
                .findFirst().orElse(null);
        assertNotNull(profileDto, "selectedProfile not found in profiles list");
        assertEquals("admin", profileDto.name());
    }

    @Test
    @TestSecurity(user = "admin", roles = {"USER", "MANY_PROFILES"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void createProfile() {
        RestAssured.given()
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        String testName = "testName";
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateProfileRequest(testName))
                .post("profile-service/profiles/create")
                .then()
                .statusCode(201);

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateProfileRequest(testName))
                .redirects().follow(false)
                .post("profile-service/profiles/create")
                .then()
                .statusCode(303).extract();
    }

    @Test
    @TestSecurity(user = "admin", roles = {"USER"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void createProfileNoAccess() {
        RestAssured.given()
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        String testName = "testName";
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateProfileRequest(testName))
                .post("profile-service/profiles/create")
                .then()
                .statusCode(403);
    }

    @Test
    @TestSecurity(user = "admin", roles = {"USER", "MANY_PROFILES"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void findProfileIds() {
        ProfileListDto as = RestAssured.given()
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateProfileRequest("NameA"))
                .post("profile-service/profiles/create")
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateProfileRequest("NameB"))
                .post("profile-service/profiles/create")
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateProfileRequest("NameC"))
                .post("profile-service/profiles/create")
                .then()
                .statusCode(201);

        List<GameProfileDto> profiles = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(Set.of("NameA".toLowerCase(Locale.ENGLISH), "NameB".toLowerCase(Locale.ENGLISH)))
                .post("profile-service/profiles")
                .as(new TypeRef<>() {
                });
        Assertions.assertEquals(2, profiles.size());
        List<String> names = profiles.stream().map(GameProfileDto::name).collect(Collectors.toList());
        Assertions.assertTrue(names.contains("NameA"));
        Assertions.assertTrue(names.contains("NameB"));
    }
}