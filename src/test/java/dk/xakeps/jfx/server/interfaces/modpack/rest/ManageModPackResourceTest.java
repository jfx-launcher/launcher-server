package dk.xakeps.jfx.server.interfaces.modpack.rest;

import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.*;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GetOrCreateRequest;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.ProfileListDto;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.smallrye.jwt.build.Jwt;
import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Set;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
public class ManageModPackResourceTest {
    @Test
    @TestSecurity(user = "admin", roles = {"USER", "CREATE_MOD_PACK", "MODIFY_MOD_PACK"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testUpload() throws URISyntaxException, IOException, NoSuchAlgorithmException {
        ProfileListDto content = RestAssured.given()
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        String modPackId = "mod_pack_id";
        String minecraftVersion = "minecraft_version";
        String javaVersion = "17";
        Set<String> ignoredFiles = Set.of("dir1", "dir2", "dir3");

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateModPackRequest(modPackId))
                .post("mod-pack-service/manage/create")
                .then().statusCode(200);

        URL testModPackUrl = getClass().getResource("/test_mod_pack.zip");
        Assertions.assertNotNull(testModPackUrl, "/test_mod_pack.zip not found");
        URI testModPackUri = testModPackUrl.toURI();
        RestAssured.given()
                .contentType(ContentType.MULTIPART)
                .multiPart(new File(testModPackUri))
                .formParam("minecraftVersion", minecraftVersion)
                .formParam("javaVersion", javaVersion)
                .multiPart("ignoredFiles", ignoredFiles, MediaType.APPLICATION_JSON)
                .post("mod-pack-service/manage/{modPackId}/versions", modPackId)
                .then()
                .statusCode(202);

        ModPacksManifest manifest = RestAssured.given()
                .get("mod-pack-service/mod_manifest.json")
                .as(ModPacksManifest.class);
        ModPackInfo modPackInfo = manifest.modPacks().stream()
                .filter(mp -> mp.modPackId().equals(modPackId))
                .findFirst().orElse(null);
        Assertions.assertNotNull(modPackInfo);
        List<ModPackVersionDto> versions = modPackInfo.versions();
        Assertions.assertEquals(1, versions.size());
        ModPackVersionDto modPackVersion = versions.get(0);

//        Assertions.assertEquals(createdModPackResource, modPackVersion.uri());

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new EnableModPackVersionRequest(modPackVersion.version()))
                .post("mod-pack-service/manage/{modPackId}/versions/enabled", modPackId)
                .then()
                .statusCode(200);

        ModPackData modPackData = RestAssured.get(modPackVersion.uri())
                .as(ModPackData.class);
        Assertions.assertEquals(modPackId, modPackData.modPackId());
        Assertions.assertEquals(minecraftVersion, modPackData.minecraftVersion());
        Assertions.assertEquals(javaVersion, modPackData.javaVersion());
        Assertions.assertEquals(ignoredFiles, modPackData.ignoredFiles());

        List<ModArtifact> files = modPackData.files();
        Assertions.assertEquals(2, files.size());
        ModArtifact firstFile = files.get(0);
        ModArtifact secondFile = files.get(1);

        Assertions.assertTrue(firstFile.file().equals("dir/UploadedModPack.txt")
                || firstFile.file().equals("test_skin.png"));
        Assertions.assertTrue(secondFile.file().equals("dir/UploadedModPack.txt")
                || secondFile.file().equals("test_skin.png"));

        if (firstFile.file().equals("dir/UploadedModPack.txt")) {
            checkArtifact("dir/UploadedModPack.txt", firstFile);
        } else {
            checkArtifact("test_skin.png", firstFile);
        }

        if (secondFile.file().equals("dir/UploadedModPack.txt")) {
            checkArtifact("dir/UploadedModPack.txt", secondFile);
        } else {
            checkArtifact("test_skin.png", secondFile);
        }
    }

    @Test
    @TestSecurity(user = "admin", roles = {"USER", "CREATE_MOD_PACK", "MODIFY_MOD_PACK", "MODIFY_MOD_PACK_OVERRIDE"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testDelete() {
        ProfileListDto content = RestAssured.given()
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        String modPackId = "mod_pack_to_delete";
        String minecraftVersion = "minecraft_version_to_delete";

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateModPackRequest(modPackId))
                .post("mod-pack-service/manage/create")
                .then().statusCode(200);

        // check exists
        ModPacksManifest manifest = RestAssured.given()
                .get("mod-pack-service/mod_manifest.json")
                .as(ModPacksManifest.class);
        ModPackInfo modPackInfo = manifest.modPacks().stream()
                .filter(mp -> mp.modPackId().equals(modPackId))
                .findFirst().orElse(null);
        Assertions.assertNotNull(modPackInfo);

        RestAssured.given()
                .delete("mod-pack-service/manage/{modPackId}", modPackId)
                .then()
                .statusCode(202);

        // check not exists
        ModPacksManifest manifestAfterDelete = RestAssured.given()
                .get("mod-pack-service/mod_manifest.json")
                .as(ModPacksManifest.class);
        ModPackInfo notExistingModPack = manifestAfterDelete.modPacks().stream()
                .filter(mp -> mp.modPackId().equals(modPackId))
                .findFirst().orElse(null);
        Assertions.assertNull(notExistingModPack);
    }

    @Test
    @TestSecurity(user = "admin", roles = {"USER", "CREATE_MOD_PACK", "MODIFY_MOD_PACK"})
    @OidcSecurity(claims = {
            @Claim(key = "sub", value = "randomSubject")
    })
    public void testManager() {
        ProfileListDto content = RestAssured.given()
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        String modPackId = "mod_pack_id_1";
        String minecraftVersion = "minecraft_version_1";

        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(new CreateModPackRequest(modPackId))
                .post("mod-pack-service/manage/create")
                .then().statusCode(200);

        List<ModPackAdmin> resp = RestAssured.given()
                .get("mod-pack-service/manage/{modPackId}/managers", modPackId)
                .then()
                .statusCode(200)
                .extract()
                .as(new TypeRef<>() {});
        Assertions.assertEquals(1, resp.size());
        Assertions.assertEquals("randomSubject", resp.get(0).externalId());
    }

    @Test
    public void testManagerAdd() {
        String firstUserAccessToken = getAccessToken("admin", "randomSubject",
                Set.of("USER", "CREATE_MOD_PACK", "MODIFY_MOD_PACK", "MODIFY_MOD_PACK_OVERRIDE"));
        RestAssured.given()
                .auth().oauth2(firstUserAccessToken)
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);
        String secondUserAccessToken = getAccessToken("second_user", "secondUserSubject",
                Set.of("USER", "CREATE_MOD_PACK", "MODIFY_MOD_PACK"));
        RestAssured.given().auth().oauth2(secondUserAccessToken)
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        String modPackId = "mod_pack_id_2";
        String minecraftVersion = "minecraft_version_2";

        RestAssured.given()
                .auth().oauth2(firstUserAccessToken)
                .contentType(ContentType.JSON)
                .body(new CreateModPackRequest(modPackId))
                .post("mod-pack-service/manage/create")
                .then().statusCode(200);

        RestAssured.given()
                .auth().oauth2(firstUserAccessToken)
                .contentType(ContentType.JSON)
                .body(new ManagerRequest("secondUserSubject"))
                .post("mod-pack-service/manage/{modPackId}/managers", modPackId)
                .then()
                .statusCode(200);

        List<ModPackAdmin> resp = RestAssured.given()
                .auth().oauth2(secondUserAccessToken)
                .get("mod-pack-service/manage/{modPackId}/managers", modPackId)
                .then()
                .statusCode(200)
                .extract()
                .as(new TypeRef<>() {});
        Assertions.assertEquals(2, resp.size());
        Assertions.assertTrue(resp.get(0).externalId().equals("randomSubject")
                || resp.get(1).externalId().equals("randomSubject"));
        Assertions.assertTrue(resp.get(0).externalId().equals("secondUserSubject")
                || resp.get(1).externalId().equals("secondUserSubject"));
    }

    @Test
    public void testManagerRemove() {
        String firstUserAccessToken = getAccessToken("admin", "randomSubject",
                Set.of("USER", "CREATE_MOD_PACK", "MODIFY_MOD_PACK", "MODIFY_MOD_PACK_OVERRIDE"));
        RestAssured.given()
                .auth().oauth2(firstUserAccessToken)
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);
        String secondUserAccessToken = getAccessToken("second_user", "secondUserSubject",
                Set.of("USER", "CREATE_MOD_PACK", "MODIFY_MOD_PACK"));
        RestAssured.given().auth().oauth2(secondUserAccessToken)
                .body(new GetOrCreateRequest(false, false))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .as(ProfileListDto.class);

        String modPackId = "mod_pack_id_3";
        String minecraftVersion = "minecraft_version_3";

        RestAssured.given()
                .auth().oauth2(firstUserAccessToken)
                .contentType(ContentType.JSON)
                .body(new CreateModPackRequest(modPackId))
                .post("mod-pack-service/manage/create")
                .then().statusCode(200);

        RestAssured.given()
                .auth().oauth2(firstUserAccessToken)
                .contentType(ContentType.JSON)
                .body(new ManagerRequest("secondUserSubject"))
                .post("mod-pack-service/manage/{modPackId}/managers", modPackId)
                .then()
                .statusCode(200);

        RestAssured.given()
                .auth().oauth2(firstUserAccessToken)
                .contentType(ContentType.JSON)
                .delete("mod-pack-service/manage/{modPackId}/managers/{manager}", modPackId, "secondUserSubject")
                .then()
                .statusCode(200);

        List<ModPackAdmin> resp = RestAssured.given()
                .auth().oauth2(firstUserAccessToken)
                .get("mod-pack-service/manage/{modPackId}/managers", modPackId)
                .then()
                .statusCode(200)
                .extract()
                .as(new TypeRef<>() {});
        Assertions.assertEquals(1, resp.size());
        Assertions.assertEquals("randomSubject", resp.get(0).externalId());
    }

    private void checkArtifact(String expectedFile, ModArtifact modArtifact) throws URISyntaxException, IOException, NoSuchAlgorithmException {
        Assertions.assertEquals(expectedFile, modArtifact.file());
        URL resource = getClass().getResource("/" + expectedFile);
        Assertions.assertNotNull(resource, "/" + expectedFile + " not found");
        Path filePath = Path.of(resource.toURI());

        long localFileSize = Files.size(filePath);
        Assertions.assertEquals(localFileSize, modArtifact.size());

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        byte[] localFileBytes = Files.readAllBytes(filePath);
        byte[] localFileDigest = messageDigest.digest(localFileBytes);
        String localHashStr = Hex.encodeHexString(localFileDigest);
        Assertions.assertEquals(localHashStr, modArtifact.hash());

        byte[] downloadedFileBytes = RestAssured.get(modArtifact.url()).asByteArray();
        Assertions.assertEquals(modArtifact.size(), downloadedFileBytes.length);
        byte[] downloadedFileDigest = messageDigest.digest(downloadedFileBytes);
        String downloadedHashStr = Hex.encodeHexString(downloadedFileDigest);
        Assertions.assertEquals(modArtifact.hash(), downloadedHashStr);
    }

    private String getAccessToken(String userName, String subject, Set<String> groups) {
        return Jwt.preferredUserName(userName)
                .subject(subject)
                .groups(groups)
                .issuer("https://server.example.com")
                .audience("https://service.example.com")
                .sign();
    }
}