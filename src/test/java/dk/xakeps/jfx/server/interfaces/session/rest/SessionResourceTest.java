package dk.xakeps.jfx.server.interfaces.session.rest;

import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GameProfileDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GetOrCreateRequest;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.ProfileListDto;
import dk.xakeps.jfx.server.interfaces.session.facade.dto.JoinServerRequest;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.smallrye.jwt.build.Jwt;
import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
public class SessionResourceTest {
    private static final String USER = "admin";

    @Test
//    @TestSecurity(user = USER, roles = {"USER", "JOIN_GAME"})
//    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void join() {
        String accessToken = getAccessToken(USER, Set.of("USER", "JOIN_GAME"));
        ProfileListDto content = RestAssured.given().auth().oauth2(accessToken)
                .body(new GetOrCreateRequest(true, true))
                .contentType(ContentType.JSON)
                .post("profile-service/profiles/getOrCreate")
                .then()
                .statusCode(200)
                .extract()
                .as(ProfileListDto.class);

        String serverId = UUID.randomUUID().toString();

        RestAssured.given()
                .body(new JoinServerRequest(accessToken, content.selectedProfile(), serverId))
                .contentType(ContentType.JSON)
                .post("session-service/join")
                .then()
                .statusCode(204);

        String name = null;
        for (GameProfileDto profile : content.profiles()) {
            if (content.selectedProfile().equals(profile.id())) {
                name = profile.name();
            }
        }
        assertNotNull(name, "selectedProfile not found");
        assertEquals(USER, name);

        RestAssured.given()
                .get("session-service/hasJoined?username=" + name + "&serverId=" + serverId)
                .then()
                .statusCode(200);

        GameProfileDto profileResult = RestAssured.given()
                .get("session-service/profile/" + content.selectedProfile() + "?unsigned=false")
                .then()
                .statusCode(200).extract()
                .as(GameProfileDto.class);

        assertEquals(content.selectedProfile(), profileResult.id());
        assertEquals(name, profileResult.name());
    }

    @Test
    public void joinNoAccess() {
        RestAssured.given().auth().oauth2(getAccessToken(USER, Set.of("USER")))
                .body(new JoinServerRequest("not-checked-yet", UUID.randomUUID(), ""))
                .contentType(ContentType.JSON)
                .post("session-service/join")
                .then()
                .statusCode(403);
    }

    private String getAccessToken(String userName, Set<String> groups) {
        return Jwt.preferredUserName(userName)
                .subject("randomSubject")
                .groups(groups)
                .issuer("https://server.example.com")
                .audience("https://service.example.com")
                .sign();
    }
}