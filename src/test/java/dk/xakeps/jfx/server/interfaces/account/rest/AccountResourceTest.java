package dk.xakeps.jfx.server.interfaces.account.rest;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
public class AccountResourceTest {
    @Test
    @TestSecurity(user = "admin", roles = {"CAN_SEE_USERS"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testFindUserByEmail() {
        String id = "20a8ecda-7f89-4429-a150-14e250db9cc6";
        String email = "admin@xakeps.dk";
        List<AccountInfo> accountInfos = RestAssured.given().get("account-service/find?email=" + email)
                .then()
                .statusCode(200)
                .extract()
                .as(new TypeRef<List<AccountInfo>>() {});
        assertEquals(1, accountInfos.size());
        assertEquals(id, accountInfos.get(0).id);
        assertEquals(email, accountInfos.get(0).email);
    }

    @Test
    @TestSecurity(user = "admin", roles = {"CAN_SEE_USERS"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testFindUserById() {
        String id = "20a8ecda-7f89-4429-a150-14e250db9cc6";
        String email = "admin@xakeps.dk";
        AccountInfo accountInfo = RestAssured.given().get("account-service/find/" + id)
                .then()
                .statusCode(200)
                .extract()
                .as(AccountInfo.class);
        assertEquals(id, accountInfo.id);
        assertEquals(email, accountInfo.email);
    }

    @Test
    @TestSecurity(user = "admin", roles = {"CAN_SEE_USERS"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testFindUsersByIds() {
        String id = "20a8ecda-7f89-4429-a150-14e250db9cc6";
        String email = "admin@xakeps.dk";
        List<AccountInfo> accountInfos = RestAssured.given().get("account-service/find?id=" + id)
                .then()
                .statusCode(200)
                .extract()
                .as(new TypeRef<List<AccountInfo>>() {});
        assertEquals(1, accountInfos.size());
        assertEquals(id, accountInfos.get(0).id);
        assertEquals(email, accountInfos.get(0).email);
    }

    public static record AccountInfo(String id, String email) {}
}