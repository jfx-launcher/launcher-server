package dk.xakeps.jfx.server.interfaces.version.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.server.domain.model.version.VersionManifest;
import dk.xakeps.jfx.server.domain.model.version.VersionManifestEntry;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
public class ManageVersionResourceTest {
    @Test
    @TestSecurity(user = "admin", roles = {"MODIFY_VERSION"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testUploadForge_1_7_10() throws URISyntaxException, IOException {
        URL forgeInstallerUrl = getClass().getResource("/forge-1.7.10-10.13.4.1614-1.7.10-installer.jar");
        Assertions.assertNotNull(forgeInstallerUrl, "/forge-1.7.10-10.13.4.1614-1.7.10-installer.jar not found");
        URI forgeInstallerUri = forgeInstallerUrl.toURI();
        RestAssured.given()
                .contentType(ContentType.MULTIPART)
                .multiPart(new File(forgeInstallerUri))
                .post("version-service/manage/upload/forge")
                .then()
                .statusCode(202);

        String version = "1.7.10-Forge10.13.4.1614-1.7.10";

        checkVersionPresentInManifest(version);

        JsonNode actual = RestAssured.given()
                .get("version-service/version/forge/" + version + ".json")
                .then()
                .statusCode(200)
                .extract()
                .as(JsonNode.class);
        URL expectedJsonUrl = getClass().getResource("/1.7.10-Forge10.13.4.1614-1.7.10.json");
        Assertions.assertNotNull(expectedJsonUrl, "/1.7.10-Forge10.13.4.1614-1.7.10.json not found");
        URI expectedJsonUri = expectedJsonUrl.toURI();
        try (BufferedReader reader = Files.newBufferedReader(Path.of(expectedJsonUri), StandardCharsets.UTF_8)) {
            JsonNode expected = new ObjectMapper().readTree(reader);
            assertEquals(expected, actual);
        }
    }

    @Test
    @TestSecurity(user = "admin", roles = {"MODIFY_VERSION"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testUploadForge_1_12_2() throws URISyntaxException, IOException {
        URL forgeInstallerUrl = getClass().getResource("/forge-1.12.2-14.23.5.2855-installer.jar");
        Assertions.assertNotNull(forgeInstallerUrl, "/forge-1.12.2-14.23.5.2855-installer.jar not found");
        URI forgeInstallerUri = forgeInstallerUrl.toURI();
        RestAssured.given()
                .contentType(ContentType.MULTIPART)
                .multiPart(new File(forgeInstallerUri))
                .post("version-service/manage/upload/forge")
                .then()
                .statusCode(202);

        String version = "1.12.2-forge-14.23.5.2855";

        checkVersionPresentInManifest(version);

        JsonNode actual = RestAssured.given()
                .get("version-service/version/forge/" + version + ".json")
                .then()
                .statusCode(200)
                .extract()
                .as(JsonNode.class);
        URL expectedJsonUrl = getClass().getResource("/1.12.2-forge-14.23.5.2855.json");
        Assertions.assertNotNull(expectedJsonUrl, "/1.12.2-forge-14.23.5.2855.json not found");
        URI expectedJsonUri = expectedJsonUrl.toURI();
        try (BufferedReader reader = Files.newBufferedReader(Path.of(expectedJsonUri), StandardCharsets.UTF_8)) {
            JsonNode expected = new ObjectMapper().readTree(reader);
            assertEquals(expected, actual);
        }
    }

    @Test
    @TestSecurity(user = "admin", roles = {"MODIFY_VERSION"})
    @OidcSecurity(claims = @Claim(key = "sub", value = "randomSubject"))
    public void testUploadForge_1_16_5() throws URISyntaxException, IOException {
        URL forgeInstallerUrl = getClass().getResource("/forge-1.16.5-36.2.5-installer.jar");
        Assertions.assertNotNull(forgeInstallerUrl, "/forge-1.16.5-36.2.5-installer.jar not found");
        URI forgeInstallerUri = forgeInstallerUrl.toURI();
        RestAssured.given()
                .contentType(ContentType.MULTIPART)
                .multiPart(new File(forgeInstallerUri))
                .post("version-service/manage/upload/forge")
                .then()
                .statusCode(202);

        String version = "1.16.5-forge-36.2.5";

        checkVersionPresentInManifest(version);

        byte[] bytes = RestAssured.given()
                .get("version-service/version/forge/" + version + ".jar")
                .then()
                .statusCode(200)
                .extract()
                .asByteArray();
        byte[] localFileBytes = Files.readAllBytes(Path.of(forgeInstallerUri));
        Assertions.assertEquals(localFileBytes.length, bytes.length);
    }

    private void checkVersionPresentInManifest(String version) {
        boolean foundVersion = false;
        VersionManifest as = RestAssured.get("version-service/version_manifest.json").as(VersionManifest.class);
        for (VersionManifestEntry entry : as.versions()) {
            if (entry.id().equals(version)) {
                foundVersion = true;
                break;
            }
        }
        assertTrue(foundVersion);
    }
}