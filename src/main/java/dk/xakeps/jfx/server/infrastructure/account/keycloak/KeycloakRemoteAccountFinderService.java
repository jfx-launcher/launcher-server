package dk.xakeps.jfx.server.infrastructure.account.keycloak;

import dk.xakeps.jfx.server.domain.model.account.RemoteAccountInfo;
import dk.xakeps.jfx.server.domain.service.RemoteAccountFinderService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of RemoteAccountFinderService
 * that queries Keycloak for given account info
 */
@ApplicationScoped
public class KeycloakRemoteAccountFinderService implements RemoteAccountFinderService {
    private final KeycloakClient keycloakClient;

    @Inject
    public KeycloakRemoteAccountFinderService(@RestClient KeycloakClient keycloakClient) {
        this.keycloakClient = keycloakClient;
    }

    @Override
    public Optional<RemoteAccountInfo> findAccountById(String id) {
        KeycloakAccount account = keycloakClient.findAccountById(id).orElse(null);

        return checkAndReturnAccount(account);
    }

    @Override
    public Optional<RemoteAccountInfo> findAccountByEmail(String email) {
        List<KeycloakAccount> accountByEmail = keycloakClient.findAccountByEmail(email, true);
        if (accountByEmail.size() != 1) {
            return Optional.empty();
        }

        return checkAndReturnAccount(accountByEmail.get(0));
    }

    @Override
    public List<RemoteAccountInfo> findAccountsByEmails(List<String> emails) {
        return emails.stream().map(this::findAccountByEmail)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    @Override
    public List<RemoteAccountInfo> findAccountsByIds(List<String> ids) {
        return ids.stream().map(this::findAccountById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private Optional<RemoteAccountInfo> checkAndReturnAccount(KeycloakAccount account) {
        if (account == null) {
            return Optional.empty();
        }
        if (!Boolean.TRUE.equals(account.emailVerified())
                || !Boolean.TRUE.equals(account.enabled())) {
            return Optional.empty();
        }
        return Optional.of(new RemoteAccountInfo(account.id(), account.email()));
    }
}
