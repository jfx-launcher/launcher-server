package dk.xakeps.jfx.server.infrastructure.version;

import dk.xakeps.jfx.server.domain.model.version.VersionManifest;
import dk.xakeps.jfx.server.domain.service.RemoteVersionListService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class RemoteVersionListServiceImpl implements RemoteVersionListService {
    private final RemoteVersionListClient client;

    @Inject
    public RemoteVersionListServiceImpl(@RestClient RemoteVersionListClient client) {
        this.client = client;
    }

    @Override
    public VersionManifest getVersionManifest() {
        return client.getVersionManifest();
    }
}
