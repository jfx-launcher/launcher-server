package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.session.Session;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class SessionRepo implements PanacheRepository<Session> {
    public Optional<Session> findByProfileId(UUID profileId) {
        return find("profileId", profileId).singleResultOptional();
    }
    public Optional<Session> findByUsernameAndServerId(String username, String serverId) {
        return find(
                "username = :username and serverId = :serverId",
                Parameters.with("username", username).and("serverId", serverId)
        ).singleResultOptional();
    }
}
