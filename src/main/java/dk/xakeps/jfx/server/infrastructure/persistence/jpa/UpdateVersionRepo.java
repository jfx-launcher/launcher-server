package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.update.UpdateVersionNotFoundException;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.domain.model.update.UpdateVersion;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import org.hibernate.jpa.QueryHints;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class UpdateVersionRepo implements PanacheRepository<UpdateVersion>, RepoBase<UpdateVersion> {
    public Optional<UpdateVersion> findByVersion(UpdatePackageType updatePackageType,
                                                 String version, String arch, String system) {
        return find("#UpdateVersion.select",
                Parameters.with("updatePackageType", updatePackageType)
                        .and("arch", arch)
                        .and("system", system)
                        .and("version", version)).singleResultOptional();
    }

    public List<UpdateVersion> listByType(UpdatePackageType updatePackageType) {
        return find("#UpdateVersion.selectByPackageType",
                Parameters.with("updatePackageType", updatePackageType))
                .list();
    }

    public Optional<UpdateVersion> findWithArtifacts(UpdatePackageType updatePackageType,
                                                     String version, String arch, String system) {
        EntityManager entityManager = getEntityManager();
        return toSingleResultOptional(entityManager.createNamedQuery("UpdateVersion.select", UpdateVersion.class)
                .setParameter("updatePackageType", updatePackageType)
                .setParameter("version", version)
                .setParameter("arch", arch)
                .setParameter("system", system)
                .setHint(QueryHints.HINT_FETCHGRAPH, entityManager.getEntityGraph("UpdateVersion.full"))
                .getResultList());
    }

    public void deleteVersion(UpdatePackageType updatePackageType,
                              String version, String arch, String system) {
        UpdateVersion updateVersion = findByVersion(updatePackageType, version, arch, system).orElse(null);
        if (updateVersion == null) {
            throw new UpdateVersionNotFoundException();
        }
        delete(updateVersion);
    }
}
