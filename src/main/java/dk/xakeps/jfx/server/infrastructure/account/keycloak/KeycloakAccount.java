package dk.xakeps.jfx.server.infrastructure.account.keycloak;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record KeycloakAccount(@JsonProperty("id") String id,
                              @JsonProperty("email") String email,
                              @JsonProperty("emailVerified") Boolean emailVerified,
                              @JsonProperty("enabled") Boolean enabled) {
}
