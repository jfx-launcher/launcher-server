package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.profile.Account;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.hibernate.jpa.QueryHints;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class AccountRepo implements PanacheRepository<Account>, RepoBase<Account> {
    public Optional<Account> findByExternalId(String externalId) {
        return find("externalId", externalId).singleResultOptional();
    }

    public Optional<Account> findByExternalIdWithProfilesAndProperties(String externalId) {
        List<Account> results = getEntityManager().createNamedQuery("Account.findByExternalId", Account.class)
                .setParameter("externalId", externalId)
                .setHint(QueryHints.HINT_FETCHGRAPH, getEntityManager().getEntityGraph("Account.withProfilesAndProperties"))
                .getResultList();
        return toSingleResultOptional(results);
    }
}
