package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.profile.GameProfile;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.RepoBase;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import org.hibernate.jpa.QueryHints;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@ApplicationScoped
public class GameProfileRepo implements PanacheRepository<GameProfile>, RepoBase<GameProfile> {
    public boolean existsByUuid(UUID uuid) {
        return count("uuid", uuid) >= 1;
    }

    public boolean existsByName(String name) {
        return count("name", name) >= 1;
    }

    public Optional<GameProfile> findByUuid(UUID uuid) {
        return find("uuid", uuid).singleResultOptional();
    }

    public Optional<GameProfile> findByUuidWithProperties(UUID uuid) {
        List<GameProfile> results = getEntityManager().createNamedQuery("GameProfile.findByUuid", GameProfile.class)
                .setHint(QueryHints.HINT_FETCHGRAPH, getEntityManager().getEntityGraph("GameProfile.withProperties"))
                .setParameter("uuid", uuid).getResultList();
        return toSingleResultOptional(results);
    }

    public Optional<GameProfile> findByName(String name) {
        return find("name", name).singleResultOptional();
    }

    public Optional<GameProfile> findByAccountExternalIdAndUuid(String externalId, UUID uuid) {
        return find(
                "uuid = :uuid and account.externalId = :externalId",
                Parameters.with("externalId", externalId).and("uuid", uuid)
        ).singleResultOptional();
    }

    public Optional<GameProfile> findByAccountExternalIdAndUuidWithProperties(String externalId, UUID uuid) {
        List<GameProfile> results = getEntityManager().createNamedQuery("GameProfile.findByUuidAndExternalId", GameProfile.class)
                .setHint(QueryHints.HINT_FETCHGRAPH, getEntityManager().getEntityGraph("GameProfile.withProperties"))
                .setParameter("uuid", uuid)
                .setParameter("externalId", externalId)
                .getResultList();
        return toSingleResultOptional(results);
    }

    public Optional<GameProfile> findByAccountExternalIdAndName(String externalId, String name) {
        return find(
                "name = :name and account.externalId = :externalId",
                Parameters.with("externalId", externalId).and("name", name)
        ).singleResultOptional();
    }

    public List<GameProfile> findByNames(Set<String> names) {
        return find("select p from GameProfile p where p.name in :names",
                Parameters.with("names", names)).list();
    }
}
