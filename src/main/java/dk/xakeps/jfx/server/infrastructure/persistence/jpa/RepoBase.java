package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import javax.persistence.NonUniqueResultException;
import java.util.List;
import java.util.Optional;

public interface RepoBase<T> {
    default Optional<T> toSingleResultOptional(List<T> results) {
        if (results.isEmpty()) {
            return Optional.empty();
        } else if (results.size() > 1) {
            throw new NonUniqueResultException();
        } else {
            return Optional.of(results.get(0));
        }
    }
}