package dk.xakeps.jfx.server.infrastructure.version;

import dk.xakeps.jfx.server.domain.model.version.VersionManifest;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Client to access version_manifest.json
 */
@ApplicationScoped
@RegisterRestClient(baseUri = "https://launchermeta.mojang.com/mc/game")
public interface RemoteVersionListClient {
    @GET
    @Path("version_manifest.json")
    VersionManifest getVersionManifest();
}
