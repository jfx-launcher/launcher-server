package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.update.Platform;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class PlatformRepo implements PanacheRepository<Platform> {
    public Optional<Platform> findByIds(String arch, String system) {
        return find("arch = :arch and system = :system",
                Parameters.with("arch", arch).and("system", system))
                .singleResultOptional();
    }
}
