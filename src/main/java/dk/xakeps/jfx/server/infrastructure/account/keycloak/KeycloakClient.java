package dk.xakeps.jfx.server.infrastructure.account.keycloak;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import java.util.List;
import java.util.Optional;

/**
 * Some simple client to access keycloak rest api
 */
@OidcClientFilter
@RegisterRestClient(configKey = "account-finder.keycloak")
public interface KeycloakClient {
    @GET
    @Path("users/{id}")
    Optional<KeycloakAccount> findAccountById(@PathParam("id") String id);

    @GET
    @Path("users")
    List<KeycloakAccount> findAccountByEmail(@QueryParam("email") String email,
                                             @QueryParam("exact") @DefaultValue("true") boolean exact);
}
