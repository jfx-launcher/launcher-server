package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.modpack.ModPack;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import org.hibernate.jpa.QueryHints;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;
import java.util.stream.Stream;

@ApplicationScoped
public class ModPackRepo implements PanacheRepository<ModPack>, RepoBase<ModPack> {
    public Optional<ModPack> findByModPackIdAndMinecraftVersion(String modPackId) {
        return find("#ModPack.select",
                Parameters.with("modPackId", modPackId))
                .singleResultOptional();
    }

    public Optional<ModPack> findByModPackIdAndMinecraftVersionWithManager(String modPackId, String managerId) {
        return find("#ModPack.selectSecured",
                Parameters.with("modPackId", modPackId)
                        .and("managerId", managerId))
                .singleResultOptional();
    }

    public Stream<ModPack> findAllWithVersionsWithoutArtifact() {
        return getEntityManager().createNamedQuery("ModPack.selectAll", ModPack.class)
                .setHint(QueryHints.HINT_FETCHGRAPH,
                        getEntityManager().createEntityGraph("ModPack.withVersionsWithoutArtifacts"))
                .getResultStream();

    }

    public Optional<ModPack> findModPackWithManagers(String modPackId) {
        return toSingleResultOptional(getEntityManager().createNamedQuery("ModPack.select", ModPack.class)
                .setHint(QueryHints.HINT_FETCHGRAPH,
                        getEntityManager().createEntityGraph("ModPack.withManagers"))
                .setParameter("modPackId", modPackId)
                .getResultList());
    }

    public boolean deleteModPack(String modPackId) {
        return delete("modPackId = :modPackId",
                Parameters.with("modPackId", modPackId)) > 0;
    }
}
