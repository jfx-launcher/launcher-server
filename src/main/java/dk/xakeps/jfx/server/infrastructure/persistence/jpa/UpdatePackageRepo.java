package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageNotFoundException;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackage;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import org.hibernate.jpa.QueryHints;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class UpdatePackageRepo implements PanacheRepository<UpdatePackage>, RepoBase<UpdatePackage> {
    public boolean exists(UpdatePackageType updatePackageType, String arch, String system) {
        return count("updatePackageType = :updatePackageType and platform.arch = :arch and platform.system = :system",
                Parameters.with("updatePackageType", updatePackageType)
                        .and("arch", arch)
                        .and("system", system)) > 0;
    }

    public Optional<UpdatePackage> find(UpdatePackageType updatePackageType, String  arch, String system) {
        return find("updatePackageType = :updatePackageType and platform.arch = :arch and platform.system = :system",
                Parameters.with("updatePackageType", updatePackageType)
                        .and("arch", arch)
                        .and("system", system))
                .singleResultOptional();
    }

    public void deleteByPackageType(UpdatePackageType updatePackageType) {
        delete("updatePackageType = :updatePackageType", updatePackageType);
    }

    public void deletePackage(UpdatePackageType updatePackageType, String arch, String system) {
        UpdatePackage updatePackage = find(updatePackageType, arch, system).orElse(null);
        if (updatePackage == null) {
            throw new UpdatePackageNotFoundException();
        }
        delete(updatePackage);
    }

    public List<UpdatePackage> listByType(UpdatePackageType updatePackageType) {
        EntityManager entityManager = getEntityManager();
        return entityManager.createNamedQuery("UpdatePackage.selectByPackageType", UpdatePackage.class)
                .setParameter("updatePackageType", updatePackageType)
                .setHint(QueryHints.HINT_FETCHGRAPH, entityManager.getEntityGraph("UpdatePackage.withPackageVersions"))
                .getResultList();
    }
}
