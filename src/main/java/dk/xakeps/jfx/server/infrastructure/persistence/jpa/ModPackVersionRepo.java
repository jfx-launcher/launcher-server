package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.modpack.ModPackVersion;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import java.time.Instant;
import java.util.Optional;

@ApplicationScoped
public class ModPackVersionRepo implements PanacheRepository<ModPackVersion> {
    public Optional<ModPackVersion> findByUploadTime(String modPackId, Instant uploadTime) {
        return find("#ModPackVersion.select",
                Parameters.with("modPackId", modPackId)
                        .and("uploadTime", uploadTime)).singleResultOptional();
    }
}
