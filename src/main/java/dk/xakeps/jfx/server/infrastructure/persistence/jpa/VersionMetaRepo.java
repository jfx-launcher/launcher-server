package dk.xakeps.jfx.server.infrastructure.persistence.jpa;

import dk.xakeps.jfx.server.domain.model.version.VersionMeta;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class VersionMetaRepo implements PanacheRepository<VersionMeta> {
}
