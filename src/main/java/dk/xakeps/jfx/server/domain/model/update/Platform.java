package dk.xakeps.jfx.server.domain.model.update;

import javax.persistence.*;

@Entity
@Table(name = "platform", uniqueConstraints = @UniqueConstraint(name = "UK_platform", columnNames = {"arch", "system"}))
public class Platform {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "arch", nullable = false)
    private String arch;
    @Column(name = "system", nullable = false)
    private String system;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArch() {
        return arch;
    }

    public void setArch(String arch) {
        this.arch = arch;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }
}
