package dk.xakeps.jfx.server.domain.model.profile.property.texture;

import dk.xakeps.jfx.server.domain.model.profile.property.PropertyValue;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Entity
public class TexturesPropertyValue extends PropertyValue {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "propertyValue")
    @MapKey(name = "textureType")
    @MapKeyEnumerated(EnumType.STRING)
    private Map<TextureType, Texture> textures;

    public TexturesPropertyValue() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Map<TextureType, Texture> getTextures() {
        return textures;
    }

    public void setTextures(Map<TextureType, Texture> textures) {
        this.textures = textures;
    }

    public void addTexture(Texture texture) {
        if (textures == null) {
            textures = new HashMap<>(1);
        }
        texture.setPropertyValue(this);
        textures.put(texture.getTextureType(), texture);
    }

    public Optional<Texture> getTexture(TextureType textureType) {
        if (textures != null) {
            return Optional.ofNullable(textures.get(textureType));
        }
        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TexturesPropertyValue that = (TexturesPropertyValue) o;
        return Objects.equals(textures, that.textures);
    }

    @Override
    public int hashCode() {

        return Objects.hash(textures);
    }

    @Override
    public String toString() {
        return "TexturesPropertyValue{" +
                "id=" + id +
                ", textures=" + textures +
                '}';
    }
}