package dk.xakeps.jfx.server.domain.model.modpack;

public class IllegalIgnoredPathException extends RuntimeException {
    public IllegalIgnoredPathException() {
    }

    public IllegalIgnoredPathException(String message) {
        super(message);
    }

    public IllegalIgnoredPathException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalIgnoredPathException(Throwable cause) {
        super(cause);
    }

    public IllegalIgnoredPathException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
