package dk.xakeps.jfx.server.domain.model.modpack;

import dk.xakeps.jfx.server.domain.model.shared.NotFoundException;

public class ModPackVersionNotFoundException extends NotFoundException {
    public ModPackVersionNotFoundException() {
    }

    public ModPackVersionNotFoundException(String message) {
        super(message);
    }

    public ModPackVersionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModPackVersionNotFoundException(Throwable cause) {
        super(cause);
    }

    public ModPackVersionNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
