package dk.xakeps.jfx.server.domain.model.update;

public class UpdateVersionUpdateAlreadyExistsException extends UpdateAlreadyExistsException {
    public UpdateVersionUpdateAlreadyExistsException() {
    }

    public UpdateVersionUpdateAlreadyExistsException(String message) {
        super(message);
    }

    public UpdateVersionUpdateAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateVersionUpdateAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public UpdateVersionUpdateAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
