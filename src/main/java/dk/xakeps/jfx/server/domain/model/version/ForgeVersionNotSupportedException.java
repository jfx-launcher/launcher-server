package dk.xakeps.jfx.server.domain.model.version;

public class ForgeVersionNotSupportedException extends VersionProcessingException {
    public ForgeVersionNotSupportedException() {
    }

    public ForgeVersionNotSupportedException(String message) {
        super(message);
    }

    public ForgeVersionNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForgeVersionNotSupportedException(Throwable cause) {
        super(cause);
    }

    public ForgeVersionNotSupportedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
