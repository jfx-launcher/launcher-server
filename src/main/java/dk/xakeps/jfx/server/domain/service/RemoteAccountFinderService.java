package dk.xakeps.jfx.server.domain.service;

import dk.xakeps.jfx.server.domain.model.account.RemoteAccountInfo;

import java.util.List;
import java.util.Optional;

/**
 * Finds an account in a local or remote service
 */
public interface RemoteAccountFinderService {
    Optional<RemoteAccountInfo> findAccountById(String id);
    Optional<RemoteAccountInfo> findAccountByEmail(String email);
    List<RemoteAccountInfo> findAccountsByEmails(List<String> emails);
    List<RemoteAccountInfo> findAccountsByIds(List<String> ids);
}
