package dk.xakeps.jfx.server.domain.model.version;

public class VersionProcessingException extends RuntimeException {
    public VersionProcessingException() {
    }

    public VersionProcessingException(String message) {
        super(message);
    }

    public VersionProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public VersionProcessingException(Throwable cause) {
        super(cause);
    }

    public VersionProcessingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
