package dk.xakeps.jfx.server.domain.model.update;

public class UpdatePackageUpdateAlreadyExistsException extends UpdateAlreadyExistsException {
    public UpdatePackageUpdateAlreadyExistsException() {
    }

    public UpdatePackageUpdateAlreadyExistsException(String message) {
        super(message);
    }

    public UpdatePackageUpdateAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdatePackageUpdateAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public UpdatePackageUpdateAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
