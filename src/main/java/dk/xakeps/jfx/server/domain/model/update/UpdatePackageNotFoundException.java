package dk.xakeps.jfx.server.domain.model.update;

import dk.xakeps.jfx.server.domain.model.shared.NotFoundException;

public class UpdatePackageNotFoundException extends NotFoundException {

    public UpdatePackageNotFoundException() {
    }

    public UpdatePackageNotFoundException(String message) {
        super(message);
    }

    public UpdatePackageNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdatePackageNotFoundException(Throwable cause) {
        super(cause);
    }

    public UpdatePackageNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
