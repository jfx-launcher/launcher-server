package dk.xakeps.jfx.server.domain.model.version;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.net.URI;
import java.time.OffsetDateTime;

public record VersionManifestEntry(@JsonProperty("id") String id,
                                   @JsonProperty("type") String versionType,
                                   @JsonProperty("url") URI uri,
                                   @JsonProperty("time") @JsonSerialize(using = OffsetDateTimeSerializer.class) OffsetDateTime updateTime,
                                   @JsonProperty("releaseTime") @JsonSerialize(using = OffsetDateTimeSerializer.class) OffsetDateTime releaseTime,
                                   @JsonProperty("_versionConfig") @JsonInclude(JsonInclude.Include.NON_NULL) VersionConfig versionConfig) {
}
