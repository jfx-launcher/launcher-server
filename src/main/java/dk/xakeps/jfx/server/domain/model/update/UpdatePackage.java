package dk.xakeps.jfx.server.domain.model.update;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "update_package", uniqueConstraints = @UniqueConstraint(
        name = "UK_update_package",
        columnNames = {"platform_id", "update_package_type"}))
@NamedQueries({
        @NamedQuery(name = "UpdatePackage.selectByPackageType",
                query = "select u from UpdatePackage u where u.updatePackageType = :updatePackageType")
})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "UpdatePackage.withPackageVersions",
                attributeNodes = @NamedAttributeNode("packageVersions"))
})
public class UpdatePackage {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "platform_id",
            foreignKey = @ForeignKey(name = "FK_update_package_to_platform"))
    private Platform platform;
    @Enumerated(EnumType.STRING)
    @Column(name = "update_package_type", nullable = false)
    private UpdatePackageType updatePackageType;

    @OneToMany(mappedBy = "updatePackage", cascade = CascadeType.ALL)
    private Set<UpdateVersion> packageVersions;

    @OneToOne
    @JoinColumn(name = "enabled_update_version_id",
            foreignKey = @ForeignKey(name = "FK_update_package_to_enabled_update_version"))
    private UpdateVersion enabledVersion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public UpdatePackageType getUpdatePackageType() {
        return updatePackageType;
    }

    public void setUpdatePackageType(UpdatePackageType updatePackageType) {
        this.updatePackageType = updatePackageType;
    }

    public Set<UpdateVersion> getPackageVersions() {
        return packageVersions;
    }

    public void setPackageVersions(Set<UpdateVersion> packageVersions) {
        this.packageVersions = packageVersions;
    }

    public UpdateVersion getEnabledVersion() {
        return enabledVersion;
    }

    public void setEnabledVersion(UpdateVersion enabledVersion) {
        this.enabledVersion = enabledVersion;
    }
}
