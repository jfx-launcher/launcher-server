package dk.xakeps.jfx.server.domain.model.version;

import com.fasterxml.jackson.annotation.JsonProperty;

public record LatestVersions(@JsonProperty("release") String release,
                             @JsonProperty("snapshot") String snapshot) {
}
