package dk.xakeps.jfx.server.domain.model.account;

/**
 * This data is fetched from a service that handles accounts
 * for example Keycloak
 */
public record RemoteAccountInfo(String id, String email) {
}
