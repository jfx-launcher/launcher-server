package dk.xakeps.jfx.server.domain.model.profile;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name = "account", uniqueConstraints = {
        @UniqueConstraint(name = "UK_id", columnNames = "id"),
        @UniqueConstraint(name = "UK_external_id", columnNames = "external_id")
})
@NamedQueries({
        @NamedQuery(name = "Account.findByExternalId", query = "select a from Account a where a.externalId = :externalId")
})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Account.withProfiles", attributeNodes = {
                @NamedAttributeNode("gameProfiles"),
                @NamedAttributeNode("selectedProfile")
        }),
        @NamedEntityGraph(name = "Account.withProfilesAndProperties", attributeNodes = {
                @NamedAttributeNode(value = "gameProfiles", subgraph = "profilesSubgraph"),
                @NamedAttributeNode(value = "selectedProfile", subgraph = "selectedProfileSubgraph")
        }, subgraphs = {
                @NamedSubgraph(name = "profilesSubgraph", attributeNodes = {
                        @NamedAttributeNode("properties")
                }),
                @NamedSubgraph(name = "selectedProfileSubgraph", attributeNodes = {
                        @NamedAttributeNode("properties")
                })
        })
})
public class Account {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "external_id", nullable = false)
    private String externalId;

    @MapKey(name = "uuid")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "account")
    private Map<UUID, GameProfile> gameProfiles;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "selected_profile_id",
            foreignKey = @ForeignKey(name = "FK_account_to_selected_profile"))
    private GameProfile selectedProfile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Map<UUID, GameProfile> getGameProfiles() {
        return gameProfiles;
    }

    public void setGameProfiles(Map<UUID, GameProfile> gameProfiles) {
        this.gameProfiles = gameProfiles;
    }

    public void addGameProfile(GameProfile profile) {
        if (gameProfiles == null) {
            gameProfiles = new HashMap<>(1);
        }
        gameProfiles.put(profile.getUuid(), profile);
        profile.setAccount(this);
    }

    public GameProfile getSelectedProfile() {
        return selectedProfile;
    }

    public void setSelectedProfile(GameProfile selectedProfile) {
        this.selectedProfile = selectedProfile;
    }
}
