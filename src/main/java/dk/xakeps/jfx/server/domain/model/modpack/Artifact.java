package dk.xakeps.jfx.server.domain.model.modpack;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Artifact {
    @Column(name = "file", nullable = false)
    private String file;
    @Column(name = "size", nullable = false)
    private long size;
    @Column(name = "hash", length = 20, nullable = false, columnDefinition = "VARBINARY(20)")
    private byte[] hash;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }
}
