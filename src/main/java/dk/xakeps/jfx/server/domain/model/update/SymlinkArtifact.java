package dk.xakeps.jfx.server.domain.model.update;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("symlink")
public class SymlinkArtifact extends Artifact {
    @Column(name = "target")
    private String target;
    @Column(name = "source")
    private String source;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
