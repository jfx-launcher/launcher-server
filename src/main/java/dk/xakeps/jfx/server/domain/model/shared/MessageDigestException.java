package dk.xakeps.jfx.server.domain.model.shared;

public class MessageDigestException extends RuntimeException {
    public MessageDigestException() {
    }

    public MessageDigestException(String message) {
        super(message);
    }

    public MessageDigestException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageDigestException(Throwable cause) {
        super(cause);
    }

    public MessageDigestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
