package dk.xakeps.jfx.server.domain.model.version;

public enum InstallType {
    FORGE_OLD("forge", Extension.JSON),
    FORGE_MODERN("forge", Extension.JAR);

    private final String type;
    private final Extension extension;

    InstallType(String type, Extension extension) {
        this.type = type;
        this.extension = extension;
    }

    public String getType() {
        return type;
    }

    public Extension getExtension() {
        return extension;
    }

    public enum Extension {
        JAR("jar"), JSON("json");

        private final String asString;

        Extension(String asString) {
            this.asString = asString;
        }

        public String getAsString() {
            return asString;
        }
    }
}
