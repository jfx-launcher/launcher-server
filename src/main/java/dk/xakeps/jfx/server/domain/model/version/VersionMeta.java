package dk.xakeps.jfx.server.domain.model.version;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "version_meta", uniqueConstraints = @UniqueConstraint(name = "UK_version_id", columnNames = "version_id"))
public class VersionMeta {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "version_id", nullable = false)
    private String versionId;
    @Column(name = "version_type")
    private String versionType;
    @Column(name = "update_time")
    private OffsetDateTime updateTime;
    @Column(name = "release_time")
    private OffsetDateTime releaseTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "install_type")
    private InstallType installType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getVersionType() {
        return versionType;
    }

    public void setVersionType(String versionType) {
        this.versionType = versionType;
    }

    public OffsetDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(OffsetDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public OffsetDateTime getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(OffsetDateTime releaseTime) {
        this.releaseTime = releaseTime;
    }

    public InstallType getInstallType() {
        return installType;
    }

    public void setInstallType(InstallType installType) {
        this.installType = installType;
    }
}
