package dk.xakeps.jfx.server.domain.model.version;

public enum VersionType {
    RELEASE, SNAPSHOT
}
