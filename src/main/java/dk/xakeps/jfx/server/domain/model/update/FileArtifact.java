package dk.xakeps.jfx.server.domain.model.update;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("file")
public class FileArtifact extends Artifact {
    @Column(name = "file")
    private String file;
    @Column(name = "hash", length = 20, columnDefinition = "VARBINARY(20)")
    private byte[] hash;
    @Column(name = "size")
    private long size;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
