package dk.xakeps.jfx.server.domain.model.profile.property;

import dk.xakeps.jfx.server.domain.model.profile.GameProfile;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "property_value")
@Inheritance
public abstract class PropertyValue {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "timestamp", nullable = false)
    private Instant timestamp;

    @OneToOne
    @JoinColumn(name = "game_profile_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_property_value_to_game_profile"))
    private GameProfile gameProfile;

    @OneToOne
    @JoinColumn(name = "property_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_property_value_to_property"))
    private Property property;

    public PropertyValue() {
        this.timestamp = Instant.now();
    }

    protected PropertyValue(Instant timestamp, GameProfile gameProfile, Property property) {
        this.timestamp = timestamp;
        this.gameProfile = gameProfile;
        this.property = property;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public GameProfile getGameProfile() {
        return gameProfile;
    }

    public void setGameProfile(GameProfile gameProfile) {
        this.gameProfile = gameProfile;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
}
