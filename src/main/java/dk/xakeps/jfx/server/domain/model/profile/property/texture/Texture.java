package dk.xakeps.jfx.server.domain.model.profile.property.texture;

import dk.xakeps.jfx.server.domain.model.profile.TextureException;
import org.mp4parser.tools.Hex;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "texture")
public class Texture {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "hash", length = 20, nullable = false, columnDefinition = "VARBINARY(20)")
    private byte[] hash;

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = "metadata_key", length = 32)
    @MapKeyEnumerated(EnumType.STRING)
    @Column(name = "metadata_value", length = 64, nullable = false)
    @CollectionTable(name = "texture_metadata",
            joinColumns = @JoinColumn(name = "texture_id"),
            foreignKey = @ForeignKey(name = "FK_texture_metadata_to_texture"))
    private Map<TextureMetadata, String> metadata;

    @Column(name = "texture_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private TextureType textureType;

    @ManyToOne
    @JoinColumn(name = "property_value_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_texture_to_property_value"))
    private TexturesPropertyValue propertyValue;

    public Texture() {
        this.metadata = new HashMap<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        if (hash.length > 20) {
            throw new TextureException("Hash can't be larger than 20 bytes");
        }
        this.hash = hash;
    }

    public Map<TextureMetadata, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<TextureMetadata, String> metadata) {
        this.metadata = metadata;
    }

    public TextureType getTextureType() {
        return textureType;
    }

    public void setTextureType(TextureType textureType) {
        this.textureType = textureType;
    }

    public TexturesPropertyValue getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(TexturesPropertyValue propertyValue) {
        this.propertyValue = propertyValue;
    }

    public void addMetadata(TextureMetadata metadata, Object o) {
        if (o.getClass().isInstance(metadata.getType())) {
            throw new TextureException(String.format("Wrong value for metadata. Required: %s, found %s", metadata.getType(), o.getClass()));
        }
        this.metadata.put(metadata, metadata.toString(o));
    }

    @Override
    public String toString() {
        return "Texture{" +
                "id=" + id +
                ", hash=" + Hex.encodeHex(hash) +
                ", metadata=" + metadata +
                ", textureType=" + textureType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Texture texture = (Texture) o;
        return Arrays.equals(hash, texture.hash) && Objects.equals(metadata, texture.metadata) && textureType == texture.textureType;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(metadata, textureType);
        result = 31 * result + Arrays.hashCode(hash);
        return result;
    }
}
