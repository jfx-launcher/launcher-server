package dk.xakeps.jfx.server.domain.model.update;

import javax.persistence.*;

@Entity
@Table(name = "update_artifact")
@DiscriminatorColumn(name = "type")
public class Artifact {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
