package dk.xakeps.jfx.server.domain.model.modpack;

import dk.xakeps.jfx.server.domain.model.profile.Account;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "mod_pack",
        uniqueConstraints = @UniqueConstraint(name = "UK_mod_pack", columnNames = {"mod_pack_id"})
)

@NamedQueries({
        @NamedQuery(name = "ModPack.selectSecured",
                query = "select m from ModPack m join m.managers manager where m.modPackId = :modPackId and manager.externalId = :managerId"),
        @NamedQuery(name = "ModPack.select", query = "select m from ModPack m where m.modPackId = :modPackId"),
        @NamedQuery(name = "ModPack.selectAll", query = "select m from ModPack m")
})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "ModPack.withVersionsWithoutArtifacts", attributeNodes = {
                @NamedAttributeNode("versions")
        }),
        @NamedEntityGraph(name = "ModPack.withManagers", attributeNodes = {
                @NamedAttributeNode("managers")
        })
})
public class ModPack {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "mod_pack_id", nullable = false, updatable = false)
    private String modPackId;

    @OrderBy("uploadTime asc")
    @OneToMany(mappedBy = "modPack", cascade = CascadeType.ALL)
    private List<ModPackVersion> versions;

    @OneToOne
    @JoinColumn(name = "mod_pack_version_id",
            foreignKey = @ForeignKey(name = "FK_mod_pack_to_enabled_mod_pack_version"))
    private ModPackVersion enabledModPack;

    @ManyToMany
    @JoinTable(name = "mod_pack_manager",
            joinColumns = @JoinColumn(name = "mod_pack_id", foreignKey = @ForeignKey(name = "FK_mod_pack_manager_to_mod_pack")),
            inverseJoinColumns = @JoinColumn(name = "account_id", foreignKey = @ForeignKey(name = "FK_mod_pack_manager_to_account")))
    private Set<Account> managers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModPackId() {
        return modPackId;
    }

    public void setModPackId(String modPackId) {
        this.modPackId = modPackId;
    }

    public List<ModPackVersion> getVersions() {
        return versions;
    }

    public void setVersions(List<ModPackVersion> versions) {
        this.versions = versions;
    }

    public ModPackVersion getEnabledModPack() {
        return enabledModPack;
    }

    public void setEnabledModPack(ModPackVersion enabledModPack) {
        this.enabledModPack = enabledModPack;
    }

    public Set<Account> getManagers() {
        return managers;
    }

    public void setManagers(Set<Account> managers) {
        this.managers = managers;
    }
}
