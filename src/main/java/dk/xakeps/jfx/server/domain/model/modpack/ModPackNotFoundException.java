package dk.xakeps.jfx.server.domain.model.modpack;

import dk.xakeps.jfx.server.domain.model.shared.NotFoundException;

public class ModPackNotFoundException extends NotFoundException {
    public ModPackNotFoundException() {
    }

    public ModPackNotFoundException(String message) {
        super(message);
    }

    public ModPackNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModPackNotFoundException(Throwable cause) {
        super(cause);
    }

    public ModPackNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
