package dk.xakeps.jfx.server.domain.model.version;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record VersionManifest(@JsonProperty("latest") LatestVersions latestVersions,
                              @JsonProperty("versions") List<VersionManifestEntry> versions) {
}
