package dk.xakeps.jfx.server.domain.model.update;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "update_version")
@NamedQueries({
        @NamedQuery(name = "UpdateVersion.select",
                query = "select v from UpdateVersion v where v.version = :version and v.updatePackage.updatePackageType = :updatePackageType and v.updatePackage.platform.arch = :arch and v.updatePackage.platform.system = :system"),
        @NamedQuery(name = "UpdateVersion.selectByPackageType",
                query = "select v from UpdateVersion v where v.updatePackage.updatePackageType = :updatePackageType")
})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "UpdateVersion.full", attributeNodes = {
                @NamedAttributeNode("artifacts"),
                @NamedAttributeNode(value = "updatePackage",
                        subgraph = "platformSubGraph")
        }, subgraphs = {
                @NamedSubgraph(name = "platformSubGraph", attributeNodes = @NamedAttributeNode(value = "platform"))
        })
})
public class UpdateVersion {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "version", nullable = false)
    private String version;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "artifact_id",
            foreignKey = @ForeignKey(name = "FK_artifact_id_to_update_version"))
    private List<Artifact> artifacts;

    @ManyToOne
    @JoinColumn(name = "update_package_id",
            foreignKey = @ForeignKey(name = "FK_update_package_version_to_update_package"))
    private UpdatePackage updatePackage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Artifact> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(List<Artifact> artifacts) {
        this.artifacts = artifacts;
    }

    public UpdatePackage getUpdatePackage() {
        return updatePackage;
    }

    public void setUpdatePackage(UpdatePackage updatePackage) {
        this.updatePackage = updatePackage;
    }
}
