package dk.xakeps.jfx.server.domain.model.profile.property.texture;

import java.util.Locale;

public enum TextureMetadata {
    MODEL(SkinTextureModel.class) {
        @Override
        public String toString(Object val) {
            return val.toString().toLowerCase(Locale.ROOT);
        }
    };

    private final Class<?> type;

    TextureMetadata(Class<?> type) {
        this.type = type;
    }

    public Class<?> getType() {
        return type;
    }

    public abstract String toString(Object val);
}
