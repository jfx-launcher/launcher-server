package dk.xakeps.jfx.server.domain.model.version;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class OffsetDateTimeDeserializer extends FromStringDeserializer<OffsetDateTime> {
    private static final DateTimeFormatter ISO_8601_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ssxxx")
            .withZone(ZoneId.of("UTC"));

    protected OffsetDateTimeDeserializer() {
        super(OffsetDateTime.class);
    }

    @Override
    protected OffsetDateTime _deserialize(String value, DeserializationContext ctxt) throws IOException {
        String substring = value.substring(value.length() - 4);
        if (substring.contains(":")) {
            return OffsetDateTime.parse(value);
        } else {
            String offset = value.substring(value.length() - 2);
            String time = value.substring(0, value.length() - 2);

            return OffsetDateTime.parse(time + ":" + offset);
        }
    }
}
