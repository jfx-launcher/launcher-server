package dk.xakeps.jfx.server.domain.model.update;

public enum UpdatePackageType {
    JAVA, LAUNCHER, UPDATER
}
