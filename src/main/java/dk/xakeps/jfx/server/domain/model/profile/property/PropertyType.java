package dk.xakeps.jfx.server.domain.model.profile.property;

import dk.xakeps.jfx.server.domain.model.profile.property.texture.TexturesPropertyValue;

public enum PropertyType {
    TEXTURES(TexturesPropertyValue.class);

    private final Class<?>[] acceptableClasses;

    PropertyType(Class<?>... acceptableClasses) {
        this.acceptableClasses = acceptableClasses;
    }

    public boolean accepts(Class<?> clazz) {
        for (Class<?> acceptableClass : acceptableClasses) {
            if (acceptableClass.equals(clazz)) {
                return true;
            }
        }
        return false;
    }
}