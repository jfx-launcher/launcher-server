package dk.xakeps.jfx.server.domain.model.profile;

public class GameProfileException extends RuntimeException {
    public GameProfileException() {
    }

    public GameProfileException(String message) {
        super(message);
    }

    public GameProfileException(String message, Throwable cause) {
        super(message, cause);
    }

    public GameProfileException(Throwable cause) {
        super(cause);
    }

    public GameProfileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
