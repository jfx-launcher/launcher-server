package dk.xakeps.jfx.server.domain.model.profile;

import dk.xakeps.jfx.server.domain.model.profile.property.PropertyType;
import dk.xakeps.jfx.server.domain.model.profile.property.Property;

import javax.persistence.*;
import java.time.Instant;
import java.util.*;

@Entity
@Table(name = "game_profile", uniqueConstraints = {
        @UniqueConstraint(name = "UK_uuid", columnNames = "uuid"),
        @UniqueConstraint(name = "UK_name", columnNames = "name")
})
@NamedQueries({
        @NamedQuery(name = "GameProfile.findByNameAndExternalId",
                query = "SELECT p from GameProfile p where p.name = :name and p.account.externalId = :externalId"),
        @NamedQuery(name = "GameProfile.findByUuid",
                query = "SELECT p from GameProfile p where p.uuid = :uuid"),
        @NamedQuery(name = "GameProfile.findByUuidAndExternalId",
                query = "SELECT p from GameProfile p where p.uuid = :uuid and p.account.externalId = :externalId")
})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "GameProfile.withProperties", attributeNodes = {
                @NamedAttributeNode("properties")
        })
})
public class GameProfile {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "uuid", updatable = false, nullable = false, length = 16)
    private UUID uuid;

    @Column(name = "name", length = 16, nullable = false)
    private String name;

    @Column(name = "created_at", updatable = false, nullable = false)
    private Instant createdAt;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "profile")
    @MapKey(name = "propertyType")
    @MapKeyEnumerated(EnumType.STRING)
    private Map<PropertyType, Property> properties;

    @ManyToOne
    @JoinColumn(name = "account_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_game_profile_to_account"))
    private Account account;

    public GameProfile() {
        this.createdAt = Instant.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Map<PropertyType, Property> getProperties() {
        return properties;
    }

    public void setProperties(Map<PropertyType, Property> properties) {
        this.properties = properties;
    }

    public void addProperty(Property property) {
        if (properties == null) {
            properties = new HashMap<>(1);
        }
        property.setProfile(this);
        properties.put(property.getPropertyType(), property);
    }

    public Optional<Property> getProperty(PropertyType propertyType) {
        if (properties != null) {
            return Optional.ofNullable(properties.get(propertyType));
        }
        return Optional.empty();
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameProfile that = (GameProfile) o;
        return Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {

        return Objects.hash(uuid);
    }

    @Override
    public String toString() {
        return "GameProfile{" +
                "id=" + id +
                ", uuid=" + uuid +
                ", name='" + name + '\'' +
                ", createdAt=" + createdAt +
                ", properties=" + properties +
                '}';
    }
}
