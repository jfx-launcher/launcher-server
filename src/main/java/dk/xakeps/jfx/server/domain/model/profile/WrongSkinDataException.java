package dk.xakeps.jfx.server.domain.model.profile;

import dk.xakeps.jfx.server.domain.model.shared.WrongDataException;

public class WrongSkinDataException extends WrongDataException {
    public WrongSkinDataException() {
    }

    public WrongSkinDataException(String message) {
        super(message);
    }

    public WrongSkinDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongSkinDataException(Throwable cause) {
        super(cause);
    }

    public WrongSkinDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
