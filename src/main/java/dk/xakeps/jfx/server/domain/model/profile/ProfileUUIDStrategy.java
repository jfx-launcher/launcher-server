package dk.xakeps.jfx.server.domain.model.profile;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public enum ProfileUUIDStrategy {
    RANDOM {
        @Override
        public UUID generate(String name) {
            return UUID.randomUUID();
        }
    },
    OFFLINE_NAME {
        @Override
        public UUID generate(String name) {
            return UUID.nameUUIDFromBytes(("OfflinePlayer:" + name).getBytes(StandardCharsets.UTF_8));
        }
    };

    public abstract UUID generate(String name);
}
