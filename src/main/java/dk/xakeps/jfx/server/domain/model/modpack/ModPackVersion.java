package dk.xakeps.jfx.server.domain.model.modpack;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "mod_pack_version",
        uniqueConstraints = @UniqueConstraint(name = "UK_mod_pack_version", columnNames = "upload_time"))
@NamedQueries({
        @NamedQuery(name = "ModPackVersion.select",
                query = "select v from ModPackVersion v where v.uploadTime = :uploadTime and v.modPack.modPackId = :modPackId")
})
public class ModPackVersion {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "mod_pack_id",
            foreignKey = @ForeignKey(name = "FK_mod_pack_version_to_mod_pack"))
    private ModPack modPack;

    @Column(name = "minecraft_version", nullable = false, updatable = false)
    private String minecraftVersion;

    @Column(name = "java_version", nullable = false, updatable = false)
    private String javaVersion;

    @ElementCollection
    @CollectionTable(name = "mod_pack_ignored_file",
            joinColumns = @JoinColumn(name = "mod_pack_version_id"),
            foreignKey = @ForeignKey(name = "FK_mod_pack_version_ignored_file_to_mod_pack_version"))
    @Column(name = "ignored_file")
    private Set<String> ignoredFiles;

    @Column(name = "upload_time", nullable = false, updatable = false)
    private Instant uploadTime;

    @ElementCollection
    @CollectionTable(name = "mod_pack_artifact",
            joinColumns = @JoinColumn(name = "mod_pack_version_id"),
            foreignKey = @ForeignKey(name = "FK_mod_pack_version_artifact_to_mod_pack_version"))
    private List<Artifact> artifacts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ModPack getModPack() {
        return modPack;
    }

    public void setModPack(ModPack modPack) {
        this.modPack = modPack;
    }

    public String getMinecraftVersion() {
        return minecraftVersion;
    }

    public void setMinecraftVersion(String minecraftVersion) {
        this.minecraftVersion = minecraftVersion;
    }

    public String getJavaVersion() {
        return javaVersion;
    }

    public void setJavaVersion(String javaVersion) {
        this.javaVersion = javaVersion;
    }

    public Set<String> getIgnoredFiles() {
        return ignoredFiles;
    }

    public void setIgnoredFiles(Set<String> ignoredFiles) {
        this.ignoredFiles = ignoredFiles;
    }

    public Instant getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Instant uploadTime) {
        this.uploadTime = uploadTime;
    }

    public List<Artifact> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(List<Artifact> artifacts) {
        this.artifacts = artifacts;
    }
}
