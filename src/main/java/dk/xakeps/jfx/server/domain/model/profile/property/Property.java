package dk.xakeps.jfx.server.domain.model.profile.property;

import dk.xakeps.jfx.server.domain.model.profile.GameProfile;
import dk.xakeps.jfx.server.domain.model.profile.PropertyException;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Objects;

@Entity
@Table(name = "property",
        uniqueConstraints = @UniqueConstraint(name = "UK_property", columnNames = {"property_type", "profile_id"})
)
public class Property {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "property_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private PropertyType propertyType;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "property")
    private PropertyValue value;

    @ManyToOne
    @JoinColumn(name = "profile_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_property_to_profile"))
    private GameProfile profile;

    public Property() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        if (value != null) {
            if (!propertyType.accepts(value.getClass())) {
                throw new PropertyException(String.format("PropertyType %s does not accept PropertyValue %s", propertyType, value.getClass()));
            }
        }
        this.propertyType = propertyType;
    }

    public PropertyValue getValue() {
        return value;
    }

    public void setValue(PropertyValue value) {
        if (propertyType != null) {
            if (!propertyType.accepts(value.getClass())) {
                throw new PropertyException(String.format("PropertyType %s does not accept PropertyValue %s", propertyType, value.getClass()));
            }
        }
        value.setProperty(this);
        if (profile != null) {
            value.setGameProfile(profile);
        }
        this.value = value;
    }

    public GameProfile getProfile() {
        return profile;
    }

    public void setProfile(GameProfile profile) {
        if (value != null) {
            value.setGameProfile(profile);
        }
        this.profile = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Property property = (Property) o;
        return Objects.equals(propertyType, property.propertyType) &&
                Objects.equals(value, property.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(propertyType, value);
    }

    @Override
    public String toString() {
        return "Property{" +
                "id=" + id +
                ", propertyType=" + propertyType +
                ", value=" + value +
                '}';
    }
}
