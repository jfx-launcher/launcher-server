package dk.xakeps.jfx.server.domain.model.update;

import dk.xakeps.jfx.server.domain.model.shared.NotFoundException;

public class UpdateVersionNotFoundException extends NotFoundException {
    public UpdateVersionNotFoundException() {
    }

    public UpdateVersionNotFoundException(String message) {
        super(message);
    }

    public UpdateVersionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateVersionNotFoundException(Throwable cause) {
        super(cause);
    }

    public UpdateVersionNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
