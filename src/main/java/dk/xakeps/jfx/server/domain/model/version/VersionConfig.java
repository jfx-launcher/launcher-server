package dk.xakeps.jfx.server.domain.model.version;

import com.fasterxml.jackson.annotation.JsonProperty;

public record VersionConfig(@JsonProperty("installType") InstallType installType) {
}
