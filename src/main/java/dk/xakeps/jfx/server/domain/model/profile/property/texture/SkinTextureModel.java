package dk.xakeps.jfx.server.domain.model.profile.property.texture;

public enum SkinTextureModel {
    CLASSIC, SLIM
}
