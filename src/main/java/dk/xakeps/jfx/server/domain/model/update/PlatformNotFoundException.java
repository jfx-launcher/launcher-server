package dk.xakeps.jfx.server.domain.model.update;

import dk.xakeps.jfx.server.domain.model.shared.NotFoundException;

public class PlatformNotFoundException extends NotFoundException {
    public PlatformNotFoundException() {
    }

    public PlatformNotFoundException(String message) {
        super(message);
    }

    public PlatformNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlatformNotFoundException(Throwable cause) {
        super(cause);
    }

    public PlatformNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
