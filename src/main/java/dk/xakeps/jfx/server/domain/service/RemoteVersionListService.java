package dk.xakeps.jfx.server.domain.service;

import dk.xakeps.jfx.server.domain.model.version.VersionManifest;

public interface RemoteVersionListService {
    VersionManifest getVersionManifest();
}
