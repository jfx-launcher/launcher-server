package dk.xakeps.jfx.server.domain.model.update;

import dk.xakeps.jfx.server.domain.model.shared.WrongDataException;

public class WrongUpdateVersionDataException extends WrongDataException {
    public WrongUpdateVersionDataException() {
    }

    public WrongUpdateVersionDataException(String message) {
        super(message);
    }

    public WrongUpdateVersionDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongUpdateVersionDataException(Throwable cause) {
        super(cause);
    }

    public WrongUpdateVersionDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
