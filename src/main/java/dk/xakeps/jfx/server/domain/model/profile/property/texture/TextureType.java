package dk.xakeps.jfx.server.domain.model.profile.property.texture;

public enum TextureType {
    SKIN, CAPE, ELYTRA
}
