package dk.xakeps.jfx.server.domain.model.update;

public class UpdateAlreadyExistsException extends RuntimeException {
    public UpdateAlreadyExistsException() {
    }

    public UpdateAlreadyExistsException(String message) {
        super(message);
    }

    public UpdateAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public UpdateAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
