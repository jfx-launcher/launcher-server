package dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler;

import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackAdmin;
import dk.xakeps.jfx.server.domain.model.profile.Account;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ModPackAdminAssembler {
    public ModPackAdmin toDto(Account account) {
        return new ModPackAdmin(account.getExternalId());
    }
}
