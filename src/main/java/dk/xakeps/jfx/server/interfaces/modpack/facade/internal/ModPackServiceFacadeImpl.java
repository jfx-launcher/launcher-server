package dk.xakeps.jfx.server.interfaces.modpack.facade.internal;

import dk.xakeps.jfx.server.application.ModPackService;
import dk.xakeps.jfx.server.domain.model.modpack.ModPackVersionNotFoundException;
import dk.xakeps.jfx.server.domain.model.modpack.ModPack;
import dk.xakeps.jfx.server.domain.model.modpack.ModPackVersion;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.ModPackRepo;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.ModPackVersionRepo;
import dk.xakeps.jfx.server.interfaces.modpack.facade.ModPackServiceFacade;
import dk.xakeps.jfx.server.interfaces.modpack.facade.ModPackVersionRewrite;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackAdmin;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackData;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPacksManifest;
import dk.xakeps.jfx.server.interfaces.modpack.facade.exception.WrongModPackDataException;
import dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler.ModPackAdminAssembler;
import dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler.ModPackDataAssembler;
import dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler.ModPackInfoAssembler;
import dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler.ModPacksManifestAssembler;
import org.apache.tika.Tika;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ModPackServiceFacadeImpl implements ModPackServiceFacade {
    private final ModPackService modPackService;
    private final ModPackRepo modPackRepo;
    private final ModPackVersionRepo modPackVersionRepo;

    private final ModPackInfoAssembler modPackInfoAssembler;
    private final ModPacksManifestAssembler modPacksManifestAssembler;
    private final ModPackDataAssembler modPackDataAssembler;

    private final ModPackVersionRewrite modPackVersionRewrite;

    private final ModPackAdminAssembler modPackAdminAssembler;

    @Inject
    public ModPackServiceFacadeImpl(ModPackService modPackService,
                                    ModPackRepo modPackRepo,
                                    ModPackVersionRepo modPackVersionRepo,
                                    ModPackInfoAssembler modPackInfoAssembler,
                                    ModPacksManifestAssembler modPacksManifestAssembler,
                                    ModPackDataAssembler modPackDataAssembler,
                                    ModPackVersionRewrite modPackVersionRewrite,
                                    ModPackAdminAssembler modPackAdminAssembler) {
        this.modPackService = modPackService;
        this.modPackRepo = modPackRepo;
        this.modPackVersionRepo = modPackVersionRepo;
        this.modPackInfoAssembler = modPackInfoAssembler;
        this.modPacksManifestAssembler = modPacksManifestAssembler;
        this.modPackDataAssembler = modPackDataAssembler;
        this.modPackVersionRewrite = modPackVersionRewrite;
        this.modPackAdminAssembler = modPackAdminAssembler;
    }

    @Override
    public void upload(String externalId, String modPackId, String minecraftVersion,
                       String javaVersion, Set<String> ignoredFiles, InputStream zipStream) throws IOException {
        Path zipFile = saveZip(modPackId, zipStream);
        modPackService.save(false, externalId, modPackId, minecraftVersion,
                javaVersion, ignoredFiles, zipFile);
    }

    @Override
    public void uploadOverride(String externalId, String modPackId, String minecraftVersion,
                               String javaVersion, Set<String> ignoredFiles, InputStream zipStream) throws IOException {
        Path zipFile = saveZip(modPackId, zipStream);
        modPackService.save(true, externalId, modPackId, minecraftVersion,
                javaVersion, ignoredFiles, zipFile);
    }

    @Override
    public void enableModPackVersion(String externalId, String modPackId, String modPackVersion) {
        modPackService.enableModPackVersion(false, externalId, modPackId,
                modPackVersionRewrite.fromString(modPackVersion));
    }

    @Override
    public void enableModPackVersionOverride(String externalId, String modPackId, String modPackVersion) {
        modPackService.enableModPackVersion(true, externalId, modPackId,
                modPackVersionRewrite.fromString(modPackVersion));
    }

    @Override
    public void createModPack(String externalId, String modPackId) {
        modPackService.createModPack(externalId, modPackId);
    }

    @Override
    public void deleteModPack(String modPackId) {
        modPackService.deleteModPack(modPackId);
    }

    @Override
    public ModPacksManifest getModPacksManifest(URI baseUri) {
        return modPacksManifestAssembler.toDto(
                modPackRepo.findAllWithVersionsWithoutArtifact()
                        .map(modPack -> modPackInfoAssembler.toDto(modPack, baseUri))
                        .collect(Collectors.toList()));
    }

    @Override
    public Optional<ModPackData> getModPackData(String modPackId, URI baseUri) {
        ModPack modPack = modPackRepo.findByModPackIdAndMinecraftVersion(modPackId)
                .orElse(null);
        if (modPack == null) {
            return Optional.empty();
        }
        if (modPack.getEnabledModPack() == null) {
            throw new ModPackVersionNotFoundException("Mod pack has no enabled versions");
        }
        return Optional.of(modPackDataAssembler.toDto(modPack.getEnabledModPack(), baseUri));
    }

    @Override
    public Optional<ModPackData> getModPackData(String modPackId, String version, URI baseUri) {
        Instant modPackVersion = modPackVersionRewrite.fromString(version);
        ModPackVersion modPackVersionObj = modPackVersionRepo.findByUploadTime(modPackId, modPackVersion).orElse(null);
        if (modPackVersionObj == null) {
            return Optional.empty();
        }
        return Optional.of(modPackDataAssembler.toDto(modPackVersionObj, baseUri));
    }

    @Override
    public Optional<Path> getModPackFile(String modPackId, String version, String filePath) {
        return modPackService.getFile(modPackId, modPackVersionRewrite.fromString(version), filePath);
    }

    @Override
    public List<ModPackAdmin> getModPackManagers(String requester, String modPackId) {
        return modPackService.getModPackManagers(false, requester, modPackId).stream()
                .map(modPackAdminAssembler::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ModPackAdmin> getModPackManagersOverride(String requester, String modPackId) {
        return modPackService.getModPackManagers(true, requester, modPackId).stream()
                .map(modPackAdminAssembler::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void addManager(String modPackId, String managerToAddId) {
        modPackService.addManager(modPackId, managerToAddId);
    }

    @Override
    public void deleteManager(String modPackId, String managerToDeleteId) {
        modPackService.deleteManager(modPackId, managerToDeleteId);
    }

    private Path saveZip(String modPackId, InputStream zipStream) throws IOException {
        Tika tika = new Tika();
        try (BufferedInputStream bis = new BufferedInputStream(zipStream)) {
            String contentType = tika.detect(bis);
            if (!"application/zip".equals(contentType)) {
                throw new WrongModPackDataException("Unsupported content type: " + contentType);
            }
            Path tmpDir = Files.createTempDirectory("mod_pack_service");
            Path zipFile = tmpDir.resolve(modPackId + ".zip");
            Files.createDirectories(zipFile.getParent());
            Files.copy(bis, zipFile);
            return zipFile;
        }
    }
}
