package dk.xakeps.jfx.server.interfaces.modpack.facade.internal;

import dk.xakeps.jfx.server.interfaces.modpack.facade.ModPackVersionRewrite;

import javax.enterprise.context.ApplicationScoped;
import java.time.Instant;

@ApplicationScoped
public class ModPackVersionRewriteImpl implements ModPackVersionRewrite {
    @Override
    public String toString(Instant instant) {
        return toTransferString(instant.toString());
    }

    @Override
    public Instant fromString(String instantStr) {
        return Instant.parse(fromTransferString(instantStr));
    }

    private String toTransferString(String string) {
        return string.replace(':', '_');
    }

    private String fromTransferString(String string) {
        return string.replace('_', ':');
    }
}
