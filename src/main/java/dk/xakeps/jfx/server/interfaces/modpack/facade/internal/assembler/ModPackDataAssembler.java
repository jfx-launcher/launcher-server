package dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.modpack.ModPackVersion;
import dk.xakeps.jfx.server.interfaces.modpack.facade.ModPackVersionRewrite;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModArtifact;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackData;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ModPackDataAssembler {
    private final ModArtifactAssembler modArtifactAssembler;
    private final ModPackVersionRewrite modPackVersionRewrite;

    @Inject
    public ModPackDataAssembler(ModArtifactAssembler modArtifactAssembler,
                                ModPackVersionRewrite modPackVersionRewrite) {
        this.modArtifactAssembler = modArtifactAssembler;
        this.modPackVersionRewrite = modPackVersionRewrite;
    }

    public ModPackData toDto(ModPackVersion enabledModPack, URI baseUri) {
        String modPackId = enabledModPack.getModPack().getModPackId();
        String minecraftVersion = enabledModPack.getMinecraftVersion();
        String javaVersion = enabledModPack.getJavaVersion();
        Set<String> ignoredFiles = enabledModPack.getIgnoredFiles();
        String version = modPackVersionRewrite.toString(enabledModPack.getUploadTime());
        List<ModArtifact> modArtifacts = enabledModPack.getArtifacts().stream()
                .map(artifact -> modArtifactAssembler.toDto(baseUri, enabledModPack, artifact))
                .collect(Collectors.toList());
        return new ModPackData(modPackId, minecraftVersion, javaVersion, ignoredFiles, version, modArtifacts);
    }
}
