package dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler.property;

import dk.xakeps.jfx.server.domain.model.profile.property.texture.Texture;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.TextureDto;
import dk.xakeps.jfx.server.interfaces.profile.rest.SkinResource;
import org.apache.commons.codec.binary.Hex;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@ApplicationScoped
public class TextureDtoAssembler {
    public TextureDto toDto(Texture texture, URI baseUri) {
        String hash = Hex.encodeHexString(texture.getHash());
        URI uri = UriBuilder.fromUri(baseUri)
                .path(SkinResource.class)
                .path(SkinResource.class, "getSkin").build(hash);
        Map<String, String> metadata = texture.getMetadata().entrySet()
                .stream()
                .collect(Collectors.toMap(e -> e.getKey().toString().toLowerCase(Locale.ROOT), Map.Entry::getValue));

        return new TextureDto(uri, metadata);
    }
}
