package dk.xakeps.jfx.server.interfaces.profile.facade;

import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.PropertyValueDto;

import java.security.SignatureException;

public interface PropertyService {
    String propertyToBase64(PropertyValueDto propertyDto);
    String propertySignature(String propertyBase64) throws SignatureException;
}
