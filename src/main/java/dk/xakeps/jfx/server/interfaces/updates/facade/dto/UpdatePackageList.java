package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;

import java.util.List;

public record UpdatePackageList(UpdatePackageType updatePackageType, List<UpdatePackageManifest> manifests) {
}
