package dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackage;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageList;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageManifest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class UpdatePackageListAssembler {
    private final UpdatePackageManifestAssembler updatePackageManifestAssembler;

    @Inject
    public UpdatePackageListAssembler(UpdatePackageManifestAssembler updatePackageManifestAssembler) {
        this.updatePackageManifestAssembler = updatePackageManifestAssembler;
    }

    public UpdatePackageList toDto(UpdatePackageType updatePackageType, List<UpdatePackage> updatePackages, URI baseUri) {
        List<UpdatePackageManifest> manifests = updatePackages.stream()
                .map(updatePackage -> updatePackageManifestAssembler.toDto(updatePackage, baseUri))
                .collect(Collectors.toList());
        return new UpdatePackageList(updatePackageType, manifests);
    }
}
