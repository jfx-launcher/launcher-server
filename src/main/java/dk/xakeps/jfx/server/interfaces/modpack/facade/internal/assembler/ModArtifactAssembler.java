package dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.modpack.Artifact;
import dk.xakeps.jfx.server.domain.model.modpack.ModPackVersion;
import dk.xakeps.jfx.server.interfaces.modpack.facade.ModPackVersionRewrite;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModArtifact;
import dk.xakeps.jfx.server.interfaces.modpack.rest.ModPackResource;
import org.apache.commons.codec.binary.Hex;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

@ApplicationScoped
public class ModArtifactAssembler {
    private final ModPackVersionRewrite modPackVersionRewrite;

    @Inject
    public ModArtifactAssembler(ModPackVersionRewrite modPackVersionRewrite) {
        this.modPackVersionRewrite = modPackVersionRewrite;
    }

    public ModArtifact toDto(URI baseUri, ModPackVersion modPackVersion, Artifact artifact) {
        String hash = Hex.encodeHexString(artifact.getHash());

        String modPackId = modPackVersion.getModPack().getModPackId();
        String version = modPackVersionRewrite.toString(modPackVersion.getUploadTime());

        return new ModArtifact(artifact.getFile(), artifact.getSize(), hash,
                UriBuilder.fromUri(baseUri)
                        .path(ModPackResource.class)
                        .path(ModPackResource.class, "modPackFile")
                        .resolveTemplate("modPackId", modPackId)
                        .resolveTemplate("version", version)
                        .resolveTemplate("filePath", artifact.getFile(), false)
                        .build());
    }
}
