package dk.xakeps.jfx.server.interfaces.modpack.facade.dto;

public record ModPackAdmin(String externalId) {
}
