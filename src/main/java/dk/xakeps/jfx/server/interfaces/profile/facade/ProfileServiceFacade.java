package dk.xakeps.jfx.server.interfaces.profile.facade;

import dk.xakeps.jfx.server.domain.model.profile.property.texture.SkinTextureModel;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.*;

import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface ProfileServiceFacade {
    ProfileListDto findProfilesOrCreate(String externalId, String name, GetOrCreateRequest request, URI baseUri);
    Optional<GameProfileDto> findProfile(UUID uuid, boolean full, boolean signed, URI baseUri);

    CreateProfileResult createProfile(String externalId, String name);

    List<GameProfileDto> findProfileIds(Set<String> names, URI baseUri);
    void changeSkin(String externalId, UUID profileId, SkinTextureModel skinType, InputStream skinStream);

    void changeCape(String externalId, UUID profileId, InputStream skinStream);
}
