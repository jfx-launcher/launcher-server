package dk.xakeps.jfx.server.interfaces.profile.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.UUIDDeser;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.UUIDSer;

import java.util.List;
import java.util.UUID;

public record ProfileListDto(@JsonProperty("selectedProfile") @JsonDeserialize(using = UUIDDeser.class) @JsonSerialize(using = UUIDSer.class) UUID selectedProfile,
                             @JsonProperty("profiles") List<GameProfileDto> profiles) {
}
