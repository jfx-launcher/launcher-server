package dk.xakeps.jfx.server.interfaces.version.facade.internal;

import dk.xakeps.jfx.server.application.VersionService;
import dk.xakeps.jfx.server.domain.model.version.VersionProcessingException;
import dk.xakeps.jfx.server.domain.model.version.VersionManifest;
import dk.xakeps.jfx.server.interfaces.version.facade.VersionServiceFacade;
import org.apache.tika.Tika;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

@ApplicationScoped
public class VersionServiceFacadeImpl implements VersionServiceFacade {
    private final VersionService versionService;

    @Inject
    public VersionServiceFacadeImpl(VersionService versionService) {
        this.versionService = versionService;
    }

    @Override
    public VersionManifest getVersionManifest(URI baseUri) {
        return versionService.getVersionManifest(baseUri);
    }

    @Override
    public void installForge(InputStream installerFile) {
        try {
            Path path = saveJar(installerFile);
            versionService.installForge(path);
        } catch (IOException e) {
            throw new VersionProcessingException("Can't save installer jar", e);
        }
    }

    @Override
    public Optional<Path> getForgeJson(String version) {
        return versionService.getForgeJson(version);
    }

    @Override
    public Optional<Path> getForgeInstaller(String version) {
        return versionService.getForgeInstaller(version);
    }

    private Path saveJar(InputStream jarStream) throws IOException {
        Tika tika = new Tika();
        try (BufferedInputStream bis = new BufferedInputStream(jarStream)) {
            String contentType = tika.detect(bis);
            if (!"application/java-archive".equals(contentType)) {
                throw new VersionProcessingException("Unsupported content type: " + contentType);
            }
            Path tmpFile = Files.createTempFile("forge-installer", ".jar");
            Files.copy(bis, tmpFile, StandardCopyOption.REPLACE_EXISTING);
            return tmpFile;
        }
    }
}
