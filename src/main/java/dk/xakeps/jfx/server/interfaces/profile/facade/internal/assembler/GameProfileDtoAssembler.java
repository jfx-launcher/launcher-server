package dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.profile.GameProfile;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GameProfileDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.PropertyDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler.property.PropertyDtoAssembler;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationScoped
public class GameProfileDtoAssembler {
    private final PropertyDtoAssembler propertyDtoAssembler;

    @Inject
    public GameProfileDtoAssembler(PropertyDtoAssembler propertyDtoAssembler) {
        this.propertyDtoAssembler = propertyDtoAssembler;
    }

    public GameProfileDto toDto(GameProfile gameProfile, boolean full, boolean signed, URI baseUri) {
        UUID id = gameProfile.getUuid();
        String name = gameProfile.getName();
        List<PropertyDto> properties = Collections.emptyList();
        if (full && gameProfile.getProperties() != null) {
            properties = gameProfile.getProperties().values()
                    .stream()
                    .map(property -> propertyDtoAssembler.toDto(property, signed, baseUri))
                    .collect(Collectors.toList());;
        }

        return new GameProfileDto(id, name, properties);
    }
}
