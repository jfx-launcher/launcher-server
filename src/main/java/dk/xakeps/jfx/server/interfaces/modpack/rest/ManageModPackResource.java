package dk.xakeps.jfx.server.interfaces.modpack.rest;

import dk.xakeps.jfx.server.interfaces.modpack.facade.ModPackServiceFacade;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.CreateModPackRequest;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.EnableModPackVersionRequest;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ManagerRequest;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.MultipartModPackUpload;
import io.quarkus.security.identity.SecurityIdentity;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;

@RequestScoped
@Path("mod-pack-service/manage")
public class ManageModPackResource {
    private final ModPackServiceFacade modPackServiceFacade;
    private final JsonWebToken token;
    private final SecurityIdentity securityIdentity;

    @Inject
    public ManageModPackResource(ModPackServiceFacade modPackServiceFacade, JsonWebToken token,
                                 SecurityIdentity securityIdentity) {
        this.modPackServiceFacade = modPackServiceFacade;
        this.token = token;
        this.securityIdentity = securityIdentity;
    }

    @RolesAllowed("CREATE_MOD_PACK")
    @Produces
    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    @Path("create")
    public Response createModPack(@Valid CreateModPackRequest request, @Context UriInfo uriInfo) {
        modPackServiceFacade.createModPack(token.getSubject(), request.modPackId());
//        URI modPack = UriBuilder.fromResource(ModPackResource.class)
//                .path(ModPackResource.class, "latestModPack")
//                .build(request.minecraftVersion(), request.modPackId());
//        return Response.created(modPack).build();
        return Response.ok().build();
    }

    @RolesAllowed({"MODIFY_MOD_PACK_OVERRIDE"})
    @Produces
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @DELETE
    @Path("{modPackId}")
    public Response deleteModPack(@PathParam("modPackId") String modPackId) {
        modPackServiceFacade.deleteModPack(modPackId);
        return Response.accepted().build();
    }

    @RolesAllowed({"MODIFY_MOD_PACK", "MODIFY_MOD_PACK_OVERRIDE"})
    @Produces
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @POST
    @Path("{modPackId}/versions")
    public Response uploadModPackVersion(@PathParam("modPackId") String modPackId,
                                         @MultipartForm MultipartModPackUpload form) throws IOException {
        if (securityIdentity.hasRole("MODIFY_MOD_PACK_OVERRIDE")) {
            modPackServiceFacade.uploadOverride(token.getSubject(), modPackId,
                    form.getMinecraftVersion(), form.getJavaVersion(), form.getIgnoredFiles(), form.getFile());
        } else {
            modPackServiceFacade.upload(token.getSubject(), modPackId,
                    form.getMinecraftVersion(), form.getJavaVersion(), form.getIgnoredFiles(), form.getFile());
        }
        return Response.accepted().build();
    }

    @RolesAllowed({"MODIFY_MOD_PACK", "MODIFY_MOD_PACK_OVERRIDE"})
    @Produces
    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    @Path("{modPackId}/versions/enabled")
    public Response enableModPackVersion(@PathParam("modPackId") String modPackId,
                                         @Valid EnableModPackVersionRequest request) {
        if (securityIdentity.hasRole("MODIFY_MOD_PACK_OVERRIDE")) {
            modPackServiceFacade.enableModPackVersionOverride(token.getSubject(), modPackId, request.version());
        } else {
            modPackServiceFacade.enableModPackVersion(token.getSubject(), modPackId, request.version());
        }
        return Response.ok().build();
    }

    @RolesAllowed({"MODIFY_MOD_PACK", "MODIFY_MOD_PACK_OVERRIDE"})
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("{modPackId}/managers")
    public Response getManagers(@PathParam("modPackId") String modPackId) {
        if (securityIdentity.hasRole("MODIFY_MOD_PACK_OVERRIDE")) {
            return Response.ok(modPackServiceFacade.getModPackManagersOverride(token.getSubject(), modPackId)).build();
        } else {
            return Response.ok(modPackServiceFacade.getModPackManagers(token.getSubject(), modPackId)).build();
        }
    }

    @RolesAllowed({"MODIFY_MOD_PACK_OVERRIDE"})
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    @Path("{modPackId}/managers")
    public Response addManager(@PathParam("modPackId") String modPackId,
                               @Valid ManagerRequest request) {
        modPackServiceFacade.addManager(modPackId, request.externalId());
        return Response.ok().build();
    }

    @RolesAllowed({"MODIFY_MOD_PACK_OVERRIDE"})
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @DELETE
    @Path("{modPackId}/managers/{id}")
    public Response deleteManager(@PathParam("modPackId") String modPackId,
                                  @PathParam("id") String managerId) {
        modPackServiceFacade.deleteManager(modPackId, managerId);
        return Response.ok().build();
    }
}
