package dk.xakeps.jfx.server.interfaces.profile.facade.dto;

import java.util.UUID;

public record CreateProfileResult(UUID profileId, boolean created) {
}
