package dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler.property;

import dk.xakeps.jfx.server.domain.model.profile.property.texture.TextureType;
import dk.xakeps.jfx.server.domain.model.profile.property.texture.TexturesPropertyValue;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.TextureDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.TexturePropertyValueDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationScoped
public class TexturePropertyValueDtoAssembler {
    private final TextureDtoAssembler textureDtoAssembler;

    @Inject
    public TexturePropertyValueDtoAssembler(TextureDtoAssembler textureDtoAssembler) {
        this.textureDtoAssembler = textureDtoAssembler;
    }

    public TexturePropertyValueDto toDto(TexturesPropertyValue texturesPropertyValue, URI baseUri) {
        long timestamp = texturesPropertyValue.getTimestamp().getEpochSecond();
        UUID profileId = texturesPropertyValue.getGameProfile().getUuid();
        String profileName = texturesPropertyValue.getGameProfile().getName();
        Map<TextureType, TextureDto> textures = texturesPropertyValue.getTextures().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> textureDtoAssembler.toDto(e.getValue(), baseUri)));

        return new TexturePropertyValueDto(timestamp, profileId, profileName, textures);
    }
}
