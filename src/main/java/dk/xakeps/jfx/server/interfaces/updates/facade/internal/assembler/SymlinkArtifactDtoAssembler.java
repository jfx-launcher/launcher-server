package dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.update.SymlinkArtifact;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.SymlinkArtifactDto;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SymlinkArtifactDtoAssembler {
    public SymlinkArtifactDto toDto(SymlinkArtifact symlinkArtifact) {
        return new SymlinkArtifactDto(
                symlinkArtifact.getTarget(),
                symlinkArtifact.getSource()
        );
    }
}
