package dk.xakeps.jfx.server.interfaces.updates.rest;

import dk.xakeps.jfx.server.domain.model.shared.ForbiddenException;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.interfaces.updates.facade.PlatformFacade;
import dk.xakeps.jfx.server.interfaces.updates.facade.UpdatePackageServiceFacade;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.EnableVersionRequest;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.MultipartUpdateVersionUpload;
import io.quarkus.security.identity.SecurityIdentity;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Locale;

@RequestScoped
@Path("updates-service")
public class UpdatesResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdatesResource.class);

    private final UpdatePackageServiceFacade updatePackageServiceFacade;
    private final PlatformFacade platformFacade;
    private final SecurityIdentity securityContext;

    @Inject
    public UpdatesResource(UpdatePackageServiceFacade updatePackageServiceFacade,
                           PlatformFacade platformFacade,
                           SecurityIdentity securityContext) {
        this.updatePackageServiceFacade = updatePackageServiceFacade;
        this.platformFacade = platformFacade;
        this.securityContext = securityContext;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes
    public Response getSupportedPlatforms() {
        return Response.ok(platformFacade.getPlatforms()).build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("{updatePackageType}/manifest.json")
    public Response listUpdatePackages(@PathParam("updatePackageType") String updatePackageType,
                                       @Context UriInfo uriInfo) {
        return Response.ok(updatePackageServiceFacade.getUpdatePackageManifest(
                getUpdatePackageType(updatePackageType), uriInfo.getBaseUri()))
                .build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("{updatePackageType}/{system}/{arch}/{version}.json")
    public Response getVersionInfo(@PathParam("updatePackageType") String updatePackageType,
                                   @PathParam("version") String version,
                                   @PathParam("arch") String arch,
                                   @PathParam("system") String system,
                                   @Context UriInfo uriInfo) {
        return updatePackageServiceFacade.getUpdatePackageData(
                getUpdatePackageType(updatePackageType), version, arch, system, uriInfo.getBaseUri())
                .map(Response::ok)
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND))
                .build();
    }

    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @GET
    @Path("{updatePackageType}/{system}/{arch}/{version}/files/{filePath:.+}")
    public Response getVersionFile(@PathParam("updatePackageType") String updatePackageType,
                                   @PathParam("version") String version,
                                   @PathParam("arch") String arch,
                                   @PathParam("system") String system,
                                   @PathParam("filePath") String filePath) {
        return updatePackageServiceFacade.getFile(getUpdatePackageType(updatePackageType),
                version, arch, system, filePath)
                .map(UpdatesResource::pathToInputStream)
                .map(Response::ok)
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND))
                .build();
    }

    @RolesAllowed("MANAGE_UPDATES")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces
    @POST
    @Path("{updatePackageType}/{system}/{arch}")
    public Response uploadVersion(@PathParam("updatePackageType") String updatePackageType,
                                  @PathParam("arch") String arch,
                                  @PathParam("system") String system,
                                  @MultipartForm MultipartUpdateVersionUpload form) throws IOException {
        if (!securityContext.hasRole("MANAGE_UPDATES_" + updatePackageType.toUpperCase(Locale.ROOT))) {
            throw new ForbiddenException();
        }
        updatePackageServiceFacade.uploadUpdateVersion(getUpdatePackageType(updatePackageType), form.getVersion(),
                arch, system, form.getFile());
        return Response.accepted().build();
    }


    @RolesAllowed("MANAGE_UPDATES")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces
    @DELETE
    @Path("{updatePackageType}/{system}/{arch}")
    public Response deleteVersion(@PathParam("updatePackageType") String updatePackageType,
                                  @PathParam("arch") String arch,
                                  @PathParam("system") String system) throws IOException {
        if (!securityContext.hasRole("MANAGE_UPDATES_" + updatePackageType.toUpperCase(Locale.ROOT))) {
            throw new ForbiddenException();
        }
        updatePackageServiceFacade.deletePackage(getUpdatePackageType(updatePackageType), arch, system);
        return Response.accepted().build();
    }

    @RolesAllowed("MANAGE_UPDATES")
    @Consumes
    @Produces
    @DELETE
    @Path("{updatePackageType}")
    public Response deletePackage(@PathParam("updatePackageType") String updatePackageType) {
        if (!securityContext.hasRole("MANAGE_UPDATES_" + updatePackageType.toUpperCase(Locale.ROOT))) {
            throw new ForbiddenException();
        }
        updatePackageServiceFacade.deletePackage(getUpdatePackageType(updatePackageType));
        return Response.accepted().build();
    }

    @RolesAllowed("MANAGE_UPDATES")
    @Produces
    @DELETE
    @Path("{updatePackageType}/{system}/{arch}/{version}")
    public Response removeVersion(@PathParam("updatePackageType") String updatePackageType,
                                  @PathParam("version") String version,
                                  @PathParam("arch") String arch,
                                  @PathParam("system") String system) {
        if (!securityContext.hasRole("MANAGE_UPDATES_" + updatePackageType.toUpperCase(Locale.ROOT))) {
            throw new ForbiddenException();
        }
        updatePackageServiceFacade.removeUpdateVersion(getUpdatePackageType(updatePackageType), version, arch, system);
        return Response.ok().build();
    }

    @RolesAllowed("MANAGE_UPDATES")
    @Produces
    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    @Path("{updatePackageType}/{system}/{arch}/version/enabled")
    public Response enableVersion(@PathParam("updatePackageType") String updatePackageType,
                                  @PathParam("arch") String arch,
                                  @PathParam("system") String system,
                                  @Valid EnableVersionRequest request) {
        if (!securityContext.hasRole("MANAGE_UPDATES_" + updatePackageType.toUpperCase(Locale.ROOT))) {
            throw new ForbiddenException();
        }
        updatePackageServiceFacade.enableVersion(getUpdatePackageType(updatePackageType),
                request.version(), arch, system);
        return Response.ok().build();
    }

    private static InputStream pathToInputStream(java.nio.file.Path path) {
        try {
            return Files.newInputStream(path);
        } catch (IOException e) {
            LOGGER.error("Can't open stream", e);
            return null;
        }
    }

    private static UpdatePackageType getUpdatePackageType(String str) {
        return UpdatePackageType.valueOf(str.toUpperCase(Locale.ROOT));
    }
}
