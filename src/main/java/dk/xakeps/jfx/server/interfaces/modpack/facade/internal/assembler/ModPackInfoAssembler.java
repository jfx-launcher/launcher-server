package dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.modpack.ModPack;
import dk.xakeps.jfx.server.domain.model.modpack.ModPackVersion;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackInfo;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackVersionDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ModPackInfoAssembler {
    private final ModPackVersionAssembler modPackVersionAssembler;

    @Inject
    public ModPackInfoAssembler(ModPackVersionAssembler modPackVersionAssembler) {
        this.modPackVersionAssembler = modPackVersionAssembler;
    }

    public ModPackInfo toDto(ModPack modPack, URI baseUri) {
        String modPackId = modPack.getModPackId();
        List<ModPackVersionDto> versions = modPack.getVersions().stream()
                .map(modPackVersion -> modPackVersionAssembler.toDto(modPackVersion, baseUri))
                .collect(Collectors.toList());
        String enabledVersion = "";
        ModPackVersion enabledModPack = modPack.getEnabledModPack();
        if (enabledModPack != null) {
            enabledVersion = enabledModPack.getUploadTime().toString();
        }
        return new ModPackInfo(modPackId, versions, enabledVersion);
    }
}
