package dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler.property;

import dk.xakeps.jfx.server.domain.model.profile.GameProfileException;
import dk.xakeps.jfx.server.domain.model.profile.property.Property;
import dk.xakeps.jfx.server.interfaces.profile.facade.PropertyService;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.PropertyDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.security.SignatureException;
import java.util.Locale;

@ApplicationScoped
public class PropertyDtoAssembler {
    private final PropertyValueDtoAssembler propertyValueDtoAssembler;
    private final PropertyService propertyService;

    @Inject
    public PropertyDtoAssembler(PropertyValueDtoAssembler propertyValueDtoAssembler, PropertyService propertyService) {
        this.propertyValueDtoAssembler = propertyValueDtoAssembler;
        this.propertyService = propertyService;
    }

    public PropertyDto toDto(Property property, boolean signed, URI baseUri) {
        String name = property.getPropertyType().toString().toLowerCase(Locale.ENGLISH);
        String value = propertyService.propertyToBase64(propertyValueDtoAssembler.toDto(property.getValue(), baseUri));
        String signature = "";
        if (signed) {
            try {
                signature = propertyService.propertySignature(value);
            } catch (SignatureException e) {
                throw new GameProfileException("Can't sign property", e);
            }
        }

        return new PropertyDto(name, value, signature);
    }
}
