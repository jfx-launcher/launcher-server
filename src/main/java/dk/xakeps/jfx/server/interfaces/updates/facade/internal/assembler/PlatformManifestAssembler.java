package dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.update.Platform;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.PlatformDto;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.PlatformManifest;

import javax.enterprise.context.ApplicationScoped;
import java.util.*;

@ApplicationScoped
public class PlatformManifestAssembler {
    public PlatformManifest toDto(List<Platform> platforms) {
        Map<String, PlatformDto> platformDtoMap = new HashMap<>();
        for (Platform platform : platforms) {
            PlatformDto platformDto = platformDtoMap
                    .computeIfAbsent(platform.getSystem(), sys -> new PlatformDto(sys, new HashSet<>(2)));
            platformDto.architectures().add(platform.getArch());
        }
        return new PlatformManifest(platformDtoMap, Arrays.asList(UpdatePackageType.values()));
    }
}
