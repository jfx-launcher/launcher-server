package dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.update.FileArtifact;
import dk.xakeps.jfx.server.domain.model.update.UpdateVersion;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.FileArtifactDto;
import dk.xakeps.jfx.server.interfaces.updates.rest.UpdatesResource;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.HexFormat;

@ApplicationScoped
public class FileArtifactDtoAssembler {
    public FileArtifactDto toDto(UpdateVersion updateVersion, FileArtifact fileArtifact, URI baseUri) {
        return new FileArtifactDto(
                fileArtifact.getFile(),
                HexFormat.of().formatHex(fileArtifact.getHash()),
                fileArtifact.getSize(),
                UriBuilder.fromUri(baseUri)
                        .path(UpdatesResource.class)
                        .path(UpdatesResource.class, "getVersionFile")
                        .resolveTemplate("updatePackageType", updateVersion.getUpdatePackage().getUpdatePackageType())
                        .resolveTemplate("system", updateVersion.getUpdatePackage().getPlatform().getSystem())
                        .resolveTemplate("arch", updateVersion.getUpdatePackage().getPlatform().getArch())
                        .resolveTemplate("version", updateVersion.getVersion())
                        .resolveTemplate("filePath", fileArtifact.getFile(), false)
                        .build()
                );
    }
}
