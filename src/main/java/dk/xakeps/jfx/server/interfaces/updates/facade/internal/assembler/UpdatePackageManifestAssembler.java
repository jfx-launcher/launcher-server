package dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackage;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.domain.model.update.UpdateVersion;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageInfo;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageManifest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class UpdatePackageManifestAssembler {
    private final UpdatePackageInfoAssembler updatePackageInfoAssembler;

    @Inject
    public UpdatePackageManifestAssembler(UpdatePackageInfoAssembler updatePackageInfoAssembler) {
        this.updatePackageInfoAssembler = updatePackageInfoAssembler;
    }

    public UpdatePackageManifest toDto(UpdatePackage updatePackage, URI baseUri) {
        UpdatePackageType updatePackageType = updatePackage.getUpdatePackageType();
        String arch = updatePackage.getPlatform().getArch();
        String system = updatePackage.getPlatform().getSystem();
        UpdateVersion enabledVersionObj = updatePackage.getEnabledVersion();
        String enabledVersion = "";
        if (enabledVersionObj != null) {
            enabledVersion = enabledVersionObj.getVersion();
        }
        List<UpdatePackageInfo> updates = updatePackage.getPackageVersions().stream()
                .map(updateVersion -> updatePackageInfoAssembler.toDto(updateVersion, baseUri))
                .collect(Collectors.toList());
        return new UpdatePackageManifest(updatePackageType, arch, system, enabledVersion, updates);
    }
}
