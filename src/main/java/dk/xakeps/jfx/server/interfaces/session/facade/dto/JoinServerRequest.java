package dk.xakeps.jfx.server.interfaces.session.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.UUIDDeser;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.UUIDSer;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public record JoinServerRequest(@JsonProperty("accessToken") @NotNull String accessToken,
                                @JsonProperty("selectedProfile") @JsonDeserialize(using = UUIDDeser.class) @JsonSerialize(using = UUIDSer.class) @NotNull UUID selectedProfile,
                                @JsonProperty("serverId") @NotNull String serverId) {
}
