package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;

import java.util.List;

public record UpdatePackageData(UpdatePackageType updatePackageType,
                                String version, String arch, String system,
                                List<FileArtifactDto> files, List<SymlinkArtifactDto> symlinks) {
}
