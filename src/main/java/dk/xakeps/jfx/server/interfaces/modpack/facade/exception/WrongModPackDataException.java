package dk.xakeps.jfx.server.interfaces.modpack.facade.exception;

public class WrongModPackDataException extends RuntimeException {
    public WrongModPackDataException() {
    }

    public WrongModPackDataException(String message) {
        super(message);
    }

    public WrongModPackDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongModPackDataException(Throwable cause) {
        super(cause);
    }

    public WrongModPackDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
