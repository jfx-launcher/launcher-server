package dk.xakeps.jfx.server.interfaces.profile.rest;

import dk.xakeps.jfx.server.application.DashlessUUIDUtil;
import dk.xakeps.jfx.server.domain.model.profile.WrongSkinDataException;
import dk.xakeps.jfx.server.domain.model.profile.property.texture.SkinTextureModel;
import dk.xakeps.jfx.server.interfaces.profile.facade.ProfileServiceFacade;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.MultipartCapeUpload;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.MultipartSkinUpload;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Locale;

@Path("profile-service/skin/")
@RequestScoped
public class SkinResource {
    private final ProfileServiceFacade profileServiceFacade;
    private final JsonWebToken token;

    @Inject
    public SkinResource(ProfileServiceFacade profileServiceFacade, JsonWebToken token) {
        this.profileServiceFacade = profileServiceFacade;
        this.token = token;
    }

    @Produces
    @GET
    @Path("{hash}")
    public Response getSkin(@PathParam("hash") String hash) throws IOException {
        java.nio.file.Path file = java.nio.file.Path.of("skins", hash).toAbsolutePath();
        java.nio.file.Path of = java.nio.file.Path.of("").toAbsolutePath();
        if (!file.startsWith(of)) {
            return Response.noContent().build();
        }
        if (Files.notExists(file)) {
            return Response.noContent().build();
        }
        InputStream inputStream = Files.newInputStream(file);
        return Response.ok(inputStream, new MediaType("image", "png")).build();
    }

    @Produces
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RolesAllowed("CHANGE_SKIN")
    @POST
    @Path("skin")
    public Response updateSkin(@MultipartForm MultipartSkinUpload body) {
        String profileId = body.getProfileId();
        if (profileId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        SkinTextureModel skinType = SkinTextureModel.CLASSIC;
        if (body.getVariant() != null) {
            skinType = switch (body.getVariant().toLowerCase(Locale.ROOT)) {
                case "classic" -> SkinTextureModel.CLASSIC;
                case "slim" -> SkinTextureModel.SLIM;
                default -> throw new WrongSkinDataException("Unexpected value: " + body.getVariant().toLowerCase(Locale.ROOT));
            };
        }
        String externalId = token.getSubject();
        profileServiceFacade.changeSkin(externalId, DashlessUUIDUtil.fromString(profileId), skinType, body.getFile());

        return Response.accepted().build();
    }

    @Produces
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RolesAllowed("CHANGE_CAPE")
    @POST
    @Path("cape")
    public Response updateCape(@MultipartForm MultipartCapeUpload body) {
        String profileId = body.getProfileId();
        if (profileId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        String externalId = token.getSubject();
        profileServiceFacade.changeCape(externalId, DashlessUUIDUtil.fromString(profileId), body.getFile());

        return Response.accepted().build();
    }
}
