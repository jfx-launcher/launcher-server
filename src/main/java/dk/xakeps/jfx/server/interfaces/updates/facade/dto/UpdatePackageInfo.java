package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;

import java.net.URI;

public record UpdatePackageInfo(@JsonProperty("updatePackageType") UpdatePackageType updatePackageType,
                                @JsonProperty("system") String system,
                                @JsonProperty("arch") String arch,
                                @JsonProperty("version") String version,
                                @JsonProperty("url") URI uri) {
}
