package dk.xakeps.jfx.server.interfaces.modpack.facade.dto;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.util.Set;

public class MultipartModPackUpload {
    @FormParam("file")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    private InputStream file;

    @FormParam("minecraftVersion")
    @PartType(MediaType.TEXT_PLAIN)
    private String minecraftVersion;

    @FormParam("javaVersion")
    @PartType(MediaType.TEXT_PLAIN)
    private String javaVersion;

    @FormParam("ignoredFiles")
    @PartType(MediaType.APPLICATION_JSON)
    private Set<String> ignoredFiles;

    public InputStream getFile() {
        return file;
    }

    public String getMinecraftVersion() {
        return minecraftVersion;
    }

    public String getJavaVersion() {
        return javaVersion;
    }

    public Set<String> getIgnoredFiles() {
        return ignoredFiles;
    }
}
