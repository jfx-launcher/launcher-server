package dk.xakeps.jfx.server.interfaces.updates.facade;

import dk.xakeps.jfx.server.interfaces.updates.facade.dto.PlatformManifest;

public interface PlatformFacade {
    PlatformManifest getPlatforms();
}
