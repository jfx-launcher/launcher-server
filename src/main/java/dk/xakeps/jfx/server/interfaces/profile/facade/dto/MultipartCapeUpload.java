package dk.xakeps.jfx.server.interfaces.profile.facade.dto;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;

public class MultipartCapeUpload {
    @FormParam("file")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    private InputStream file;

    @FormParam("profileId")
    @PartType(MediaType.TEXT_PLAIN)
    private String profileId;

    public InputStream getFile() {
        return file;
    }

    public String getProfileId() {
        return profileId;
    }
}
