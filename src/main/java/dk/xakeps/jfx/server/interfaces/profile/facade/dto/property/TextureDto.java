package dk.xakeps.jfx.server.interfaces.profile.facade.dto.property;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.Map;

public record TextureDto(@JsonProperty("url") URI url,
                         @JsonProperty("metadata") Map<String, String> metadata) {
}
