package dk.xakeps.jfx.server.interfaces.modpack.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.Set;

public record ModPackVersionDto(@JsonProperty("version") String version,
                                @JsonProperty("minecraftVersion") String minecraftVersion,
                                @JsonProperty("javaVersion") String javaVersion,
                                @JsonProperty("ignoredFiles") Set<String> ignoredFiles,
                                @JsonProperty("url") URI uri) {
}
