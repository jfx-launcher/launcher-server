package dk.xakeps.jfx.server.interfaces.modpack.rest;

import dk.xakeps.jfx.server.interfaces.modpack.facade.ModPackServiceFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@Path("mod-pack-service")
public class ModPackResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModPackResource.class);

    private final ModPackServiceFacade modPackServiceFacade;

    @Inject
    public ModPackResource(ModPackServiceFacade modPackServiceFacade) {
        this.modPackServiceFacade = modPackServiceFacade;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("mod_manifest.json")
    public Response listModPacks(@Context UriInfo uriInfo) {
        return Response.ok(modPackServiceFacade.getModPacksManifest(uriInfo.getBaseUri()))
                .build();
    }

//    @Produces(MediaType.APPLICATION_JSON)
//    @GET
//    @Path("modpack/{minecraftVersion}/{modPackId}.json")
//    public Response latestModPack(@PathParam("minecraftVersion") String minecraftVersion,
//                                  @PathParam("modPackId") String modPackId,
//                                  @Context UriInfo uriInfo) {
//        return modPackServiceFacade.getModPackData(modPackId, minecraftVersion, uriInfo.getBaseUri())
//                .map(Response::ok)
//                .orElseGet(Response::noContent)
//                .build();
//    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path("modpack/{modPackId}/{version}.json")
    public Response modPack(@PathParam("modPackId") String modPackId,
                            @PathParam("version") String version,
                            @Context UriInfo uriInfo) {
        return modPackServiceFacade.getModPackData(modPackId, version, uriInfo.getBaseUri())
                .map(Response::ok)
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND))
                .build();
    }

    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @GET
    @Path("modpack/files/{modPackId}/{version}/{filePath:.+}")
    public Response modPackFile(@PathParam("modPackId") String modPackId,
                                @PathParam("version") String version,
                                @PathParam("filePath") String filePath) {
        return modPackServiceFacade.getModPackFile(modPackId, version, filePath)
                .map(ModPackResource::pathToInputStream)
                .map(Response::ok)
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND))
                .build();
    }

    private static InputStream pathToInputStream(java.nio.file.Path path) {
        try {
            return Files.newInputStream(path);
        } catch (IOException e) {
            LOGGER.error("Can't open stream", e);
            return null;
        }
    }
}
