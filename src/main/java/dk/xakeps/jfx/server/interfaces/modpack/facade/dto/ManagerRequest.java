package dk.xakeps.jfx.server.interfaces.modpack.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;

public record ManagerRequest(@JsonProperty("externalId") @NotEmpty String externalId) {
}
