package dk.xakeps.jfx.server.interfaces.version.rest;

import dk.xakeps.jfx.server.interfaces.version.facade.VersionServiceFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@Path("version-service")
public class VersionResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(VersionResource.class);
    private final VersionServiceFacade versionServiceFacade;

    @Inject
    public VersionResource(VersionServiceFacade versionServiceFacade) {
        this.versionServiceFacade = versionServiceFacade;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("version_manifest.json")
    public Response versionManifest(@Context UriInfo uriInfo) {
        return Response.ok(versionServiceFacade.getVersionManifest(uriInfo.getBaseUri())).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("version/forge/{version}.json")
    public Response forgeJson(@PathParam("version") String version) {
        return versionServiceFacade.getForgeJson(version)
                .map(VersionResource::pathToInputStream)
                .map(Response::ok)
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND))
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Path("version/forge/{version}.jar")
    public Response forgeInstaller(@PathParam("version") String version) {
        return versionServiceFacade.getForgeInstaller(version)
                .map(VersionResource::pathToInputStream)
                .map(Response::ok)
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND))
                .build();
    }

    private static InputStream pathToInputStream(java.nio.file.Path path) {
        try {
            return Files.newInputStream(path);
        } catch (IOException e) {
            LOGGER.error("Failed to open stream", e);
            // FIXME: maybe throw an error?
            return null;
        }
    }
}
