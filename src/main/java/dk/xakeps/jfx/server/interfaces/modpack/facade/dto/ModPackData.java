package dk.xakeps.jfx.server.interfaces.modpack.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public record ModPackData(@JsonProperty("modPackId") String modPackId,
                          @JsonProperty("minecraftVersion") String minecraftVersion,
                          @JsonProperty("javaVersion") String javaVersion,
                          @JsonProperty("ignoredFiles") Set<String> ignoredFiles,
                          @JsonProperty("version") String version,
                          @JsonProperty("files") List<ModArtifact> files) {
    public ModPackData {
        Objects.requireNonNull(modPackId, "modPackId");
        Objects.requireNonNull(minecraftVersion, "minecraftVersion");
        Objects.requireNonNull(javaVersion, "javaVersion");
        Objects.requireNonNull(ignoredFiles, "ignoredFiles");
        Objects.requireNonNull(version, "version");
        Objects.requireNonNull(files, "files");
    }
}