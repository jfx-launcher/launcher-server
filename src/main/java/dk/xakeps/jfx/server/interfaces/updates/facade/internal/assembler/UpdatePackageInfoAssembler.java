package dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.domain.model.update.UpdateVersion;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageInfo;
import dk.xakeps.jfx.server.interfaces.updates.rest.UpdatesResource;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

@ApplicationScoped
public class UpdatePackageInfoAssembler {
    public UpdatePackageInfo toDto(UpdateVersion updateVersion, URI baseUri) {
        UpdatePackageType updatePackageType = updateVersion.getUpdatePackage().getUpdatePackageType();
        String system = updateVersion.getUpdatePackage().getPlatform().getSystem();
        String arch = updateVersion.getUpdatePackage().getPlatform().getArch();
        String version = updateVersion.getVersion();
        return new UpdatePackageInfo(
                updatePackageType,
                system,
                arch,
                version,
                UriBuilder.fromUri(baseUri)
                        .path(UpdatesResource.class)
                        .path(UpdatesResource.class, "getVersionInfo")
                        .build(updatePackageType, system, arch, version)
                );
    }
}
