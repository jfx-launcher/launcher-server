package dk.xakeps.jfx.server.interfaces.version.facade;

import dk.xakeps.jfx.server.domain.model.version.VersionManifest;

import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;

public interface VersionServiceFacade {
    VersionManifest getVersionManifest(URI baseUri);
    void installForge(InputStream installerFile);
    Optional<Path> getForgeJson(String version);
    Optional<Path> getForgeInstaller(String version);
}
