package dk.xakeps.jfx.server.interfaces.updates.facade.internal;

import dk.xakeps.jfx.server.application.UpdatePackageService;
import dk.xakeps.jfx.server.domain.model.update.WrongUpdateVersionDataException;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.domain.model.update.UpdateVersion;
import dk.xakeps.jfx.server.interfaces.updates.facade.UpdatePackageServiceFacade;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageData;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageList;
import dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler.UpdatePackageDataAssembler;
import dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler.UpdatePackageListAssembler;
import org.apache.tika.Tika;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Optional;

@ApplicationScoped
public class UpdatePackageServiceFacadeImpl implements UpdatePackageServiceFacade {
    private final UpdatePackageService updatePackageService;

    private final UpdatePackageListAssembler updatePackageListAssembler;
    private final UpdatePackageDataAssembler updatePackageDataAssembler;

    @Inject
    public UpdatePackageServiceFacadeImpl(UpdatePackageService updatePackageService,
                                          UpdatePackageListAssembler updatePackageListAssembler,
                                          UpdatePackageDataAssembler updatePackageDataAssembler) {
        this.updatePackageService = updatePackageService;
        this.updatePackageListAssembler = updatePackageListAssembler;
        this.updatePackageDataAssembler = updatePackageDataAssembler;
    }

    @Override
    public UpdatePackageList getUpdatePackageManifest(UpdatePackageType updatePackageType, URI baseUri) {
        return updatePackageListAssembler.toDto(updatePackageType,
                updatePackageService.listPackages(updatePackageType), baseUri);
    }

    @Override
    public Optional<Path> getFile(UpdatePackageType updatePackageType, String version, String arch, String system, String filePath) {
        return updatePackageService.getFile(updatePackageType, version, arch, system, filePath);
    }

    @Override
    public Optional<UpdatePackageData> getUpdatePackageData(UpdatePackageType updatePackageType, String version,
                                                            String arch, String system, URI baseUri) {
        UpdateVersion updateVersion = updatePackageService.getVersionWithArtifacts(updatePackageType,
                version, arch, system).orElse(null);
        if (updateVersion == null) {
            return Optional.empty();
        }
        return Optional.of(updatePackageDataAssembler.toDto(updateVersion, baseUri));
    }

    @Override
    public void uploadUpdateVersion(UpdatePackageType updatePackageType, String version,
                                    String arch, String system, InputStream file) throws IOException {
        Path archive = saveZip(updatePackageType, version, arch, system, file);
        updatePackageService.save(updatePackageType, version, arch, system, archive);
    }

    @Override
    public void removeUpdateVersion(UpdatePackageType updatePackageType, String version, String arch, String system) {
        updatePackageService.removeVersion(updatePackageType, version, arch, system);
    }

    @Override
    public void enableVersion(UpdatePackageType updatePackageType, String version, String arch, String system) {
        updatePackageService.enable(updatePackageType, version, arch, system);
    }

    @Override
    public void deletePackage(UpdatePackageType updatePackageType) {
        updatePackageService.deletePackage(updatePackageType);
    }

    @Override
    public void deletePackage(UpdatePackageType updatePackageType, String arch, String system) {
        updatePackageService.deletePackage(updatePackageType, arch, system);
    }

    private Path saveZip(UpdatePackageType updatePackageType,
                         String version, String arch, String system, InputStream zipStream) throws IOException {
        Tika tika = new Tika();
        try (BufferedInputStream bis = new BufferedInputStream(zipStream)) {
            String contentType = tika.detect(bis);
            String extension;
            if ("application/zip".equals(contentType)) {
                extension = "zip";
            } else if ("application/gzip".equals(contentType)) {
                extension = "tar.gz"; // it might be not tar
            } else {
                throw new WrongUpdateVersionDataException("Unsupported content type: " + contentType);
            }
            Path tmpDir = Files.createTempDirectory("update_service");
            Path zipFile = tmpDir.resolve(updatePackageType.toString().toLowerCase(Locale.ROOT))
                    .resolve(system).resolve(arch).resolve(version + "." + extension);
            Files.createDirectories(zipFile.getParent());
            Files.copy(bis, zipFile);
            return zipFile;
        }
    }
}
