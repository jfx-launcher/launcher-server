package dk.xakeps.jfx.server.interfaces.modpack.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public record ModPackInfo(@JsonProperty("modPackId") String modPackId,
                          @JsonProperty("versions") List<ModPackVersionDto> versions,
                          @JsonProperty("enabledVersion") String enabledVersion) {
    public ModPackInfo {
        Objects.requireNonNull(modPackId, "modPackId");
        Objects.requireNonNull(versions, "versions");
        Objects.requireNonNull(enabledVersion, "enabledVersion");
    }
}

