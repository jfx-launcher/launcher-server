package dk.xakeps.jfx.server.interfaces.profile.facade.dto.property;

import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.server.domain.model.profile.property.texture.TextureType;

import java.util.Map;
import java.util.UUID;

public record TexturePropertyValueDto(@JsonProperty("timestamp") long timestamp,
                                      @JsonProperty("profileId") UUID profileId,
                                      @JsonProperty("profileName") String profileName,
                                      @JsonProperty("textures") Map<TextureType, TextureDto> textures) implements PropertyValueDto {
}
