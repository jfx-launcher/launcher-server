package dk.xakeps.jfx.server.interfaces.modpack.facade;

import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackAdmin;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackData;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPacksManifest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ModPackServiceFacade {
    void createModPack(String externalId, String modPackId);
    void deleteModPack(String modPackId);
    void upload(String externalId, String modPackId, String minecraftVersion,
                String javaVersion, Set<String> ignoredFiles, InputStream zipStream) throws IOException;
    void uploadOverride(String externalId, String modPackId, String minecraftVersion,
                        String javaVersion, Set<String> ignoredFiles, InputStream zipStream) throws IOException;
    void enableModPackVersion(String externalId, String modPackId, String modPackVersion);
    void enableModPackVersionOverride(String externalId, String modPackId, String modPackVersion);

    ModPacksManifest getModPacksManifest(URI baseUri);
    Optional<ModPackData> getModPackData(String modPackId, URI baseUri);
    Optional<ModPackData> getModPackData(String modPackId, String version, URI baseUri);

    Optional<Path> getModPackFile(String modPackId, String version, String filePath);

    List<ModPackAdmin> getModPackManagers(String requester, String modPackId);
    List<ModPackAdmin> getModPackManagersOverride(String requester, String modPackId);

    void addManager(String modPackId, String managerToAddId);

    void deleteManager(String modPackId, String managerToDeleteId);
}
