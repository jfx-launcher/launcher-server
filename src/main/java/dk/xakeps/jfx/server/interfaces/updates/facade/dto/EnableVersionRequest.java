package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;

public record EnableVersionRequest(@JsonProperty("version") @NotEmpty String version) {
}
