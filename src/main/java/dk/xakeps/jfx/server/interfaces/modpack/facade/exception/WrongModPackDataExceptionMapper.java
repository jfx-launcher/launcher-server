package dk.xakeps.jfx.server.interfaces.modpack.facade.exception;

import dk.xakeps.jfx.server.application.rest.ErrorResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class WrongModPackDataExceptionMapper implements ExceptionMapper<WrongModPackDataException> {
    @Override
    public Response toResponse(WrongModPackDataException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorResponse(exception.getClass().getSimpleName(), exception.getMessage()))
                .build();
    }
}
