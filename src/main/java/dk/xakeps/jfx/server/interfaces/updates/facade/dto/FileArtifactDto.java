package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

import java.net.URI;

public record FileArtifactDto(String file, String hash, long size, URI uri) {
}
