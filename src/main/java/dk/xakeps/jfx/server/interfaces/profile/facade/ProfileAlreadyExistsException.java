package dk.xakeps.jfx.server.interfaces.profile.facade;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class ProfileAlreadyExistsException extends ClientErrorException {
    public ProfileAlreadyExistsException(String message) {
        super(message, Response.Status.CONFLICT);
    }
}
