package dk.xakeps.jfx.server.interfaces.modpack.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.Objects;

public record ModArtifact(@JsonProperty("file") String file, @JsonProperty("size") long size,
                          @JsonProperty("hash") String hash, @JsonProperty("url") URI url) {
    public ModArtifact {
        Objects.requireNonNull(file, "file");
        Objects.requireNonNull(hash, "hash");
        Objects.requireNonNull(url, "url");
    }
}
