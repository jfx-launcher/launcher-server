package dk.xakeps.jfx.server.interfaces.version.rest;

import dk.xakeps.jfx.server.interfaces.version.facade.VersionServiceFacade;
import dk.xakeps.jfx.server.interfaces.version.facade.dto.MultipartForgeUpload;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("version-service/manage")
public class ManageVersionResource {
    private final VersionServiceFacade versionServiceFacade;
    private final JsonWebToken jsonWebToken;

    @Inject
    public ManageVersionResource(VersionServiceFacade versionServiceFacade,
                                 JsonWebToken jsonWebToken) {
        this.versionServiceFacade = versionServiceFacade;
        this.jsonWebToken = jsonWebToken;
    }

    @Produces
    @RolesAllowed("MODIFY_VERSION")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("upload/forge")
    @POST
    public Response uploadForge(@MultipartForm MultipartForgeUpload form) {
        versionServiceFacade.installForge(form.getFile());
        return Response.accepted().build();
    }
}
