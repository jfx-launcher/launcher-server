package dk.xakeps.jfx.server.interfaces.account.rest;

import dk.xakeps.jfx.server.domain.service.RemoteAccountFinderService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@Path("account-service")
public class AccountResource {
    private final RemoteAccountFinderService remoteAccountFinderService;

    public AccountResource(RemoteAccountFinderService remoteAccountFinderService) {
        this.remoteAccountFinderService = remoteAccountFinderService;
    }

    @RolesAllowed("CAN_SEE_USERS")
    @GET
    @Path("find")
    public Response findByEmail(@QueryParam("email") List<String> email,
                                @QueryParam("id") List<String> id) {
        if (email != null && !email.isEmpty()) {
            return Response.ok(remoteAccountFinderService.findAccountsByEmails(email)).build();
        } else if (id != null && !id.isEmpty()) {
            return Response.ok(remoteAccountFinderService.findAccountsByIds(id)).build();
        } else {
            return Response.ok(Collections.emptyList()).build();
        }
    }

    @RolesAllowed("CAN_SEE_USERS")
    @GET
    @Path("find/{id}")
    public Response findById(@PathParam("id") @Valid @NotEmpty String id) {
        return remoteAccountFinderService.findAccountById(id).map(Response::ok)
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND))
                .build();
    }
}
