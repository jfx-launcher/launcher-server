package dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler.property;

import dk.xakeps.jfx.server.domain.model.profile.GameProfileException;
import dk.xakeps.jfx.server.domain.model.profile.property.PropertyValue;
import dk.xakeps.jfx.server.domain.model.profile.property.texture.TexturesPropertyValue;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.PropertyValueDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;

@ApplicationScoped
public class PropertyValueDtoAssembler {
    private final TexturePropertyValueDtoAssembler texturePropertyValueDtoAssembler;

    @Inject
    public PropertyValueDtoAssembler(TexturePropertyValueDtoAssembler texturePropertyValueDtoAssembler) {
        this.texturePropertyValueDtoAssembler = texturePropertyValueDtoAssembler;
    }

    public PropertyValueDto toDto(PropertyValue propertyValue, URI baseUri) {
        if (propertyValue instanceof TexturesPropertyValue) {
            return texturePropertyValueDtoAssembler.toDto((TexturesPropertyValue) propertyValue, baseUri);
        }

        throw new GameProfileException(String.format("Class '%s' is unsupported", propertyValue.getClass()));
    }
}
