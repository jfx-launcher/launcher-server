package dk.xakeps.jfx.server.interfaces.updates.facade;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageData;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageList;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;

public interface UpdatePackageServiceFacade {
    UpdatePackageList getUpdatePackageManifest(UpdatePackageType updatePackageType, URI baseUri);

    Optional<Path> getFile(UpdatePackageType updatePackageType, String version, String arch, String system, String filePath);

    Optional<UpdatePackageData> getUpdatePackageData(UpdatePackageType updatePackageType, String version, String arch, String system, URI baseUri);

    void uploadUpdateVersion(UpdatePackageType updatePackageType, String version, String arch, String system, InputStream file) throws IOException;

    void removeUpdateVersion(UpdatePackageType updatePackageType, String version, String arch, String system);

    void enableVersion(UpdatePackageType updatePackageType, String version, String arch, String system);

    void deletePackage(UpdatePackageType updatePackageType);

    void deletePackage(UpdatePackageType updatePackageType, String arch, String system);
}
