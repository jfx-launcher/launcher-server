package dk.xakeps.jfx.server.interfaces.profile.rest;

import dk.xakeps.jfx.server.interfaces.profile.facade.ProfileServiceFacade;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.UUID;

@Path("profile-service/profile/{uuid}")
public class ProfileResource {
    private final ProfileServiceFacade profileServiceFacade;

    @Inject
    public ProfileResource(ProfileServiceFacade profileServiceFacade) {
        this.profileServiceFacade = profileServiceFacade;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response get(@PathParam("uuid") UUID uuid,
                        @QueryParam("full") @DefaultValue("false") boolean full,
                        @QueryParam("signed") @DefaultValue("false") boolean signed,
                        @Context UriInfo uriInfo) {
        return profileServiceFacade.findProfile(uuid, full, signed, uriInfo.getBaseUri())
                .map(Response::ok)
                .orElseGet(Response::noContent)
                .build();
    }
}
