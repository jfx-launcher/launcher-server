package dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.update.*;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.FileArtifactDto;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.SymlinkArtifactDto;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.UpdatePackageData;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class UpdatePackageDataAssembler {
    private final FileArtifactDtoAssembler fileArtifactDtoAssembler;
    private final SymlinkArtifactDtoAssembler symlinkArtifactDtoAssembler;

    @Inject
    public UpdatePackageDataAssembler(FileArtifactDtoAssembler fileArtifactDtoAssembler,
                                      SymlinkArtifactDtoAssembler symlinkArtifactDtoAssembler) {
        this.fileArtifactDtoAssembler = fileArtifactDtoAssembler;
        this.symlinkArtifactDtoAssembler = symlinkArtifactDtoAssembler;
    }

    public UpdatePackageData toDto(UpdateVersion updateVersion, URI baseUri) {
        String version = updateVersion.getVersion();
        UpdatePackageType updatePackageType = updateVersion.getUpdatePackage().getUpdatePackageType();
        String arch = updateVersion.getUpdatePackage().getPlatform().getArch();
        String system = updateVersion.getUpdatePackage().getPlatform().getSystem();

        List<FileArtifactDto> files = new ArrayList<>();
        List<SymlinkArtifactDto> symlinks = new ArrayList<>(4);

        for (Artifact artifact : updateVersion.getArtifacts()) {
            if (artifact instanceof FileArtifact a) {
                files.add(fileArtifactDtoAssembler.toDto(updateVersion, a, baseUri));
            } else if (artifact instanceof SymlinkArtifact a) {
                symlinks.add(symlinkArtifactDtoAssembler.toDto(a));
            } else {
                throw new IllegalArgumentException("Can't convert to dto, class: " + arch.getClass());
            }
        }

        return new UpdatePackageData(updatePackageType, version, arch, system, files, symlinks);
    }
}
