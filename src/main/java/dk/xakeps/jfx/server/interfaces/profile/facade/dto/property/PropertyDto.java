package dk.xakeps.jfx.server.interfaces.profile.facade.dto.property;

import com.fasterxml.jackson.annotation.JsonProperty;

public record PropertyDto(@JsonProperty("name") String name,
                          @JsonProperty("value") String value,
                          @JsonProperty("signature") String signature) {
}
