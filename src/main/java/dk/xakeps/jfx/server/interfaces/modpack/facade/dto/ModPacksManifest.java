package dk.xakeps.jfx.server.interfaces.modpack.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public record ModPacksManifest(@JsonProperty("modPacks") List<ModPackInfo> modPacks) {
    public ModPacksManifest {
        Objects.requireNonNull(modPacks, "modPacks");
    }
}
