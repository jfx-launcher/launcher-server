package dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler;

import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackInfo;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPacksManifest;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class ModPacksManifestAssembler {
    public ModPacksManifest toDto(List<ModPackInfo> modPackInfoList) {
        return new ModPacksManifest(modPackInfoList);
    }
}
