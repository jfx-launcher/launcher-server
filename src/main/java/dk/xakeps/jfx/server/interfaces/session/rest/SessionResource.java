package dk.xakeps.jfx.server.interfaces.session.rest;

import dk.xakeps.jfx.server.application.DashlessUUIDUtil;
import dk.xakeps.jfx.server.interfaces.session.facade.SessionServiceFacade;
import dk.xakeps.jfx.server.interfaces.session.facade.dto.JoinServerRequest;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Produces(MediaType.APPLICATION_JSON)
@Path("session-service")
public class SessionResource {
    private final SessionServiceFacade sessionServiceFacade;

    @Inject
    public SessionResource(SessionServiceFacade sessionServiceFacade) {
        this.sessionServiceFacade = sessionServiceFacade;
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed("JOIN_GAME")
    @POST
    @Path("join")
    public Response join(@Valid JoinServerRequest request) {
        if (sessionServiceFacade.join(request)) {
            return Response.noContent().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @GET
    @Path("hasJoined")
    public Response hasJoined(@QueryParam("username") @Valid @NotEmpty String username,
                              @QueryParam("serverId") @Valid @NotEmpty String serverId,
                              @QueryParam("ip") String playerIp,
                              @Context UriInfo uriInfo) {
        return sessionServiceFacade.hasJoined(username, serverId, playerIp, uriInfo.getBaseUri())
                .map(Response::ok)
                .orElseGet(Response::noContent)
                .build();
    }

    @GET
    @Path("profile/{uuid}")
    public Response getProfile(@PathParam("uuid") String uuid,
                               @QueryParam("unsigned") @DefaultValue("true") boolean unsigned,
                               @Context UriInfo uriInfo) {
        return sessionServiceFacade.getProfile(DashlessUUIDUtil.fromString(uuid), !unsigned, uriInfo.getBaseUri())
                .map(Response::ok)
                .orElseGet(Response::noContent)
                .build();
    }
}
