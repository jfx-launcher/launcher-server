package dk.xakeps.jfx.server.interfaces.profile.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public record CreateProfileRequest(@JsonProperty("name") @NotNull String name) {
}
