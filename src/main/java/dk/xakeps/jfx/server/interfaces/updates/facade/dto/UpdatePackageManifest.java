package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;

import java.util.List;

public record UpdatePackageManifest(UpdatePackageType updatePackageType,
                                    String arch,
                                    String system,
                                    String enabledVersion,
                                    List<UpdatePackageInfo> updates) {
}
