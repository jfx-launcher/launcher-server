package dk.xakeps.jfx.server.interfaces.profile.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.PropertyDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.UUIDDeser;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.UUIDSer;

import java.util.List;
import java.util.UUID;

public record GameProfileDto(@JsonProperty("id") @JsonDeserialize(using = UUIDDeser.class) @JsonSerialize(using = UUIDSer.class) UUID id,
                             @JsonProperty("name") String name,
                             @JsonProperty("properties") List<PropertyDto> properties) {
}
