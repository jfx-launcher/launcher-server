package dk.xakeps.jfx.server.interfaces.modpack.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.modpack.ModPack;
import dk.xakeps.jfx.server.domain.model.modpack.ModPackVersion;
import dk.xakeps.jfx.server.interfaces.modpack.facade.ModPackVersionRewrite;
import dk.xakeps.jfx.server.interfaces.modpack.facade.dto.ModPackVersionDto;
import dk.xakeps.jfx.server.interfaces.modpack.rest.ModPackResource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Set;

@ApplicationScoped
public class ModPackVersionAssembler {
    private final ModPackVersionRewrite modPackVersionRewrite;

    @Inject
    public ModPackVersionAssembler(ModPackVersionRewrite modPackVersionRewrite) {
        this.modPackVersionRewrite = modPackVersionRewrite;
    }

    public ModPackVersionDto toDto(ModPackVersion modPackVersion, URI baseUri) {
        ModPack modPack = modPackVersion.getModPack();
        String modPackId = modPack.getModPackId();
        String minecraftVersion = modPackVersion.getMinecraftVersion();
        String javaVersion = modPackVersion.getJavaVersion();
        Set<String> ignoredFiles = modPackVersion.getIgnoredFiles();
        String pathVersion = modPackVersionRewrite.toString(modPackVersion.getUploadTime());
        return new ModPackVersionDto(modPackVersion.getUploadTime().toString(),
                minecraftVersion,
                javaVersion,
                ignoredFiles,
                UriBuilder.fromUri(baseUri)
                        .path(ModPackResource.class)
                        .path(ModPackResource.class, "modPack")
                        .build(modPackId, pathVersion));
    }
}
