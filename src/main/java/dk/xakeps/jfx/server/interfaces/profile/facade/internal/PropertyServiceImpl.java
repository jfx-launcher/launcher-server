package dk.xakeps.jfx.server.interfaces.profile.facade.internal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.server.application.SignService;
import dk.xakeps.jfx.server.domain.model.profile.GameProfileException;
import dk.xakeps.jfx.server.interfaces.profile.facade.PropertyService;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.property.PropertyValueDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.util.Base64;

@ApplicationScoped
public class PropertyServiceImpl implements PropertyService {
    private final ObjectMapper objectMapper;
    private final SignService signService;

    @Inject
    public PropertyServiceImpl(ObjectMapper objectMapper, SignService signService) {
        this.objectMapper = objectMapper;
        this.signService = signService;
    }

    @Override
    public String propertyToBase64(PropertyValueDto propertyDto) {
        String s;
        try {
            s = objectMapper.writeValueAsString(propertyDto);
        } catch (JsonProcessingException e) {
            throw new GameProfileException("Can't serialize property value", e);
        }
        return Base64.getEncoder().encodeToString(s.getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public String propertySignature(String propertyBase64) throws SignatureException {
        return signService.signAndGetBase64(propertyBase64.getBytes(StandardCharsets.UTF_8));
    }
}
