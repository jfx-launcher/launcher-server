package dk.xakeps.jfx.server.interfaces.updates.facade.internal;

import dk.xakeps.jfx.server.infrastructure.persistence.jpa.PlatformRepo;
import dk.xakeps.jfx.server.interfaces.updates.facade.PlatformFacade;
import dk.xakeps.jfx.server.interfaces.updates.facade.dto.PlatformManifest;
import dk.xakeps.jfx.server.interfaces.updates.facade.internal.assembler.PlatformManifestAssembler;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PlatformFacadeImpl implements PlatformFacade {
    private final PlatformRepo platformRepo;
    private final PlatformManifestAssembler platformManifestAssembler;

    @Inject
    public PlatformFacadeImpl(PlatformRepo platformRepo,
                              PlatformManifestAssembler platformManifestAssembler) {
        this.platformRepo = platformRepo;
        this.platformManifestAssembler = platformManifestAssembler;
    }

    @Override
    public PlatformManifest getPlatforms() {
        return platformManifestAssembler.toDto(platformRepo.listAll());
    }
}
