package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

public record SymlinkArtifactDto(String target, String source) {
}
