package dk.xakeps.jfx.server.interfaces.profile.facade.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public record GetOrCreateRequest(@JsonProperty("full") boolean full, @JsonProperty("signed") boolean signed) {
}
