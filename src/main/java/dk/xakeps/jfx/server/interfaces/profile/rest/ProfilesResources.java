package dk.xakeps.jfx.server.interfaces.profile.rest;

import dk.xakeps.jfx.server.interfaces.profile.facade.ProfileServiceFacade;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.CreateProfileRequest;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.CreateProfileResult;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GetOrCreateRequest;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.ProfileListDto;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.Set;

@RequestScoped
@RolesAllowed("USER")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("profile-service/profiles")
public class ProfilesResources {
    private final ProfileServiceFacade profileServiceFacade;

    private final JsonWebToken token;

    @Inject
    public ProfilesResources(ProfileServiceFacade profileServiceFacade, JsonWebToken token) {
        this.profileServiceFacade = profileServiceFacade;
        this.token = token;
    }

    @RolesAllowed("USER")
    @POST
    @Path("getOrCreate")
    public Response getOrCreate(GetOrCreateRequest request, @Context UriInfo uriInfo) {
        String name = token.getName();
        String accountId = token.getSubject();
        ProfileListDto profileListDto = profileServiceFacade.findProfilesOrCreate(accountId, name, request, uriInfo.getBaseUri());
        return Response.ok(profileListDto).build();
    }

    @RolesAllowed("MANY_PROFILES")
    @POST
    @Path("create")
    public Response createProfile(@Valid @NotNull CreateProfileRequest request, @Context UriInfo uriInfo) {
        UriBuilder builder = uriInfo.getBaseUriBuilder().path(ProfileResource.class);

        String accountId = token.getSubject();
        CreateProfileResult createProfileResult = profileServiceFacade.createProfile(accountId, request.name());
        URI profileUri = builder.build(createProfileResult.profileId());
        if (!createProfileResult.created()) {
            return Response.status(Response.Status.SEE_OTHER)
                    .location(profileUri).build();
        }

        return Response.created(profileUri).build();
    }

    @POST
    public Response findProfileIds(@Valid @NotNull @Size(max = 10) Set<@NotEmpty String> names, @Context UriInfo uriInfo) {
        return Response.ok(profileServiceFacade.findProfileIds(names, uriInfo.getBaseUri())).build();
    }
}
