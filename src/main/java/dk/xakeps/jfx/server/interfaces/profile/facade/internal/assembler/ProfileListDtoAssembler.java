package dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler;

import dk.xakeps.jfx.server.domain.model.profile.Account;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GameProfileDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.ProfileListDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ProfileListDtoAssembler {
    private final GameProfileDtoAssembler gameProfileDtoAssembler;

    @Inject
    public ProfileListDtoAssembler(GameProfileDtoAssembler gameProfileDtoAssembler) {
        this.gameProfileDtoAssembler = gameProfileDtoAssembler;
    }

    public ProfileListDto toDto(Account account, boolean full, boolean signed, URI baseUri) {
        GameProfileDto selectedProfile = gameProfileDtoAssembler.toDto(account.getSelectedProfile(), full, signed, baseUri);
        List<GameProfileDto> availableProfiles = account.getGameProfiles().values().stream()
                .map(gameProfile -> gameProfileDtoAssembler.toDto(gameProfile, full, signed, baseUri))
                .collect(Collectors.toList());
        return new ProfileListDto(selectedProfile.id(), availableProfiles);
    }
}
