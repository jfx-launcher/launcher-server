package dk.xakeps.jfx.server.interfaces.profile.facade.internal;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import dk.xakeps.jfx.server.application.DashlessUUIDUtil;

import java.io.IOException;
import java.util.UUID;

public class UUIDSer extends JsonSerializer<UUID> {
    @Override
    public void serialize(UUID value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(DashlessUUIDUtil.toString(value));
    }
}
