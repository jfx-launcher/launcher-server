package dk.xakeps.jfx.server.interfaces.profile.facade.internal;

import dk.xakeps.jfx.server.application.AccountService;
import dk.xakeps.jfx.server.application.GameProfileService;
import dk.xakeps.jfx.server.domain.model.profile.Account;
import dk.xakeps.jfx.server.domain.model.profile.GameProfile;
import dk.xakeps.jfx.server.domain.model.profile.ProfileUUIDStrategy;
import dk.xakeps.jfx.server.domain.model.profile.property.texture.SkinTextureModel;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.AccountRepo;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.GameProfileRepo;
import dk.xakeps.jfx.server.interfaces.profile.facade.ProfileAlreadyExistsException;
import dk.xakeps.jfx.server.interfaces.profile.facade.ProfileServiceFacade;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.*;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler.GameProfileDtoAssembler;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler.ProfileListDtoAssembler;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.InputStream;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class ProfileServiceFacadeImpl implements ProfileServiceFacade {
    private final AccountService accountService;
    private final GameProfileService gameProfileService;
    private final AccountRepo accountRepo;
    private final GameProfileRepo gameProfileRepo;
    private final ProfileListDtoAssembler profileListDtoAssembler;
    private final GameProfileDtoAssembler gameProfileDtoAssembler;

    private final ProfileUUIDStrategy profileUUIDStrategy;

    @Inject
    public ProfileServiceFacadeImpl(AccountService accountService,
                                    GameProfileService gameProfileService,
                                    AccountRepo accountRepo,
                                    GameProfileRepo gameProfileRepo,
                                    ProfileListDtoAssembler profileListDtoAssembler,
                                    GameProfileDtoAssembler gameProfileDtoAssembler,
                                    @ConfigProperty(name = "profile.uuid-strategy") ProfileUUIDStrategy profileUUIDStrategy) {
        this.accountService = accountService;
        this.gameProfileService = gameProfileService;
        this.accountRepo = accountRepo;
        this.gameProfileRepo = gameProfileRepo;
        this.profileListDtoAssembler = profileListDtoAssembler;
        this.gameProfileDtoAssembler = gameProfileDtoAssembler;
        this.profileUUIDStrategy = profileUUIDStrategy;
    }

    @Override
    public ProfileListDto findProfilesOrCreate(String externalId, String name, GetOrCreateRequest request, URI baseUri) {
        Objects.requireNonNull(externalId, "externalId");
        Objects.requireNonNull(name, "name");
        Account result;
        if (request.full()) {
            result = accountRepo.findByExternalIdWithProfilesAndProperties(externalId).orElse(null);
        } else {
            result = accountRepo.findByExternalId(externalId).orElse(null);
        }
        if (result == null || result.getGameProfiles().isEmpty()) {
            result = accountService.createAccountAndProfile(externalId, name, profileUUIDStrategy);
        }
        return profileListDtoAssembler.toDto(result, request.full(), request.signed(), baseUri);
    }

    @Override
    public Optional<GameProfileDto> findProfile(UUID uuid, boolean full, boolean signed, URI baseUri) {
        Objects.requireNonNull(uuid, "uuid");
        Optional<GameProfile> result;
        if (full) {
            result = gameProfileRepo.findByUuidWithProperties(uuid);
        } else {
            result = gameProfileRepo.findByUuid(uuid);
        }
        return result.map(a -> gameProfileDtoAssembler.toDto(a, full, signed, baseUri));
    }

    @Transactional
    @Override
    public CreateProfileResult createProfile(String externalId, String name) {
        Objects.requireNonNull(externalId, "externalId");
        Objects.requireNonNull(name, "name");
        GameProfile existingProfile = gameProfileRepo.findByAccountExternalIdAndName(externalId, name).orElse(null);
        if (existingProfile != null) {
            return new CreateProfileResult(existingProfile.getUuid(), false);
        } else if (gameProfileRepo.existsByName(name)) {
            throw new ProfileAlreadyExistsException(String.format("Name %s already occupied", name));
        }

        GameProfile profile = accountService.createProfile(externalId, name, profileUUIDStrategy);
        return new CreateProfileResult(profile.getUuid(), true);
    }

    @Override
    public List<GameProfileDto> findProfileIds(Set<String> names, URI baseUri) {
        return gameProfileRepo.findByNames(names)
                .stream()
                .map(p -> gameProfileDtoAssembler.toDto(p, false, false, baseUri))
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void changeSkin(String externalId, UUID profileId, SkinTextureModel skinType, InputStream skinStream) {
        gameProfileService.changeSkin(externalId, profileId, skinType, skinStream);
    }

    @Override
    public void changeCape(String externalId, UUID profileId, InputStream skinStream) {
        gameProfileService.changeCape(externalId, profileId, skinStream);
    }
}
