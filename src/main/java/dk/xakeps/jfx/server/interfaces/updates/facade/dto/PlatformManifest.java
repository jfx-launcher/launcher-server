package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;

import java.util.List;
import java.util.Map;

public record PlatformManifest(Map<String, PlatformDto> platforms, List<UpdatePackageType> updatePackageTypes) {
}
