package dk.xakeps.jfx.server.interfaces.updates.facade.dto;

import java.util.Set;

public record PlatformDto(String system, Set<String> architectures) {
}
