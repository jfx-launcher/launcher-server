package dk.xakeps.jfx.server.interfaces.session.facade.internal;

import dk.xakeps.jfx.server.domain.model.profile.GameProfile;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.GameProfileRepo;
import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GameProfileDto;
import dk.xakeps.jfx.server.interfaces.profile.facade.internal.assembler.GameProfileDtoAssembler;
import dk.xakeps.jfx.server.application.SessionService;
import dk.xakeps.jfx.server.domain.model.session.Session;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.SessionRepo;
import dk.xakeps.jfx.server.interfaces.session.facade.SessionServiceFacade;
import dk.xakeps.jfx.server.interfaces.session.facade.dto.JoinServerRequest;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.net.URI;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class SessionServiceFacadeImpl implements SessionServiceFacade {
    private final GameProfileRepo gameProfileRepo;
    private final SessionService sessionService;
    private final SessionRepo sessionRepo;
    private final GameProfileDtoAssembler gameProfileDtoAssembler;

    public SessionServiceFacadeImpl(GameProfileRepo gameProfileRepo,
                                    SessionService sessionService,
                                    SessionRepo sessionRepo,
                                    GameProfileDtoAssembler gameProfileDtoAssembler) {
        this.gameProfileRepo = gameProfileRepo;
        this.sessionService = sessionService;
        this.sessionRepo = sessionRepo;
        this.gameProfileDtoAssembler = gameProfileDtoAssembler;
    }

    @Transactional
    @Override
    public boolean join(JoinServerRequest request) { // todo: validate accessToken
        GameProfile body = gameProfileRepo.findByUuid(request.selectedProfile()).orElse(null);
        if (body == null) {
            return false;
        }
        String name = body.getName();
        UUID profile = request.selectedProfile();
        String serverId = request.serverId();
        sessionService.updateSession(name, profile, serverId);
        return true;
    }

    @Override
    public Optional<GameProfileDto> hasJoined(String username, String serverId, String playerIp, URI baseUri) {
        Session session = sessionRepo.findByUsernameAndServerId(username, serverId).orElse(null);
        if (session == null) {
            return Optional.empty();
        }
        UUID profileId = session.getProfileId();
        GameProfile body = gameProfileRepo.findByUuidWithProperties(profileId).orElse(null);
        if (body == null) {
            return Optional.empty();
        }

        return Optional.of(gameProfileDtoAssembler.toDto(body, true, true, baseUri));
    }

    @Override
    public Optional<GameProfileDto> getProfile(UUID profileId, boolean signed, URI baseUri) {
        return gameProfileRepo.findByUuidWithProperties(profileId)
                .map(a -> gameProfileDtoAssembler.toDto(a, true, signed, baseUri));
    }
}
