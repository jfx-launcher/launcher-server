package dk.xakeps.jfx.server.interfaces.profile.facade.internal;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;
import dk.xakeps.jfx.server.application.DashlessUUIDUtil;

import java.io.IOException;
import java.util.UUID;

public class UUIDDeser extends FromStringDeserializer<UUID> {
    protected UUIDDeser() {
        super(UUID.class);
    }

    @Override
    protected UUID _deserialize(String value, DeserializationContext ctxt) throws IOException {
        return DashlessUUIDUtil.fromString(value);
    }
}
