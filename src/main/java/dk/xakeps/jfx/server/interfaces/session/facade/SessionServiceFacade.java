package dk.xakeps.jfx.server.interfaces.session.facade;

import dk.xakeps.jfx.server.interfaces.profile.facade.dto.GameProfileDto;
import dk.xakeps.jfx.server.interfaces.session.facade.dto.JoinServerRequest;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

public interface SessionServiceFacade {
    boolean join(JoinServerRequest request);
    Optional<GameProfileDto> hasJoined(String username, String serverId, String playerIp, URI baseUri);
    Optional<GameProfileDto> getProfile(UUID profileId, boolean signed, URI baseUri);
}
