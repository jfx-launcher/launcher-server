package dk.xakeps.jfx.server.interfaces.modpack.facade;

import java.time.Instant;

public interface ModPackVersionRewrite {
    String toString(Instant instant);
    Instant fromString(String instantStr);
}
