package dk.xakeps.jfx.server.application.internal;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import dk.xakeps.jfx.server.application.VersionDirLocator;
import dk.xakeps.jfx.server.application.VersionService;
import dk.xakeps.jfx.server.domain.model.version.ForgeVersionNotSupportedException;
import dk.xakeps.jfx.server.domain.model.version.VersionProcessingException;
import dk.xakeps.jfx.server.domain.model.version.*;
import dk.xakeps.jfx.server.domain.service.RemoteVersionListService;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.VersionMetaRepo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * This service loads real version manifest and merges our own metadata
 */
@ApplicationScoped
public class VersionServiceImpl implements VersionService {
    private final VersionMetaRepo versionMetaRepo;
    private final RemoteVersionListService remoteVersionListService;
    private final VersionDirLocator versionDirLocator;
    private final ObjectMapper objectMapper;

    @Inject
    public VersionServiceImpl(VersionMetaRepo versionMetaRepo,
                              RemoteVersionListService remoteVersionListService,
                              VersionDirLocator versionDirLocator,
                              ObjectMapper objectMapper) {
        this.versionMetaRepo = versionMetaRepo;
        this.remoteVersionListService = remoteVersionListService;
        this.versionDirLocator = versionDirLocator;
        this.objectMapper = objectMapper;
    }

    @Transactional
    @Override
    public VersionManifest getVersionManifest(URI baseUri) {
        VersionManifest versionManifest = remoteVersionListService.getVersionManifest();
        versionMetaRepo.streamAll().map(versionMeta -> toDto(versionMeta, baseUri))
                .forEach(versionManifest.versions()::add);
        return versionManifest;
    }

    @Transactional
    @Override
    public void installForge(Path installerJar) {
        InstallerInfo installerInfo = extractAndProcessVersionData(installerJar);
        Path path = versionDirLocator.findPath(installerInfo.versionData.getId(), installerInfo.installType);

        try {
            Files.createDirectories(path.getParent());
        } catch (IOException e) {
            throw new VersionProcessingException("can't save forge version");
        }
        try {
            switch (installerInfo.installType.getExtension()) {
                case JAR -> Files.copy(installerJar, path, StandardCopyOption.REPLACE_EXISTING);
                case JSON -> {
                    try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
                        objectMapper.enable(SerializationFeature.INDENT_OUTPUT).writeValue(writer, installerInfo.versionData);
                    }
                }
            }
        } catch (IOException e) {
            throw new VersionProcessingException("Can't save forge", e);
        }

        VersionMeta versionMeta = new VersionMeta();
        versionMeta.setVersionId(installerInfo.versionData.getId());
        versionMeta.setVersionType(installerInfo.versionData.getType());
        versionMeta.setUpdateTime(installerInfo.versionData.getUpdateTime());
        versionMeta.setReleaseTime(installerInfo.versionData.getReleaseTime());
        versionMeta.setInstallType(installerInfo.installType);
        versionMetaRepo.persist(versionMeta);
    }

    @Override
    public Optional<Path> getForgeInstaller(String version) {
        Path path = versionDirLocator.findPath(version, InstallType.FORGE_MODERN);
        if (Files.exists(path)) {
            return Optional.of(path);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Path> getForgeJson(String version) {
        Path path = versionDirLocator.findPath(version, InstallType.FORGE_OLD);
        if (Files.exists(path)) {
            return Optional.of(path);
        }
        return Optional.empty();
    }

    private InstallerInfo extractAndProcessVersionData(Path installerJar) {
        try (FileSystem fileSystem = FileSystems.newFileSystem(installerJar)) {
            Path versionJsonPath = fileSystem.getPath("/", "version.json");
            if (Files.exists(versionJsonPath)) {
                try (BufferedReader reader = Files.newBufferedReader(versionJsonPath, StandardCharsets.UTF_8)) {
                    VersionData versionData = objectMapper.readValue(reader, VersionData.class);
                    return processVersionData(versionData);
                }
            } else {
                Path oldVersionJsonPath = fileSystem.getPath("/", "install_profile.json");
                try (BufferedReader reader = Files.newBufferedReader(oldVersionJsonPath, StandardCharsets.UTF_8)) {
                    VersionData versionData = objectMapper.reader()
                            .at(JsonPointer.valueOf("/versionInfo"))
                            .readValue(reader, VersionData.class);
                    return processVersionData(versionData);
                }
            }
        } catch (IOException e) {
            throw new VersionProcessingException("Can't open installer jar", e);
        }
    }

    private InstallerInfo processVersionData(VersionData versionData) {
        String id = versionData.getId();
        String inheritsFrom = versionData.getInheritsFrom();

        if ("1.7.10".equals(inheritsFrom)) {
            if (!"1.7.10-Forge10.13.4.1614-1.7.10".equals(id)) {
                throw new ForgeVersionNotSupportedException("Only 1.7.10-Forge10.13.4.1614-1.7.10 is supported");
            }
            return new InstallerInfo(processOneSevenTen(versionData), InstallType.FORGE_OLD);
        } else if ("1.12.2".equals(inheritsFrom)) {
            if (!"1.12.2-forge-14.23.5.2855".equals(id)) {
                throw new ForgeVersionNotSupportedException("Only 1.12.2-forge-14.23.5.2855 is supported");
            }
            return new InstallerInfo(processOneTwelveTwo(versionData), InstallType.FORGE_OLD);
        }
        String forgeVersion = id.replace(inheritsFrom + "-forge-", "");
        if (forgeVersion.compareTo("36.1.65") > 0) {
            return new InstallerInfo(processModern(versionData), InstallType.FORGE_MODERN);
        } else {
            throw new ForgeVersionNotSupportedException("This forge version is not supported");
        }
    }

    private VersionData processOneSevenTen(VersionData versionData) {
        try (InputStream is = getClass().getResourceAsStream("/1.7.10-Forge10.13.4.1614-libs.json")) {
            List<Object> libs = objectMapper.readValue(is, new TypeReference<>() {});
            versionData.getOther().put("libraries", libs);
        } catch (IOException e) {
            throw new VersionProcessingException("Can't process 1.7.10 forge", e);
        }
        return versionData;
    }

    @SuppressWarnings("unchecked")
    private VersionData processOneTwelveTwo(VersionData versionData) {
        Object libraries = versionData.getOther().get("libraries");
        if (!(libraries instanceof List<?> l)) {
            throw new VersionProcessingException("Can't find libraries");
        }

        Map<String, Object> lib = findLib("net.minecraftforge:forge:1.12.2-14.23.5.2855", l);
        if (lib == null) {
            throw new VersionProcessingException("Forge library not found");
        }

        lib.put("name", "net.minecraftforge:forge:jar:universal:1.12.2-14.23.5.2855");
        Object downloadsObj = lib.get("downloads");
        if (!(downloadsObj instanceof Map<?, ?> downloads)) {
            throw new VersionProcessingException("downloads not found in forge library");
        }
        Object artifactObj = downloads.get("artifact");
        if (!(artifactObj instanceof Map<?, ?> artifact)) {
            throw new VersionProcessingException("artifact not found in forge library");
        }

        Object pathObj = artifact.get("path");
        if (!(pathObj instanceof String path)) {
            throw new VersionProcessingException("path not found in artifact in forge library");
        }
        String pathReplaced = path.replace(".jar", "-universal.jar");
        ((Map<String, Object>) artifact).put("path", pathReplaced);

        ((Map<String, Object>) artifact).put("url", "https://files.minecraftforge.net/maven/" + pathReplaced);

        return versionData;
    }

    private VersionData processModern(VersionData versionData) {
        return versionData;
    }

    private record InstallerInfo(VersionData versionData, InstallType installType) {}

    @SuppressWarnings("unchecked")
    private static Map<String, Object> findLib(String name, List<?> libs) {
        for (Object lib : libs) {
            if (lib instanceof Map<?, ?> m) {
                if (name.equals(m.get("name"))) {
                    return (Map<String, Object>) m;
                }
            }
        }
        return null;
    }

    private static VersionManifestEntry toDto(VersionMeta versionMeta, URI baseUri) {
        InstallType installType = versionMeta.getInstallType();
        VersionConfig versionConfig = null;
        String ext = "json";
        if (installType != null) {
            versionConfig = new VersionConfig(installType);
            ext = installType.getExtension().getAsString();
        }
        URI uri = baseUri.resolve("/version-service/version/forge/" + versionMeta.getVersionId() + "." + ext);
        return new VersionManifestEntry(
                versionMeta.getVersionId(),
                versionMeta.getVersionType(),
                uri,
                versionMeta.getUpdateTime(),
                versionMeta.getReleaseTime(),
                versionConfig
        );
    }
}
