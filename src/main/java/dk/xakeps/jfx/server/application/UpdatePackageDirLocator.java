package dk.xakeps.jfx.server.application;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;

import java.nio.file.Path;

public interface UpdatePackageDirLocator {
    Path findPath(UpdatePackageType updatePackageType, String arch, String system, String version);
}
