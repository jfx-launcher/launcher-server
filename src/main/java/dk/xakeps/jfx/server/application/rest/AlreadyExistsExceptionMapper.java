package dk.xakeps.jfx.server.application.rest;

import dk.xakeps.jfx.server.domain.model.update.UpdateAlreadyExistsException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AlreadyExistsExceptionMapper implements ExceptionMapper<UpdateAlreadyExistsException> {
    @Override
    public Response toResponse(UpdateAlreadyExistsException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorResponse(exception.getClass().getSimpleName(), exception.getMessage()))
                .build();
    }
}
