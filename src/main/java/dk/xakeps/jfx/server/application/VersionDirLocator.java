package dk.xakeps.jfx.server.application;

import dk.xakeps.jfx.server.domain.model.version.InstallType;

import java.nio.file.Path;

public interface VersionDirLocator {
    Path findPath(String version, InstallType installType);
}
