package dk.xakeps.jfx.server.application.internal;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dk.xakeps.jfx.server.domain.model.version.OffsetDateTimeDeserializer;
import dk.xakeps.jfx.server.domain.model.version.OffsetDateTimeSerializer;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

public class VersionData {
    @JsonProperty("id")
    private final String id;
    @JsonProperty("inheritsFrom")
    private final String inheritsFrom;

    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonProperty("releaseTime")
    private final OffsetDateTime releaseTime;
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonProperty("time")
    private final OffsetDateTime updateTime;

    @JsonProperty("type")
    private final String type;

    @JsonAnyGetter
    @JsonAnySetter
    private final Map<String, Object> other = new HashMap<>();

    @JsonCreator
    public VersionData(@JsonProperty("id") String id,
                       @JsonProperty("inheritsFrom") String inheritsFrom,
                       @JsonProperty("releaseTime") @JsonDeserialize(using = OffsetDateTimeDeserializer.class) OffsetDateTime releaseTime,
                       @JsonProperty("time") @JsonDeserialize(using = OffsetDateTimeDeserializer.class) OffsetDateTime updateTime,
                       @JsonProperty("type") String type) {
        this.id = id;
        this.inheritsFrom = inheritsFrom;
        this.releaseTime = releaseTime;
        this.updateTime = updateTime;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getInheritsFrom() {
        return inheritsFrom;
    }

    public OffsetDateTime getReleaseTime() {
        return releaseTime;
    }

    public OffsetDateTime getUpdateTime() {
        return updateTime;
    }

    public String getType() {
        return type;
    }

    @JsonAnyGetter
    @JsonAnySetter
    public Map<String, Object> getOther() {
        return other;
    }
}
