package dk.xakeps.jfx.server.application.rest;

import dk.xakeps.jfx.server.domain.model.version.VersionProcessingException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class VersionProcessingExceptionMapper implements ExceptionMapper<VersionProcessingException> {
    @Override
    public Response toResponse(VersionProcessingException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorResponse(exception.getClass().getSimpleName(), exception.getMessage()))
                .build();
    }
}
