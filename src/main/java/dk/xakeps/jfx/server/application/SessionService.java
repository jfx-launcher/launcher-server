package dk.xakeps.jfx.server.application;

import java.util.UUID;

public interface SessionService {
    void updateSession(String username, UUID profileId, String serverId);
}
