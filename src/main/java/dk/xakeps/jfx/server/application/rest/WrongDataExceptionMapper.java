package dk.xakeps.jfx.server.application.rest;

import dk.xakeps.jfx.server.domain.model.shared.WrongDataException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class WrongDataExceptionMapper implements ExceptionMapper<WrongDataException> {
    @Override
    public Response toResponse(WrongDataException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorResponse(exception.getClass().getSimpleName(), exception.getMessage()))
                .build();
    }
}
