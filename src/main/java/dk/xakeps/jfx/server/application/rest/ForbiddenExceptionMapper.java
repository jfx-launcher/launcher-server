package dk.xakeps.jfx.server.application.rest;

import dk.xakeps.jfx.server.domain.model.shared.ForbiddenException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {
    @Override
    public Response toResponse(ForbiddenException exception) {
        return Response.status(Response.Status.FORBIDDEN)
                .entity(new ErrorResponse(exception.getClass().getSimpleName(), exception.getMessage()))
                .build();
    }
}
