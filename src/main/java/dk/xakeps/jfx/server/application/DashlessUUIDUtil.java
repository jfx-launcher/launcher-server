package dk.xakeps.jfx.server.application;

import java.util.UUID;

public class DashlessUUIDUtil {
    public static String toString(UUID uuid) {
        return uuid.toString().replace("-", "");
    }

    public static UUID fromString(String uuid) {
        return UUID.fromString(uuid.replaceFirst("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));
    }
}
