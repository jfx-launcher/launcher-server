package dk.xakeps.jfx.server.application.rest;

import dk.xakeps.jfx.server.domain.model.profile.PropertyException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PropertyExceptionMapper implements ExceptionMapper<PropertyException> {
    @Override
    public Response toResponse(PropertyException exception) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new ErrorResponse(exception.getClass().getSimpleName(), exception.getMessage()))
                .build();
    }
}
