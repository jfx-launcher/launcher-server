package dk.xakeps.jfx.server.application.rest;

import dk.xakeps.jfx.server.domain.model.profile.TextureException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TextureExceptionMapper implements ExceptionMapper<TextureException> {
    @Override
    public Response toResponse(TextureException exception) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new ErrorResponse(exception.getClass().getSimpleName(), exception.getMessage()))
                .build();
    }
}
