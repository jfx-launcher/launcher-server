package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.application.SessionService;
import dk.xakeps.jfx.server.domain.model.session.Session;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.SessionRepo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.UUID;

@ApplicationScoped
public class SessionServiceImpl implements SessionService {
    private final SessionRepo sessionRepo;

    @Inject
    public SessionServiceImpl(SessionRepo sessionRepo) {
        this.sessionRepo = sessionRepo;
    }

    @Override
    public void updateSession(String username, UUID profileId, String serverId) {
        Session session = sessionRepo.findByProfileId(profileId).orElse(null);
        if (session == null) {
            session = new Session();
        }
        session.setUsername(username);
        session.setProfileId(profileId);
        session.setServerId(serverId);
        sessionRepo.persist(session);
    }
}
