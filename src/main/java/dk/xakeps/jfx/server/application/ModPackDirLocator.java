package dk.xakeps.jfx.server.application;

import dk.xakeps.jfx.server.domain.model.modpack.ModPackVersion;

import java.nio.file.Path;
import java.time.Instant;

public interface ModPackDirLocator {
    Path findPath(String modPackId, Instant uploadTime);

    default Path findPath(ModPackVersion modPackVersion) {
        String modPackId = modPackVersion.getModPack().getModPackId();
        Instant uploadTime = modPackVersion.getUploadTime();
        return findPath(modPackId, uploadTime);
    }
}
