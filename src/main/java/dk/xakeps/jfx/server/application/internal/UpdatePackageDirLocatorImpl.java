package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.application.UpdatePackageDirLocator;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;

import javax.enterprise.context.ApplicationScoped;
import java.nio.file.Path;
import java.util.Locale;

@ApplicationScoped
public class UpdatePackageDirLocatorImpl implements UpdatePackageDirLocator {
    @Override
    public Path findPath(UpdatePackageType updatePackageType, String arch, String system, String version) {
        return Path.of("updates.d", updatePackageType.toString().toLowerCase(Locale.ROOT),
                system, arch, version).toAbsolutePath();
    }
}
