package dk.xakeps.jfx.server.application;

import dk.xakeps.jfx.server.domain.model.update.UpdatePackage;
import dk.xakeps.jfx.server.domain.model.update.UpdatePackageType;
import dk.xakeps.jfx.server.domain.model.update.UpdateVersion;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

public interface UpdatePackageService {
    void save(UpdatePackageType updatePackageType, String version, String arch, String system, Path zipFile) throws IOException;

    void enable(UpdatePackageType updatePackageType, String version, String arch, String system);

    List<UpdateVersion> listVersions(UpdatePackageType updatePackageType);
    List<UpdatePackage> listPackages(UpdatePackageType updatePackageType);

    Optional<Path> getFile(UpdatePackageType updatePackageType, String version, String arch, String system, String filePath);

    Optional<UpdateVersion> getVersionWithArtifacts(UpdatePackageType updatePackageType, String version, String arch, String system);

    void removeVersion(UpdatePackageType updatePackageType, String version, String arch, String system);

    void deletePackage(UpdatePackageType updatePackageType);

    void deletePackage(UpdatePackageType updatePackageType, String arch, String system);
}
