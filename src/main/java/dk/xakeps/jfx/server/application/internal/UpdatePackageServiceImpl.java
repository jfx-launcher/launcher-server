package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.application.UpdatePackageDirLocator;
import dk.xakeps.jfx.server.application.UpdatePackageService;
import dk.xakeps.jfx.server.domain.model.shared.ForbiddenException;
import dk.xakeps.jfx.server.domain.model.shared.MessageDigestException;
import dk.xakeps.jfx.server.domain.model.update.*;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.PlatformRepo;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.UpdatePackageRepo;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.UpdateVersionRepo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Stream;
import java.util.zip.ZipException;

@ApplicationScoped
public class UpdatePackageServiceImpl implements UpdatePackageService {
    private final PlatformRepo platformRepo;
    private final UpdatePackageRepo updatePackageRepo;
    private final UpdateVersionRepo updateVersionRepo;
    private final UpdatePackageDirLocator updatePackageDirLocator;

    @Inject
    public UpdatePackageServiceImpl(PlatformRepo platformRepo,
                                    UpdatePackageRepo updatePackageRepo,
                                    UpdateVersionRepo updateVersionRepo,
                                    UpdatePackageDirLocator updatePackageDirLocator) {
        this.platformRepo = platformRepo;
        this.updatePackageRepo = updatePackageRepo;
        this.updateVersionRepo = updateVersionRepo;
        this.updatePackageDirLocator = updatePackageDirLocator;
    }

    @Transactional
    @Override
    public void save(UpdatePackageType updatePackageType, String version, String arch, String system, Path zipFile)
            throws IOException {
        UpdatePackage updatePackage = updatePackageRepo.find(updatePackageType, arch, system).orElse(null);
        if (updatePackage == null) {
            updatePackage = new UpdatePackage();
            Platform platform = platformRepo.findByIds(arch, system).orElse(null);
            if (platform == null) {
                throw new PlatformNotFoundException();
            }
            updatePackage.setUpdatePackageType(updatePackageType);
            updatePackage.setPlatform(platform);
        }

        UpdateVersion updateVersionCheck = updateVersionRepo.findByVersion(updatePackageType, version, arch, system)
                .orElse(null);
        if (updateVersionCheck != null) {
            throw new UpdateVersionUpdateAlreadyExistsException();
        }

        Set<UpdateVersion> packageVersions = updatePackage.getPackageVersions();
        if (packageVersions == null) {
            packageVersions = new HashSet<>();
            updatePackage.setPackageVersions(packageVersions);
        }

        Path path = updatePackageDirLocator.findPath(updatePackageType, arch, system, version);
        List<Artifact> artifacts = extractZip(path, zipFile);

        UpdateVersion updateVersion = new UpdateVersion();
        updateVersion.setVersion(version);
        updateVersion.setArtifacts(artifacts);
        updateVersion.setUpdatePackage(updatePackage);
        packageVersions.add(updateVersion);

        updatePackageRepo.persist(updatePackage);
    }

    @Transactional
    @Override
    public void enable(UpdatePackageType updatePackageType, String version, String arch, String system) {
        UpdatePackage updatePackage = updatePackageRepo.find(updatePackageType, arch, system).orElse(null);
        if (updatePackage == null) {
            throw new UpdatePackageNotFoundException();
        }

        UpdateVersion updateVersion = updateVersionRepo.findByVersion(updatePackageType, version, arch, system)
                .orElse(null);
        if (updateVersion == null) {
            throw new UpdateVersionNotFoundException();
        }

        updatePackage.setEnabledVersion(updateVersion);
        updatePackageRepo.persist(updatePackage);
    }

    @Transactional
    @Override
    public List<UpdateVersion> listVersions(UpdatePackageType updatePackageType) {
        return updateVersionRepo.listByType(updatePackageType);
    }

    @Override
    public List<UpdatePackage> listPackages(UpdatePackageType updatePackageType) {
        return updatePackageRepo.listByType(updatePackageType);
    }

    @Override
    public Optional<Path> getFile(UpdatePackageType updatePackageType, String version, String arch, String system, String filePath) {
        Path path = updatePackageDirLocator.findPath(updatePackageType, arch, system, version);
        Path file = path.resolve(filePath);
        if (!file.startsWith(path)) {
            return Optional.empty();
        }
        if (Files.notExists(file)) {
            return Optional.empty();
        }
        return Optional.of(file);
    }

    @Transactional
    @Override
    public Optional<UpdateVersion> getVersionWithArtifacts(UpdatePackageType updatePackageType, String version, String arch, String system) {
        return updateVersionRepo.findWithArtifacts(updatePackageType, version, arch, system);
    }

    @Transactional
    @Override
    public void removeVersion(UpdatePackageType updatePackageType, String version, String arch, String system) {
        updateVersionRepo.deleteVersion(updatePackageType, version, arch, system);
    }

    @Transactional
    @Override
    public void deletePackage(UpdatePackageType updatePackageType) {
        updatePackageRepo.deleteByPackageType(updatePackageType);
    }

    @Transactional
    @Override
    public void deletePackage(UpdatePackageType updatePackageType, String arch, String system) {
        updatePackageRepo.deletePackage(updatePackageType, arch, system);
    }

    private List<Artifact> extractZip(Path localFsRoot, Path archiveFile) throws IOException {
        List<Artifact> files = new ArrayList<>();
        Files.createDirectories(localFsRoot);
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new MessageDigestException("Can't find SHA-1 MessageDigest", e);
        }
        try (FileSystem fileSystem = FileSystems.newFileSystem(archiveFile,
                Thread.currentThread().getContextClassLoader())) {
            Path zipRootPath = fileSystem.getPath("/");
            try (Stream<Path> walk = Files.walk(zipRootPath)) {
                Iterator<Path> iterator = walk.iterator();
                while (iterator.hasNext()) {
                    Path file = iterator.next().toAbsolutePath();
                    if (Files.isDirectory(file)) {
                        continue;
                    }

                    Path pathWithoutRoot = zipRootPath.relativize(file);
                    Path localFsPath = localFsRoot.resolve(pathWithoutRoot.toString());
                    if (!localFsPath.startsWith(localFsRoot)) {
                        throw new ForbiddenException("local fs path breached");
                    }

                    if (Files.isSymbolicLink(file)) {
                        Path target = Files.readSymbolicLink(file);
                        Path targetZipPath = zipRootPath.relativize(target);

                        Path targetLocalPath = localFsRoot.resolve(targetZipPath);
                        if (!targetLocalPath.startsWith(localFsRoot)) {
                            throw new ForbiddenException("local fs path breached");
                        }
                        Files.createSymbolicLink(localFsPath, targetLocalPath);

                        SymlinkArtifact artifact = new SymlinkArtifact();
                        artifact.setTarget(pathToStr(targetZipPath));
                        artifact.setSource(pathToStr(pathWithoutRoot));
                        files.add(artifact);
                        continue;
                    }

                    Files.createDirectories(localFsPath.getParent());

                    Path filePathRelative = localFsRoot.relativize(localFsPath);
                    String filePathStr = pathToStr(filePathRelative);

                    long size = Files.size(file);
                    try (InputStream fis = Files.newInputStream(file);
                         DigestInputStream digestInputStream = new DigestInputStream(fis, messageDigest)) {
                        Files.copy(digestInputStream, localFsPath);
                        byte[] digest = digestInputStream.getMessageDigest().digest();
                        FileArtifact artifact = new FileArtifact();
                        artifact.setFile(filePathStr);
                        artifact.setSize(size);
                        artifact.setHash(digest);
                        files.add(artifact);
                    }

                }
            }
        } catch (ProviderNotFoundException | ZipException e) {
            throw new WrongUpdateVersionDataException("Can't unpack archive", e);
        }
        return files;
    }

    private String pathToStr(Path path) {
        StringJoiner filePathJoiner = new StringJoiner("/");
        for (Path component : path) {
            filePathJoiner.add(component.toString());
        }
        return filePathJoiner.toString();
    }
}
