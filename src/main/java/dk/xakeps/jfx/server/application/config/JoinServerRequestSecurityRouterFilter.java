package dk.xakeps.jfx.server.application.config;

import io.quarkus.vertx.web.RouteFilter;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class JoinServerRequestSecurityRouterFilter {
    @RouteFilter(3000)
    public void extractBody(RoutingContext context) {
        if (context.request().method() != HttpMethod.POST) {
            context.next();
            return;
        }

        if (!"/session-service/join".equals(context.normalizedPath())) {
            context.next();
            return;
        }
        BodyHandler bodyHandler = BodyHandler.create(false);
        bodyHandler.handle(context);
    }
    @RouteFilter(3000 - 1)
    public void copyAccessToken(RoutingContext context) {
        if (context.request().method() != HttpMethod.POST) {
            context.next();
            return;
        }

        if (!"/session-service/join".equals(context.normalizedPath())) {
            context.next();
            return;
        }

        if (context.getBodyAsJson() == null) {
            context.next();
            return;
        }
        String accessToken = context.getBodyAsJson().getString("accessToken");
        context.request().headers().add("Authorization", "Bearer " + accessToken);
        context.next();
    }
}
