package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.application.VersionDirLocator;
import dk.xakeps.jfx.server.domain.model.version.InstallType;

import javax.enterprise.context.ApplicationScoped;
import java.nio.file.Path;

@ApplicationScoped
public class VersionDirLocatorImpl implements VersionDirLocator {
    @Override
    public Path findPath(String version, InstallType installType) {
        return Path.of("versions.d", installType.getType(),
                version + "." + installType.getExtension().getAsString());
    }
}
