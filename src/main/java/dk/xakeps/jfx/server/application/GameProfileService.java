package dk.xakeps.jfx.server.application;

import dk.xakeps.jfx.server.domain.model.profile.property.texture.SkinTextureModel;

import java.io.InputStream;
import java.util.UUID;

public interface GameProfileService {
    void changeSkin(String externalId, UUID profileId, SkinTextureModel skinType, InputStream skinStream);

    void changeCape(String externalId, UUID profileId, InputStream skinStream);
}
