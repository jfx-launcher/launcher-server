package dk.xakeps.jfx.server.application;

import dk.xakeps.jfx.server.domain.model.version.VersionManifest;

import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;

public interface VersionService {
    VersionManifest getVersionManifest(URI baseUri);
    void installForge(Path installerJar);

    Optional<Path> getForgeInstaller(String version);
    Optional<Path> getForgeJson(String version);
}
