package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.application.SignService;
import org.apache.commons.io.IOUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;

@ApplicationScoped
public class SignServiceImpl implements SignService {
    private final Signature signature;
    private final String keypairPath;

    public SignServiceImpl(@ConfigProperty(name = "keypair.path", defaultValue = "/pkcs8_keypair.der") String keypairPath)
            throws NoSuchAlgorithmException, IOException, InvalidKeySpecException, InvalidKeyException {
        this.keypairPath = keypairPath;
        this.signature = Signature.getInstance("SHA1withRSA");
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        byte[] key;
        Path keypair = Path.of(keypairPath);
        if (Files.exists(keypair)) {
            key = Files.readAllBytes(keypair);
        } else {
            key = IOUtils.resourceToByteArray(keypairPath, getClass().getClassLoader());
        }

        KeySpec keySpec = new PKCS8EncodedKeySpec(key);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        signature.initSign(privateKey);
    }

    @Override
    public byte[] sign(byte[] data) throws SignatureException {
        signature.update(data);
        return signature.sign();
    }
}
