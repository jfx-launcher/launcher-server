package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.application.ModPackDirLocator;

import javax.enterprise.context.ApplicationScoped;
import java.nio.file.Path;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@ApplicationScoped
public class ModPackDirLocatorImpl implements ModPackDirLocator {
    @Override
    public Path findPath(String modPackId, Instant uploadTime) {
        String modPackTimeDir = DateTimeFormatter.ISO_INSTANT.format(uploadTime.truncatedTo(ChronoUnit.SECONDS))
                .replace(':', '_');
        return Path.of("modpacks.d", modPackId, modPackTimeDir).toAbsolutePath();
    }
}
