package dk.xakeps.jfx.server.application;

import dk.xakeps.jfx.server.domain.model.profile.Account;

import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;

public interface ModPackService {
    void save(boolean overrideRights, String externalId, String modPackId,
              String minecraftVersion, String javaVersion, Set<String> ignoredFiles, Path zipPath) throws IOException;

    void createModPack(String externalId, String modPackId);

    void deleteModPack(String modPackId);

    void enableModPackVersion(boolean overrideRights, String externalId, String modPackId, Instant version);

    Optional<Path> getFile(String modPackId, Instant version, String filePath);

    Set<Account> getModPackManagers(boolean overrideRights, String requester, String modPackId);

    void addManager(String modPackId, String managerToAddId);

    void deleteManager(String modPackId, String managerToDeleteId);
}
