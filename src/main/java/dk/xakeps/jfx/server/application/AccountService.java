package dk.xakeps.jfx.server.application;

import dk.xakeps.jfx.server.domain.model.profile.Account;
import dk.xakeps.jfx.server.domain.model.profile.GameProfile;
import dk.xakeps.jfx.server.domain.model.profile.ProfileUUIDStrategy;

public interface AccountService {
    Account createAccount(String externalId);
    Account createAccountAndProfile(String externalId, String name, ProfileUUIDStrategy uuidStrategy);
    GameProfile createProfile(String externalId, String name, ProfileUUIDStrategy uuidStrategy);
}
