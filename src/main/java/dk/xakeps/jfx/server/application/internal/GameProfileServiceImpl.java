package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.application.GameProfileService;
import dk.xakeps.jfx.server.domain.model.profile.GameProfileException;
import dk.xakeps.jfx.server.domain.model.profile.ProfileNotFoundException;
import dk.xakeps.jfx.server.domain.model.profile.WrongSkinDataException;
import dk.xakeps.jfx.server.domain.model.profile.GameProfile;
import dk.xakeps.jfx.server.domain.model.profile.property.Property;
import dk.xakeps.jfx.server.domain.model.profile.property.PropertyType;
import dk.xakeps.jfx.server.domain.model.profile.property.texture.*;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.GameProfileRepo;
import org.apache.commons.codec.binary.Hex;
import org.apache.tika.Tika;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@ApplicationScoped
public class GameProfileServiceImpl implements GameProfileService {
    private final GameProfileRepo gameProfileRepo;

    private final int maxSkinSize;

    @Inject
    public GameProfileServiceImpl(GameProfileRepo gameProfileRepo,
                                  @ConfigProperty(name = "skin.max-file-size", defaultValue = "16384") int maxSkinSize) {
        this.gameProfileRepo = gameProfileRepo;
        this.maxSkinSize = maxSkinSize;
    }

    @Transactional
    @Override
    public void changeSkin(String externalId, UUID profileId, SkinTextureModel skinType, InputStream skinStream) {
        Tuple tuple = setTexture(TextureType.SKIN, externalId, profileId, skinStream);

        Texture skinTexture = tuple.texture();
        Map<TextureMetadata, String> metadata = skinTexture.getMetadata();
        if (skinType == SkinTextureModel.SLIM) {
            skinTexture.addMetadata(TextureMetadata.MODEL, SkinTextureModel.SLIM);
        } else {
            metadata.remove(TextureMetadata.MODEL);
        }

        gameProfileRepo.persist(tuple.gameProfile());
    }

    @Transactional
    @Override
    public void changeCape(String externalId, UUID profileId, InputStream skinStream) {
        Tuple tuple = setTexture(TextureType.CAPE, externalId, profileId, skinStream);
        gameProfileRepo.persist(tuple.gameProfile());
    }

    private Tuple setTexture(TextureType textureType, String externalId, UUID profileId, InputStream skinStream) {
        Tika tika = new Tika();
        byte[] skinData;
        try {
            skinData = skinStream.readNBytes(maxSkinSize);
        } catch (IOException e) {
            throw new GameProfileException("Can't read skin data", e);
        }
        String mimeType = tika.detect(skinData);
        if (!"image/png".equals(mimeType)) {
            throw new WrongSkinDataException("not a png");
        }
        GameProfile gameProfile = gameProfileRepo.findByAccountExternalIdAndUuidWithProperties(externalId, profileId)
                .orElse(null);
        if (gameProfile == null) {
            throw new ProfileNotFoundException();
        }


        String hash;
        byte[] digest;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            digest = instance.digest(skinData);
            hash = Hex.encodeHexString(digest);
        } catch (NoSuchAlgorithmException e) {
            throw new GameProfileException("Can't find hash algorithm");
        }

        Path skinPath = Path.of("skins", hash);
        if (Files.notExists(skinPath)) {
            try {
                Files.createDirectories(skinPath.getParent());
                Files.write(skinPath, skinData);
            } catch (IOException e) {
                throw new GameProfileException("Can't save file", e);
            }
        }

        Property texturesProperty = gameProfile.getProperty(PropertyType.TEXTURES).orElse(null);
        if (texturesProperty == null) {
            texturesProperty = new Property();
            texturesProperty.setPropertyType(PropertyType.TEXTURES);
            texturesProperty.setProfile(gameProfile);
            gameProfile.addProperty(texturesProperty);
        }

        TexturesPropertyValue propertyValue = (TexturesPropertyValue) texturesProperty.getValue();
        if (propertyValue == null) {
            propertyValue = new TexturesPropertyValue();
            propertyValue.setGameProfile(gameProfile);
            propertyValue.setProperty(texturesProperty);
            texturesProperty.setValue(propertyValue);
        }
        propertyValue.setTimestamp(Instant.now());

        Texture texture = propertyValue.getTexture(textureType).orElse(null);
        if (texture == null) {
            texture = new Texture();
            texture.setTextureType(textureType);
            propertyValue.addTexture(texture);
        }
        texture.setHash(digest);
        Map<TextureMetadata, String> metadata = texture.getMetadata();
        if (metadata == null) {
            metadata = new HashMap<>();
            texture.setMetadata(metadata);
        }

        return new Tuple(gameProfile, texture);
    }

    private record Tuple(GameProfile gameProfile, Texture texture) {}
}
