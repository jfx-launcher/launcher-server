package dk.xakeps.jfx.server.application.rest;

public record ErrorResponse(String type, String message) {
}
