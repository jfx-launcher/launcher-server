package dk.xakeps.jfx.server.application;

import java.security.SignatureException;
import java.util.Base64;

public interface SignService {
    byte[] sign(byte[] data) throws SignatureException;
    default String signAndGetBase64(byte[] data) throws SignatureException {
        return Base64.getEncoder().encodeToString(sign(data));
    }
}
