package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.domain.model.modpack.*;
import dk.xakeps.jfx.server.domain.model.shared.AccountNotFoundException;
import dk.xakeps.jfx.server.domain.model.shared.ForbiddenException;
import dk.xakeps.jfx.server.domain.model.shared.MessageDigestException;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.ModPackRepo;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.ModPackVersionRepo;
import dk.xakeps.jfx.server.application.ModPackDirLocator;
import dk.xakeps.jfx.server.application.ModPackService;
import dk.xakeps.jfx.server.domain.model.profile.Account;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.AccountRepo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Stream;

@ApplicationScoped
public class ModPackServiceImpl implements ModPackService {
    private final AccountRepo accountRepo;
    private final ModPackRepo modPackRepo;
    private final ModPackVersionRepo modPackVersionRepo;
    private final ModPackDirLocator modPackDirLocator;

    @Inject
    public ModPackServiceImpl(AccountRepo accountRepo,
                              ModPackRepo modPackRepo,
                              ModPackVersionRepo modPackVersionRepo,
                              ModPackDirLocator modPackDirLocator) {
        this.accountRepo = accountRepo;
        this.modPackRepo = modPackRepo;
        this.modPackVersionRepo = modPackVersionRepo;
        this.modPackDirLocator = modPackDirLocator;
    }

    @Transactional
    @Override
    public void save(boolean overrideRights, String externalId, String modPackId,
                     String minecraftVersion, String javaVersion, Set<String> ignoredFiles, Path zipPath) throws IOException {

        for (String ignoredFile : ignoredFiles) {
            if (ignoredFile.contains("\\")) {
                throw new IllegalIgnoredPathException("Path contains illegal sequence [\\]: " + ignoredFile);
            }
        }

        ModPack modPack;
        if (overrideRights) {
            modPack = modPackRepo.findByModPackIdAndMinecraftVersion(modPackId).orElse(null);
        } else {
            modPack = modPackRepo.findByModPackIdAndMinecraftVersionWithManager(modPackId, externalId)
                    .orElse(null);
        }
        if (modPack == null) {
            throw new ModPackNotFoundException("Mod pack not found");
        }

        Instant uploadTime = Instant.now();
        Path modPackDir = modPackDirLocator.findPath(modPackId, uploadTime).toAbsolutePath();
        List<Artifact> artifacts = extractZip(modPackDir, zipPath);

        ModPackVersion modPackVersion = new ModPackVersion();
        modPackVersion.setModPack(modPack);
        modPackVersion.setUploadTime(uploadTime);
        modPackVersion.setArtifacts(artifacts);
        modPackVersion.setMinecraftVersion(minecraftVersion);
        modPackVersion.setJavaVersion(javaVersion);
        modPackVersion.setIgnoredFiles(ignoredFiles);

        List<ModPackVersion> versions = modPack.getVersions();
        if (versions == null) {
            versions = new ArrayList<>(1);
            modPack.setVersions(versions);
        }
        versions.add(modPackVersion);

        modPackRepo.persist(modPack);
    }

    @Transactional
    @Override
    public void createModPack(String externalId, String modPackId) {
        Account account = accountRepo.findByExternalId(externalId).orElse(null);
        if (account == null) {
            throw new AccountNotFoundException("Account not found");
        }
        ModPack modPack = new ModPack();
        modPack.setModPackId(modPackId);
        modPack.setManagers(Set.of(account));
        modPackRepo.persist(modPack);
    }

    @Transactional
    @Override
    public void deleteModPack(String modPackId) {
        modPackRepo.deleteModPack(modPackId);
    }

    @Transactional
    @Override
    public void enableModPackVersion(boolean overrideRights, String externalId,
                                     String modPackId, Instant version) {
        ModPack modPack;
        if (overrideRights) {
            modPack = modPackRepo.findByModPackIdAndMinecraftVersion(modPackId).orElse(null);
        } else {
            modPack = modPackRepo.findByModPackIdAndMinecraftVersionWithManager(modPackId, externalId)
                    .orElse(null);
        }
        if (modPack == null) {
            throw new ModPackNotFoundException("Mod pack not found");
        }

        ModPackVersion modPackVersionObj = modPackVersionRepo.findByUploadTime(modPackId, version)
                .orElse(null);
        if (modPackVersionObj == null) {
            throw new ModPackVersionNotFoundException("Mod pack version not found");
        }
        modPack.setEnabledModPack(modPackVersionObj);
        modPackRepo.persist(modPack);
    }

    @Override
    public Optional<Path> getFile(String modPackId, Instant version, String filePath) {
        Path path = modPackDirLocator.findPath(modPackId, version);
        Path file = path.resolve(filePath);
        if (!file.startsWith(path)) {
            return Optional.empty();
        }
        if (Files.notExists(file)) {
            return Optional.empty();
        }
        return Optional.of(file);
    }

    @Transactional
    @Override
    public Set<Account> getModPackManagers(boolean overrideRights, String requester, String modPackId) {
        ModPack modPack = modPackRepo.findModPackWithManagers(modPackId).orElse(null);
        if (modPack == null) {
            throw new ModPackNotFoundException("Mod pack not found");
        }
        if (overrideRights) {
            return modPack.getManagers();
        }
        for (Account manager : modPack.getManagers()) {
            if (manager.getExternalId().equals(requester)) {
                return modPack.getManagers();
            }
        }
        throw new ForbiddenException("Forbidden");
    }

    @Transactional
    @Override
    public void addManager(String modPackId, String managerToAddId) {
        ModPack modPack = modPackRepo.findModPackWithManagers(modPackId).orElse(null);
        if (modPack == null) {
            throw new ModPackNotFoundException("Mod pack not found");
        }
        Account account = accountRepo.findByExternalId(managerToAddId).orElse(null);
        if (account == null) {
            throw new AccountNotFoundException("Account not found");
        }
        modPack.getManagers().add(account);
        modPackRepo.persist(modPack);
    }

    @Transactional
    @Override
    public void deleteManager(String modPackId, String managerToDeleteId) {
        ModPack modPack = modPackRepo.findModPackWithManagers(modPackId).orElse(null);
        if (modPack == null) {
            throw new ModPackNotFoundException("Mod pack not found");
        }
        if (!modPack.getManagers().removeIf(account -> account.getExternalId().equals(managerToDeleteId))) {
            throw new AccountNotFoundException("Manager not exist");
        }
        modPackRepo.persist(modPack);
    }

    private List<Artifact> extractZip(Path localFsRoot, Path zipFile) throws IOException {
        List<Artifact> artifacts = new ArrayList<>();
        Files.createDirectories(localFsRoot.getParent());
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new MessageDigestException("Can't find SHA-1 MessageDigest", e);
        }
        try (FileSystem fileSystem = FileSystems.newFileSystem(zipFile)) {
            Path zipRootPath = fileSystem.getPath("/");
            try (Stream<Path> walk = Files.walk(zipRootPath)) {
                Iterator<Path> iterator = walk.iterator();
                while (iterator.hasNext()) {
                    Path file = iterator.next().toAbsolutePath();
                    if (!Files.isRegularFile(file)) {
                        continue;
                    }
                    Path pathWithoutRoot = zipRootPath.relativize(file);
                    Path localFsPath = localFsRoot.resolve(pathWithoutRoot.toString());
                    if (!localFsPath.startsWith(localFsRoot)) {
                        throw new ForbiddenException("local fs path breached");
                    }
                    Files.createDirectories(localFsPath.getParent());

                    Path filePathRelative = localFsRoot.relativize(localFsPath);
                    StringJoiner filePathJoiner = new StringJoiner("/");
                    for (Path path : filePathRelative) {
                        filePathJoiner.add(path.toString());
                    }
                    String filePathStr = filePathJoiner.toString();
                    long size = Files.size(file);
                    try (InputStream fis = Files.newInputStream(file);
                         DigestInputStream digestInputStream = new DigestInputStream(fis, messageDigest)) {
                        Files.copy(digestInputStream, localFsPath);
                        byte[] digest = digestInputStream.getMessageDigest().digest();
                        Artifact artifact = new Artifact();
                        artifact.setFile(filePathStr);
                        artifact.setSize(size);
                        artifact.setHash(digest);
                        artifacts.add(artifact);
                    }

                }
            }
        }
        return artifacts;
    }
}
