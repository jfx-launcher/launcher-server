package dk.xakeps.jfx.server.application.rest;

import dk.xakeps.jfx.server.domain.model.shared.NotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
    @Override
    public Response toResponse(NotFoundException exception) {
        return Response.status(Response.Status.NOT_FOUND)
                .entity(new ErrorResponse(exception.getClass().getSimpleName(), exception.getMessage()))
                .build();
    }
}
