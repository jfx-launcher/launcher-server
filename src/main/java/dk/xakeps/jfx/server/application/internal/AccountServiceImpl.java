package dk.xakeps.jfx.server.application.internal;

import dk.xakeps.jfx.server.domain.model.shared.AccountNotFoundException;
import dk.xakeps.jfx.server.domain.model.profile.Account;
import dk.xakeps.jfx.server.domain.model.profile.GameProfile;
import dk.xakeps.jfx.server.domain.model.profile.ProfileUUIDStrategy;
import dk.xakeps.jfx.server.infrastructure.persistence.jpa.AccountRepo;
import dk.xakeps.jfx.server.application.AccountService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class AccountServiceImpl implements AccountService {
    private final AccountRepo accountRepo;

    @Inject
    public AccountServiceImpl(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    @Override
    public Account createAccount(String externalId) {
        Account account = new Account();
        account.setExternalId(externalId);
        accountRepo.persist(account);
        return accountRepo.find("externalId", externalId).singleResult();
    }

    @Transactional
    @Override
    public Account createAccountAndProfile(String externalId, String name, ProfileUUIDStrategy uuidStrategy) {
        Account account = new Account();
        account.setExternalId(externalId);

        createAndSaveProfile(account, name, uuidStrategy);

        return account;
    }

    @Transactional
    @Override
    public GameProfile createProfile(String externalId, String name, ProfileUUIDStrategy uuidStrategy) {
        Account account = accountRepo.findByExternalId(externalId).orElse(null);
        if (account == null) {
            throw new AccountNotFoundException();
        }

        return createAndSaveProfile(account, name, uuidStrategy);
    }

    private GameProfile createAndSaveProfile(Account account, String name, ProfileUUIDStrategy uuidStrategy) {
        GameProfile profile = new GameProfile();
        profile.setUuid(uuidStrategy.generate(name));
        profile.setName(name);
        account.addGameProfile(profile);
        if (account.getSelectedProfile() == null) {
            account.setSelectedProfile(profile);
        }

        accountRepo.persist(account);
        return profile;
    }
}
