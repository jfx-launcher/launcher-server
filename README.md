# Requirements
1. MariaDB or other database server
2. Keycloak, if you want to use different OpenID server, please,
   implement your own `dk.xakeps.jfx.server.domain.service.RemoteAccountFinderService`

# Environment variables
### OpenID settings
`OIDC_SERVER` - URI to a Keycloak server
`OIDC_CLIENT_ID` - OpenID client id
`OIDC_SECRET` - OpenID secret

### Database settings
`DB_KIND` - Database type, `mariadb` by default
`DB_USERNAME` - Username
`DB_PASSWORD` - Password
`DB_URL` - JDBC URL

### Application settings
`UUID_STRATEGY` - Possible values: `RANDOM` - generates random UUID, `OFFLINE_NAME` - generates `OfflinePlayer` strategy UUID, so it's compatible with offline UUIDs
`SKIN_MAX_SIZE` - Max skin or cloak size, 16KiB by default
`KEYPAIR_PATH` - Path to a keypair that is used to sign profile properties

### Roles used:
1. `CAN_SEE_USERS` - used by account service, so users can get Subject of a different user
2. `CHANGE_SKIN`, `CHANGE_CAPE` - user can change skin or cape
3. `CREATE_MOD_PACK` - ability to create mod packs
4. `JOIN_GAME` - ability to join game servers
5. `MANAGE_UPDATES`, `MANAGE_UPDATES_JAVA`, `MANAGE_UPDATES_LAUNCHER`, `MANAGE_UPDATES_UPDATER` - ability to upload and enable JAVA, LAUNCHER and UPDATER updates
6. `MANY_PROFILES` - user can have many game profiles
7. `MODIFY_MOD_PACK` - user can modify mod pack if access was granted by user who has `MODIFY_MOD_PACK_OVERRIDE`
8. `MODIFY_MOD_PACK_OVERRIDE` - user has full rights over all mod packs, even if access was not granted
9. `MODIFY_VERSION` - user can upload game versions
10. `USER` - all users should have this role, basic right to create their first profile

### Keycloak configuration
If you use keycloak, client service account must have `view-users` role of `realm-management` client, otherwise `AccountResource` will not work properly

### Pairing with a launcher
You have to generate keypair.
1. Generate private key: `openssl genrsa -out keypair.pem 4096`
2. Extract public key: `openssl rsa -in keypair.pem -outform der -pubout -out public.der`
3. Create PKCS8 keystore: `openssl pkcs8 -topk8 -inform pem -outform der -nocrypt -in keypair.pem -out pkcs8_keypair.der`
4. Set `KEYPAIR_PATH` to PKCS8 keystore file path
5. Ship `public.der` with launcher client side

# How to build or debug this project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/server-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.html.

## Related Guides

- RESTEasy JAX-RS ([guide](https://quarkus.io/guides/rest-json)): REST endpoint framework implementing JAX-RS and more

## Provided Code

### RESTEasy JAX-RS

Easily start your RESTful Web Services

[Related guide section...](https://quarkus.io/guides/getting-started#the-jax-rs-resources)
